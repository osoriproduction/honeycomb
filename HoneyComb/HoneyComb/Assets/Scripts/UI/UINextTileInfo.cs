﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UINextTileInfo : MonoBehaviour {

    public GameManager _GameManager;  

    public List<UnitNextTileInfo> _UnitNextTileInfoList;



    void Start () {
        _GameManager = GetComponentInParent<GameManager>();
        
	}
	
    public void ReDrawNextTilesAll()
    {
        if (this.gameObject.activeSelf)
        {
            for (int i = 0; i < _UnitNextTileInfoList.Count; i++)
            {
                _UnitNextTileInfoList[i].RedrawNextTileHexLand();

            }
        }

    }


	
}
