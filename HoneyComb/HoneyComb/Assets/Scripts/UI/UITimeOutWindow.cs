﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimeOutWindow : MonoBehaviour {


    public GameManager _GameManager;

    public GameObject _ReBTNCover;
    public Text _ReCostText;

    public void CallUITimeOutWindow()
    {
 
        this.gameObject.SetActive(true);
        _GameManager._IsGlobalPause = true;

        _GameManager._DataManager._BGMManager._CurrAudioSource.Stop();
        _GameManager._DataManager._BGMManager.SoundPlay(41);

        if (_GameManager._DataManager._UserCoin>= _GameManager._CurrOpeningWorldMap._EntranceGoldFee)
        {
            if(_GameManager._CurrOpeningWorldMap._EntranceGoldFee == 0)
            {
                _ReCostText.text = "Free";

            }
            else
            {
                _ReCostText.text = _GameManager._CurrOpeningWorldMap._EntranceGoldFee + "";

            }
            _ReCostText.color = Color.white;
            _ReBTNCover.SetActive(false);
        }
        else
        {
            _ReCostText.text = _GameManager._CurrOpeningWorldMap._EntranceGoldFee + "";
            _ReCostText.color = Color.red;
            _ReBTNCover.SetActive(true);

        }

    }

    public void CloseUITimeOutWindow()
    {
        this.gameObject.SetActive(false);
        _GameManager._IsGlobalPause = false;


    }

    public void ResumeMusic()
    {
        _GameManager._DataManager._BGMManager.MusicStart((int)Random.Range(0, 2));

    }


}
