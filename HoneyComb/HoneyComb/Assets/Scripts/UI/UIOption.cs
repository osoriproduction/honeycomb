﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOption : MonoBehaviour {

    public AnimatedButton _IsMusicOn;
    public AnimatedButton _IsMusicOff;

    public AnimatedButton _IsSoundOn;
    public AnimatedButton _IsSoundOff;

    public AnimatedButton _IsVibrateOn;
    public AnimatedButton _IsVibrateOff;

    public TitleManager _TitleManager;


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseUIOption();
        }
    }

    public void CallUIOption()
    {
        if (!_TitleManager._IsMenuOn)
        {
            _TitleManager._IsMenuOn = true;

            _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

            this.gameObject.SetActive(true);

            if (_TitleManager == null)
            {
                _TitleManager = GetComponentInParent<TitleManager>();
            }

            if (_TitleManager._DataManager._BGMManager._MusicOn)
            {
                _IsMusicOn.gameObject.SetActive(true);
                _IsMusicOff.gameObject.SetActive(false);
            }
            else
            {
                _IsMusicOn.gameObject.SetActive(false);
                _IsMusicOff.gameObject.SetActive(true);
            }

            if (_TitleManager._DataManager._BGMManager._SoundOn)
            {
                _IsSoundOn.gameObject.SetActive(true);
                _IsSoundOff.gameObject.SetActive(false);
            }
            else
            {
                _IsSoundOn.gameObject.SetActive(false);
                _IsSoundOff.gameObject.SetActive(true);
            }


            if (_TitleManager._DataManager._VibrateOn)
            {
                _IsVibrateOn.gameObject.SetActive(true);
                _IsVibrateOff.gameObject.SetActive(false);
            }
            else
            {
                _IsVibrateOn.gameObject.SetActive(false);
                _IsVibrateOff.gameObject.SetActive(true);
            }
        }
    }

    public void CloseUIOption()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay(5);

        this.gameObject.SetActive(false);
        _TitleManager._IsMenuOn = false;

    }


    public void SoundOn()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));


        _TitleManager._DataManager._BGMManager._SoundOn = true;

        if (_TitleManager._DataManager._BGMManager._SoundOn)
        {
            _IsSoundOn.gameObject.SetActive(true);
            _IsSoundOff.gameObject.SetActive(false);
        }
        else
        {
            _IsSoundOn.gameObject.SetActive(false);
            _IsSoundOff.gameObject.SetActive(true);
        }

    }

    public void SoundOff()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay(5);

        _TitleManager._DataManager._BGMManager._SoundOn = false;

        if (_TitleManager._DataManager._BGMManager._SoundOn)
        {
            _IsSoundOn.gameObject.SetActive(true);
            _IsSoundOff.gameObject.SetActive(false);
        }
        else
        {
            _IsSoundOn.gameObject.SetActive(false);
            _IsSoundOff.gameObject.SetActive(true);
        }
    }

    public void MusicOn()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

        _TitleManager._DataManager._BGMManager._MusicOn = true;
        _TitleManager._DataManager._BGMManager.MusicStart(2);


        if (_TitleManager._DataManager._BGMManager._MusicOn)
        {
            _IsMusicOn.gameObject.SetActive(true);
            _IsMusicOff.gameObject.SetActive(false);
        }
        else
        {
            _IsMusicOn.gameObject.SetActive(false);
            _IsMusicOff.gameObject.SetActive(true);
        }
    }

    public void MusicOff()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay(5);

        _TitleManager._DataManager._BGMManager._CurrAudioSource.Stop();

        _TitleManager._DataManager._BGMManager._MusicOn = false;

        if (_TitleManager._DataManager._BGMManager._MusicOn)
        {
            _IsMusicOn.gameObject.SetActive(true);
            _IsMusicOff.gameObject.SetActive(false);
        }
        else
        {
            _IsMusicOn.gameObject.SetActive(false);
            _IsMusicOff.gameObject.SetActive(true);
        }

    }

    public void VibrateOn()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

        _TitleManager._DataManager._VibrateOn = true;

        if (_TitleManager._DataManager._VibrateOn)
        {
            _IsVibrateOn.gameObject.SetActive(true);
            _IsVibrateOff.gameObject.SetActive(false);
        }
        else
        {
            _IsVibrateOn.gameObject.SetActive(false);
            _IsVibrateOff.gameObject.SetActive(true);
        }

    }

    public void VibrateOff()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay(5);
        
        _TitleManager._DataManager._VibrateOn = false;

        if (_TitleManager._DataManager._VibrateOn)
        {
            _IsVibrateOn.gameObject.SetActive(true);
            _IsVibrateOff.gameObject.SetActive(false);
        }
        else
        {
            _IsVibrateOn.gameObject.SetActive(false);
            _IsVibrateOff.gameObject.SetActive(true);
        }

    }



    public void ExitGame()
    {

        _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));
        _TitleManager._DataManager.SavePlayerData();

        Application.Quit();

    }

}
