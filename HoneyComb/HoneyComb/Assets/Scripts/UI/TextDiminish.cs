﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDiminish : MonoBehaviour {

    public GameManager _GameManager;

    public bool _IsMissionPurpose;
    public Text _Text;

    void OnEnable()
    {
        _GameManager = GetComponentInParent<GameManager>();
        _Text = GetComponent<Text>();

        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.1f);
        if (_IsMissionPurpose)
        {
            if (_GameManager._CurrOpeningWorldMap._IsTryAgain) //두번째 이상일 경우.
            {
                this.gameObject.SetActive(false);
            }
            else //처음일 경우 미션 보여주고. 그다음에 꺼주자.
            {
                StartCoroutine(DimOffTitleName(_Text));
                _GameManager._CurrOpeningWorldMap._IsTryAgain = true;

            }
        }
        else
        {
            StartCoroutine(DimOffTitleName(_Text));

        }
    }

    float _currAlpha;
    IEnumerator DimOffTitleName(Text _Text)
    {
        _Text.color = new Color(_Text.color.r, _Text.color.g, _Text.color.b, 0);
        yield return new WaitForSeconds(1);
        _currAlpha = 0;

        for (int i = 0; i < 170; i++)
        {
            yield return new WaitForEndOfFrame();

            _Text.color = new Color(_Text.color.r, _Text.color.g, _Text.color.b, _currAlpha);
            _currAlpha += 0.007f;
        }

        _currAlpha = 1;

        for (int i = 0; i < 100; i++)
        {
            yield return new WaitForEndOfFrame();

            _Text.color = new Color(_Text.color.r, _Text.color.g, _Text.color.b, _currAlpha);
            _currAlpha -= 0.0165f;
        }


        _Text.color = new Color(_Text.color.r, _Text.color.g, _Text.color.b, 0);
        this.gameObject.SetActive(false);
    }

}
