﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UITitleNameRoot : MonoBehaviour {

	void Start () {
        StartCoroutine(DimOffTitleName(_NameText1));
        StartCoroutine(DimOffTitleName(_NameText2));
        StartCoroutine(DimOffTitleName(_NameText3));


    }


    public Text _NameText1;
    public Text _NameText2;
    public Text _NameText3;

    float _currAlpha;
    IEnumerator DimOffTitleName(Text _Text)
    {
        yield return new WaitForSeconds(3);
        _currAlpha = 1;

        for (int i=0; i < 400; i++)
        {
            yield return new WaitForEndOfFrame();

            _Text.color = new Color(_Text.color.r, _Text.color.g, _Text.color.b, _currAlpha);
            _currAlpha = 0.995f * _currAlpha;
        }


        _Text.color = new Color(_Text.color.r, _Text.color.g, _Text.color.b, 0);

    }

}
