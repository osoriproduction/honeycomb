﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITutorial : MonoBehaviour {

    public List<GameObject> _EachStepList;
    public GameManager _GameManager;

	public void CallUITutorial () {

        if (_GameManager._IsMenuOn)
            return;

        _GameManager._IsMenuOn = true;
        _GameManager._IsGlobalPause = true;

        this.gameObject.SetActive(true);
        CallTuto(0);
        _GameManager = GetComponentInParent<GameManager>();
    }


    public void CloseUITutorial()
    {
        _GameManager._IsGlobalPause = false;

        this.gameObject.SetActive(false);
        _GameManager._IsMenuOn = false;

    }

    public void CallTuto(int _StepNum)
    {
        for (int i = 0; i < _EachStepList.Count; i++)
        {
            _EachStepList[i].SetActive(false);
        }

        _EachStepList[_StepNum].SetActive(true);
        _GameManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

    }

}
