﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHeadQuarter : MonoBehaviour {

    public TitleManager _TitleManager;
    public List<UIUnitUpgrade> _UIUnitUpgradeList; //UI에서 가지고 있는 업그레이드 목록들. 이거 그냥 사전에 갖고 있자. 생성하지 말고.. ㅋㅋ 좋아.

    public void CallUIHeadQuarter()
    {
        if (!_TitleManager._IsMenuOn)
        {
            _TitleManager._IsMenuOn = true;
            _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

            this.gameObject.SetActive(true);
            DisplayCurrUpgrade();
        }
    }

    public void CloseUIHeadQuarter()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay(5);
        this.gameObject.SetActive(false);
        _TitleManager._IsMenuOn = false;
        _TitleManager._DataManager.SavePlayerData();
    }

    public void DisplayCurrUpgrade()
    {
        _TitleManager._DataManager.CheckHeadQuarterProduct();
        _TitleManager.ComplieCollectingCoin();

        for (int h = 0; h < _UIUnitUpgradeList.Count; h++)
        {
            for (int i = 0; i < _TitleManager._DataManager._CurrUpgradeList.Count; i++)
            {
                if (_UIUnitUpgradeList[h]._TypeNum ==  _TitleManager._DataManager._CurrUpgradeList[i]._TypeNum)
                {                   
                    if (_TitleManager._DataManager._CurrUpgradeList[i]._CurrLevel == _TitleManager._DataManager._UpgradedCurrLevel[h])
                    {
                        _UIUnitUpgradeList[h]._UnitUpgrade = _TitleManager._DataManager._CurrUpgradeList[i];

                        if (_UIUnitUpgradeList[h]._TypeNum <= 3)
                        {
                            if (_TitleManager._DataManager._CurrUpgradeList[i]._CurrLevel == 0)
                            {
                                _UIUnitUpgradeList[h]._HeadText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Header + " (미연구) ";
                                _UIUnitUpgradeList[h]._DesText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Des + "<color=grey> 현재 증가랑 없음</color>";

                            }
                            else
                            {
                                _UIUnitUpgradeList[h]._HeadText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Header + " <color=lightblue>Lv." + _TitleManager._DataManager._CurrUpgradeList[i]._CurrLevel+ "</color>";
                                _UIUnitUpgradeList[h]._DesText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Des + " <color=lime>현재 증가랑: +" + _TitleManager._DataManager._CurrUpgradeList[i]._CurrValue+ "</color>";

                            }
                        }
                        else if(h==4) //4 같은 연구의 경우 처리
                        {
                            if (_TitleManager._DataManager._CurrUpgradeList[i]._CurrLevel == 0)
                            {
                                _UIUnitUpgradeList[h]._HeadText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Header + " (미연구) ";
                                _UIUnitUpgradeList[h]._DesText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Des + " <color=lime>현재 발견하는 섬: " + (_TitleManager._DataManager._CurrUpgradeList[i]._CurrValue+3) + "개" + "</color>";

                            }
                            else
                            {
                                _UIUnitUpgradeList[h]._HeadText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Header + " <color=lightblue>Lv." + _TitleManager._DataManager._CurrUpgradeList[i]._CurrLevel + "</color>";
                                _UIUnitUpgradeList[h]._DesText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Des + " <color=lime>현재 발견하는 섬: " + (_TitleManager._DataManager._CurrUpgradeList[i]._CurrValue+3)+"개" + "</color>";
                            }
                        }
                        else if (h == 5)
                        {
                            if (_TitleManager._DataManager._CurrUpgradeList[i]._CurrLevel == 0)
                            {
                                _UIUnitUpgradeList[h]._HeadText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Header + " (미연구) ";
                                _UIUnitUpgradeList[h]._DesText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Des + "<color=grey> 추가 타일 없음</color>";

                            }
                            else
                            {
                                _UIUnitUpgradeList[h]._HeadText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Header + " <color=lightblue>Lv." + _TitleManager._DataManager._CurrUpgradeList[i]._CurrLevel + "</color>";
                                _UIUnitUpgradeList[h]._DesText.text = _TitleManager._DataManager._CurrUpgradeList[i]._Des + " <color=lime>추가 무료 타일: +" + _TitleManager._DataManager._CurrUpgradeList[i]._CurrValue + "</color>";

                            }


                        }


                        if (_TitleManager._DataManager._CurrUpgradeList[i]._NeedCoinToUpgrade == 99999)
                        {
                            _UIUnitUpgradeList[h]._CoinText.text = "MAX";

                        }
                        else
                        {
                            _UIUnitUpgradeList[h]._CoinText.text = _TitleManager._DataManager._CurrUpgradeList[i]._NeedCoinToUpgrade + "";

                        }
                        if (_TitleManager._DataManager._UserCoin >= _TitleManager._DataManager._CurrUpgradeList[i]._NeedCoinToUpgrade)
                        {
                            _UIUnitUpgradeList[h]._Cover.gameObject.SetActive(false);
                        }
                        else
                        {
                            _UIUnitUpgradeList[h]._Cover.gameObject.SetActive(true);
                        }
                    }                    
                }
            }
        }
        _TitleManager.CheckAndDisplayProductByStoneTotally();
    }

}
