﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textDungdung : MonoBehaviour {


	Vector3 currVec;
	int iFrame;
	bool bStart;
	float fT;
	float fA;
	void Awake(){
		
		bStart = false;
		StartCoroutine (StartOn ());
		fT = Random.Range (0.02f, 0.04f);
		fA = Random.Range (0.25f, 0.3f);
	}

	IEnumerator StartOn(){
		
		yield return new WaitForSeconds (Random.Range (0, 1));
		bStart = true;

	}
	void Update () {
		if (bStart == true) {
			currVec = this.transform.localPosition;
			currVec.y += Mathf.Sin (iFrame * fT) * fA;
			iFrame++;
			this.transform.localPosition = new Vector3 (currVec.x, currVec.y, currVec.z);
		}
	}
}
