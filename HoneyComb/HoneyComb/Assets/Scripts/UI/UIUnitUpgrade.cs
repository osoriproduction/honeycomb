﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUnitUpgrade : MonoBehaviour {

    //정보
    public int _TypeNum;
    public UnitUpgrade _UnitUpgrade;

    //연결
    public TitleManager _TitleManager;

    public Text _HeadText;
    public Text _DesText;
    public Text _CoinText;

    public GameObject _Cover;


    public void Start()
    {
        _TitleManager = GetComponentInParent<TitleManager>();
    }

    public void UpgradeThis()
    {
        if(_TitleManager._DataManager._UserCoin>= _UnitUpgrade._NeedCoinToUpgrade)
        {
            _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

            _TitleManager._DataManager._UpgradedCurrLevel[_TypeNum]++;
            _TitleManager._DataManager._UserCoin -= _UnitUpgrade._NeedCoinToUpgrade;
            _TitleManager._UIHeadQuarter.DisplayCurrUpgrade();
        }
        else
        {

        }

    }

}
