﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIReview : MonoBehaviour {

    public TitleManager _TitleManager;
    
	public void CallUIReview()
    {
        if (!_TitleManager._IsMenuOn)
        {
            _TitleManager._IsMenuOn = true;
            _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

            _TitleManager._DataManager._AlreadyReview = true;
            _TitleManager._DataManager.SavePlayerData();

            this.gameObject.SetActive(true);
        }
    }


    public void CloseUIReview()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay(5);

        this.gameObject.SetActive(false);
        _TitleManager._IsMenuOn = false;
    }


    public void GoReview()
    {

        _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.OSORIPD.HoneyComb");

        this.gameObject.SetActive(false);
        _TitleManager._IsMenuOn = false;


  

    }



}
