﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShowAd : MonoBehaviour {

    public TitleManager _TitleManager;

    public void CallUIShowAD()
    {
        if (!_TitleManager._IsMenuOn)
        {
            _TitleManager._IsMenuOn = true;
            _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

            this.gameObject.SetActive(true);
        }
    }

    public void CloseUIShowAD()
    {
        _TitleManager._DataManager._BGMManager.SoundPlay(5);

        _TitleManager._IsMenuOn = false;
        this.gameObject.SetActive(false);

    }

    public void ShowADForChargning() //광고 재생
    {
        _TitleManager._DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));
        _TitleManager._DataManager.ShowVideoAd(2);

        _TitleManager._IsMenuOn = false;
        this.gameObject.SetActive(false);
    }



}
