﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICoinAcquired : MonoBehaviour {

   // public Color thisColor;

	void OnEnable () {
        this.transform.localPosition = new Vector3(0, 8.7f, 0);
        //thisColor = new Color(thisColor.r, thisColor.g, thisColor.b, 1 );
        StartCoroutine(MovingUp());
	}

    float vY = 0;
    
	IEnumerator MovingUp()
    {
        for(int i=0; i<100; i++)
        {
            yield return new WaitForEndOfFrame();
            if( i> 40)
            {
                this.transform.Translate(new Vector3(0, vY,0));
                vY += 0.015f;
               // thisColor = new Color(thisColor.r, thisColor.g, thisColor.b, 1 - vY*500);
            }
        }

        this.gameObject.SetActive(false);
    }

}
