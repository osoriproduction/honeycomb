﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTileController : MonoBehaviour {

    public List<GameObject> _SelectableObjectList;

    int _TargetGroupNum;


    void OnEnable()
    {


        _TargetGroupNum = (int)Random.Range(0, _SelectableObjectList.Count);

        if (_SelectableObjectList != null)
        {
            for (int i = 0; i < _SelectableObjectList.Count; i++)
            {
                _SelectableObjectList[i].SetActive(false);
            }
        }

        if (_SelectableObjectList[_TargetGroupNum] != null)
            _SelectableObjectList[_TargetGroupNum].SetActive(true);
        
    }

}
