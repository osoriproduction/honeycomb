﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;





public enum TilePriceExpandLogicType  //타일의 확장 
{
    NaturalNumber,  //자연수
    NaturalNumberEasy, //자연수인데 이지 버젼
    OddNumber,  //홀수
    PrimeNumber  //소수( 자기자신으로만 나뉘어지는 수 )
    
}

public class UnitPosXZ
{
    public float _PosX;
    public float _PosZ;
    public float _DistR; //밴 포인트 등에 쓸만한 거리 제곱

    public UnitPosXZ()
    {
    }

    public UnitPosXZ(float _PosX, float _PosZ, float _DistR)
    {
        this._PosX = _PosX;
        this._PosZ = _PosZ;
        this._DistR = _DistR;
    }
}

public class UnitIdealWorldMap  //이데아의 월드맵에 대한 유닛 객체 클래스. 나중에 리스트로 만들엉준다.
{
    public int _Num; //고유 넘버..스테이지 번호와는 좀 다른 개념
    public int _Grade; //처음에 나오는 타일 종류. 일단 0 이면 기본 숲, 산, 들판만 나옴
    public int _LandSizeCount; //땅의 크기. 모두 깬 상태에서의 타일 개수.
    public float _LandSizeMaxDistance; //땅의 크기. 모두 깬 상태에서의 랜드 사이즈 거리. (중앙 타일로부터)
    public int _LandDensity; //얼마나 땅이 퍼져있는지 촘촘한지
    public float _InitOpenLandDistance; //처음 시작할 때 열어줄 랜드 사이즈 거리. 

    public int _WoodMultiple;  //자원별 배수
    public int _IronMultiple;
    public int _FoodMultiple;
    public int _ExpandMultiple; //확장 룰에 의한 배수 보너스

    public TilePriceExpandLogicType _TilePriceLogicType; // 어떤 가격 모형을 쓸지.    위에 입력된 값들에 의해 구성됨.  다이나믹이므로 본체로 뭔가 전송해야 함.

    
    //입력된 값에 의해 구성되는 것들
    public int _EntranceGoldFee; //입장료 골드.  위에 입력된 값들에 의해 구성됨
    public List<UnitTileIdeal> _IdealWorldMap; //이데아 속에 살아 숨쉬는 이데아월드맵 데이터.  위에 입력된 값들에 의해 구성됨

    public int _WorldValue; //시가. 소유되었을 경우,  각 멀티플 및 플레이 결과에 의해 구성됨.

    //유저가 먹은 후에 변하는 값들
    public bool _IsOwned; //유저가 소유하고 있는지.
    public int _EarningGoldPerHour; //시간당 골드 생산량

    public float _WorldCurrAngle;
    public float _WorldAngleWelocity;
    public float _WorldRadius;
    public float _WorldRefY;

    public bool _IsConnected; //유저땅에 붙여서 생산 중인지.
    public bool _IsTryAgain; //두번째로 도전하는 건지. 이렇게 하는거에는 미션 목표 안보여줘도 될듯.

    public List<AllowedUnitTileType> _AllowedUnitTileTypeList; //배치에 쓰일 타일 후보들. 이걸 그레이드별로 다르게 해주자.

    public List<UnitPosXZ> _BanPointList; //여기찍혀진 지점들에서는 맵들이 생성되지 않는다. 모양 만들기용.

    public int _UnitProductWoodByStoneInIsland;  //이 섬안에서 비행석에 의해 생산되는 자원 생산량. 
    public int _UnitProductIronByStoneInIsland;
    public int _UnitProductFoodByStoneInIsland;

    public int _TimerSec; //월드의 타이머 할당 시간 


    public UnitIdealWorldMap(){}
    public UnitIdealWorldMap(int _Num, int _Grade, int _LandSizeCount,float _LandSizeMaxDistance,int _LandDensity  , float _InitOpenLandDistance, TilePriceExpandLogicType _TilePriceExpandLogic , List<UnitPosXZ> _BanPointList)
    {
        this._Num = _Num;
        this._Grade = _Grade;
        this._LandSizeCount = _LandSizeCount;
        this._LandSizeMaxDistance = _LandSizeMaxDistance;
        this._LandDensity = _LandDensity;
        this._InitOpenLandDistance = _InitOpenLandDistance;
        this._TilePriceLogicType = _TilePriceExpandLogic;
        this._BanPointList = _BanPointList;
        //----------------------------------------------------------------------------------------------
        //여기 종속 변수들을 생성해주면 됨
        
        if(_TilePriceExpandLogic== TilePriceExpandLogicType.NaturalNumberEasy)
        {
            this._WoodMultiple = (int)Random.Range(1, 3);
            this._IronMultiple = (int)Random.Range(1, 3);
            this._FoodMultiple = (int)Random.Range(1, 3);
            this._ExpandMultiple = 1;
        }
        else if (_TilePriceExpandLogic == TilePriceExpandLogicType.NaturalNumber)
        {
            this._WoodMultiple = (int)Random.Range(1,5);
            this._IronMultiple = (int)Random.Range(1, 5);
            this._FoodMultiple = (int)Random.Range(1, 5);
            this._ExpandMultiple = 2;

        }
        else if (_TilePriceExpandLogic == TilePriceExpandLogicType.OddNumber)
        {
            this._WoodMultiple = (int)Random.Range(2, 6);
            this._IronMultiple = (int)Random.Range(2, 6);
            this._FoodMultiple = (int)Random.Range(2, 6);
            this._ExpandMultiple = 4;

        }
        else if (_TilePriceExpandLogic == TilePriceExpandLogicType.PrimeNumber)
        {
            this._WoodMultiple = (int)Random.Range(4, 10);
            this._IronMultiple = (int)Random.Range(4, 10);
            this._FoodMultiple = (int)Random.Range(4, 10);
            this._ExpandMultiple = 8;

        }


        InstantiateIdealWorldMap();  //맵 모양을 잡아 준다.
        
        for(int i=0; i<999; i++)
        {
            if (CheckIdealWorldMap()) //정합성 조건을 충족하는지
            {
                Debug.Log(" 정합성 체크: " + this._Num + " 는 정합성을 충족함 " + _IdealWorldMap.Count);
                break;
            }
            else
            {
                Debug.Log(" 정합성 체크: " + this._Num + " 은 정합성을 충족못함 " + _IdealWorldMap.Count);

                this._Num = this._Num  + 17;   //번호를 올려준 후 다시 만들어보자.
                InstantiateIdealWorldMap();
            }


        }
        if (_Num >= 2)
        {
            if (_Num <= 4) //2,3,4
            {
                this._EntranceGoldFee = Mathf.Max(1, (int)(Mathf.Sqrt(_Grade * this._WoodMultiple * this._IronMultiple * this._FoodMultiple * Random.Range(0.2f, 0.3f)) * _IdealWorldMap.Count * 0.05f)); //초반 재화 지급하고 돈벌자.

            }
            else
            {
                this._EntranceGoldFee = Mathf.Max(1, (int)(Mathf.Sqrt(_Grade * this._WoodMultiple * this._IronMultiple * this._FoodMultiple * Random.Range(0.2f, 0.3f)) * _IdealWorldMap.Count * 0.07f)); //초반 재화 지급하고 돈벌자.

            }
        }
        else
        {
            this._EntranceGoldFee = 0;

        }
        this._TimerSec = 40 - this._Grade * 5;
    }


    public void InstantiateIdealWorldMap()    //맵상에 단위월드를 생성한 후 뿌려준다.
    {
        _IdealWorldMap = new List<UnitTileIdeal>();  //만들어줄 월드 초기화 및 배정.
                
        UnitTileIdeal _TempUnitTile = null;

        bool _IsTempDiscovered;
        bool _IsTempBought;


        float _TempDist;  //생성되는 타일이 중앙으로부터의 거리.

        
        int _xMin = 70;
        int _xMax = 130;
        int _xInit = -1; //이거는 그 월드에서 처음 시작하는 첫줄 x값임. -1이 미설정된 값.

        int _yMin = -15;
        int _yMax = 15;

        for (int i = _xMin; i < _xMax; i++)   //행별 생성
        {
            for (int j = _yMin; j < _yMax; j++)   //열별 생성
            {
                if (i == (int)((_xMin + _xMax) * 0.5f) && j == (int)((_yMin + _yMax) * 0.5f)) //스타팅 포인트이다.
                {
                    _IsTempDiscovered = true;
                    _IsTempBought = true;
                }
                else
                {
                    _IsTempDiscovered = false;
                    _IsTempBought = false;
                }
                _TempUnitTile = new UnitTileIdeal();

                if (i % 2 == 0) //타일 인스턴스 생성
                {
                    _TempUnitTile._PosX = j * 8.6f;
                    _TempUnitTile._PosZ = (i - 100) * -2.5f;
                }
                else
                {
                    _TempUnitTile._PosX = j * 8.6f + 4.3f;
                    _TempUnitTile._PosZ = (i - 100) * -2.5f;
                }

                if (i == (int)((_xMin + _xMax) * 0.5f) && j == (int)((_yMin + _yMax) * 0.5f)) //스타팅 포인트이다.
                {
                    _TempUnitTile._IsGenesisTile = true;

                }
                _TempUnitTile._IsBoughtLand = _IsTempBought;
                _TempUnitTile._IsDiscovered = _IsTempDiscovered;

                //공식
                _TempUnitTile._UniqueFingerPrintNumber = _Num * (j + i * j + i + j + j * j * j + i * i * i + j * j * i + i * i * j * _Num); //


                _TempUnitTile._Num = _IdealWorldMap.Count;

                _TempDist = CalDistRFromCenterTile(_TempUnitTile);  // 여기서 거리를 계산해줌
                if (_TempDist <= _InitOpenLandDistance)
                {
                    _TempUnitTile._IsBoughtLand = true;
                    _TempUnitTile._IsDiscovered = true;
                }
                if (_IdealWorldMap.Count < _LandSizeCount)  //목표 최대 수량보다 아직 작고
                {
                    if (_TempDist <= _LandSizeMaxDistance * (1+ (_TempUnitTile._UniqueFingerPrintNumber%120-60)*0.01f ))  //최고 거리보다 작고.
                    {
                        if (!CalDistRFromEachBanPointList(_TempUnitTile._PosX, _TempUnitTile._PosZ, _BanPointList)) //밴 거리 이내면 생성하지 않는다.
                        {
                            if(_IdealWorldMap.Count ==0)
                            {
                                _xInit = i;
                            }

                            if (i == _xInit)  // 상대적 첫 줄 (개시하는 줄)
                            {
                                if (_TempUnitTile._UniqueFingerPrintNumber % 100 <= _LandDensity)  // 끝 두자리만 랜덴시티랑 비교하는데 쓴다.
                                {
                                    _IdealWorldMap.Add(_TempUnitTile);
                                }
                            }
                            else  // 두번째 줄부터임
                            {
                                if (CountNearTileExist(_TempUnitTile, 30)==1) // 근처에 타일이 하나 있으면 무조건 이어준다.
                                {
                                    _IdealWorldMap.Add(_TempUnitTile);
                                    _TempUnitTile._Num = 999998;  //테스트용 코드
                                }
                                else if (CountNearTileExist(_TempUnitTile, 30) > 1)  //접하는 타일이 두개 이상이다.
                                {
                                    if (_TempUnitTile._UniqueFingerPrintNumber % 100 <= _LandDensity)  // 끝 두자리만 랜덴시티랑 비교하는데 쓴다.
                                    {
                                        _IdealWorldMap.Add(_TempUnitTile);
                                    }
                                }
                                else 
                                {
                                   //근처에 존재하는 타일이 하나도 없으면 타일을 생성하지 않는다.

                                }
                            }
                        }
                    }
                }
            }
        }

        //Debug.Log("_IdealWorldMap.Count " + _IdealWorldMap.Count);
    }


    public bool CheckIdealWorldMap() //현재 월드맵이 정합성 조건을 만족하는지 체크한다. 타일 개수가 8개 이상이어야 하고, 모든 타일에 있어서 하나라도 접하지 않는 타일이 없어서는 안된다.
    {
        if (_IdealWorldMap != null)
        {
            if (_IdealWorldMap.Count > 7)
            {
                for (int i = 0; i < _IdealWorldMap.Count; i++)
                {                    
                    if(CountNearTileExist(_IdealWorldMap[i], 30) == 0)
                    {

                        return false;

                    }
                }
                return true;

            }
            else
            {

                return false;
            }

        }
        else
        {
            return false;

        }




    }


    float CalDistRFromCenterTile(UnitTileIdeal _target)
    {
        float _return=0;

        _return = (_target._PosX) * (_target._PosX) + (_target._PosZ ) * (_target._PosZ );

        return _return;

    }

    bool CalDistRFromEachBanPointList(float _PosX, float _PosZ, List<UnitPosXZ> _BanPointList )  //밴포인트리스트들과 대상 포인트의 거리를 체크해서, 밴 거리 안에 해당하면 true반환.
    {
        if (_BanPointList == null)
        {
            return false;
        }

        bool _return = false;
        float _tempDistR;

        for(int i=0; i< _BanPointList.Count; i++)
        {
            _tempDistR = (_PosX - _BanPointList[i]._PosX) * (_PosX - _BanPointList[i]._PosX) + (_PosZ - _BanPointList[i]._PosZ) * (_PosZ - _BanPointList[i]._PosZ);
            if(_tempDistR < _BanPointList[i]._DistR)
            {
                _return = true;
                break;
            }
        }

        return _return;
    }

    int CountNearTileExist(UnitTileIdeal _tempUnitTile, float _DistRefR )   //일정 거리 내에 추가된 월드 맵이 있는지 본다.  30 1칸, 150 2칸. 개수 반환으로 변경
    {
        int _return=0;

        for (int i=0; i< _IdealWorldMap.Count; i++)  //이미 추가된 unitTileIdeal만 있으니 괜찮음.
        {
            if (_IdealWorldMap[i] != _tempUnitTile)  //같은 타일이 아니어야 함.
            {
                if ((_tempUnitTile._PosX - _IdealWorldMap[i]._PosX) * (_tempUnitTile._PosX - _IdealWorldMap[i]._PosX)
                    + (_tempUnitTile._PosZ - _IdealWorldMap[i]._PosZ) * (_tempUnitTile._PosZ - _IdealWorldMap[i]._PosZ) < _DistRefR)
                {
                    _return++;
                }
            }
        }

        return _return;

    }





}

public class UnitTileIdeal //타일 저장용 객체
{
    public int _Num; //타일 넘버  +100 00250: 100번 라인의 x: 2.5     -100 00250: 100번 라인의 x:-2.5
    public float _PosX;
    public float _PosZ;  //\라인 번호로 대체
    public int _TileGrade; //타일의 그레이드. 자연타일은 모두 0. 가공타일은 모두 1~15까지 갖는다.
    public TileType _TileType;

    public bool _IsDiscovered; // 구입된 영지내의 땅이지만, 타일을 합치고 빼서 다시 정찰이 필요한 미확인 지역.
    public bool _IsBoughtLand; //한번이라도 돈 주고 구입된 영지 내의 땅을 의미
    public bool _IsGenesisTile;//제너시스 타일임

    public int _TilePrice; //이 타일의 가격. 아직 구입하지 못한, 어두울 때만 사용한다.
    public ResourceType _TilePriceType; //이 타일의 가격 타입. 1000이면 나무, 2000이면 골드, 3000이면 식량을 의미한다. 

    public int _UniqueFingerPrintNumber; //일종의 지문
        
}


public class UnitTile 
{
    public int _Num; //타일 넘버  +100 00250: 100번 라인의 x: 2.5     -100 00250: 100번 라인의 x:-2.5
    public float _PosX;
    public float _PosZ;  //\라인 번호로 대체
    public int _TileGrade; //타일의 그레이드. 자연타일은 모두 0. 가공타일은 모두 1~15까지 갖는다.
    public TileType _TileType;
    public UnitHexLand _UnitHexLand;  //서로 소유한다.

    public bool _IsDiscovered; // 구입된 영지내의 땅이지만, 타일을 합치고 빼서 다시 정찰이 필요한 미확인 지역.
    public bool _IsBoughtLand; //한번이라도 돈 주고 구입된 영지 내의 땅을 의미
    public bool _IsGenesisTile;//제너시스 타일임

    public int _TilePrice; //이 타일의 가격. 아직 구입하지 못한, 어두울 때만 사용한다.
    public ResourceType  _TilePriceType; //이 타일의 가격 타입. 1000이면 나무, 2000이면 골드, 3000이면 식량을 의미한다. 

    public bool _Is2XBoosting; //2배 부스팅중
    public int _CurrentProductCount; //현재 이 타일의 생산량. 자원 타일 등에만 해당됨    
    public int _UniqueFingerPrintNumber; //일종의 지문

    public UnitTile[] _NearTiles; //옆의 타일들 정보
    public List<UnitTile> _NearTileList; //옆의 타일들 정보인데 이건 리스트로 마구잡이로 순서상관없이 저장.

    public UnitTile() { }
    public UnitTile(float _PosX, float _PosZ, TileType _TileType, int _TileGrade)
    {
        this._PosX = _PosX;
        this._PosZ = _PosZ;
        if (_PosX >= 0)
        {
            this._Num = (int)(_PosZ * 100000 + _PosX);
        }
        else
        {
            this._Num = -1* (int)(_PosZ * 100000  - _PosX);

        }

        this._TileType = _TileType;
        this._TileGrade = _TileGrade;
        _UniqueFingerPrintNumber = (int)Random.Range(0, 10000);
    }

}

public enum ResourceType  //리소스 타입
{
    Gold = 100,
    Wood = 1000,
    Iron = 2000,
    Food = 3000,
    Water = 4000,
    Crystal = 5000

}


public enum TileType  //타일 타입
{
    Town=0,
    TownForest = 100,
    TownMountain =200,
    TownFarm =300,
    Forest=1000,
    Mountain=2000,
    FarmLand=3000,
    Water=4000,
    FlyingStone=5000,  //공중에 놓으면 둥둥 떠다니는 스톤. 섬이 공중에 떠 있는 원동력이라고 알려져 있음. 이 귀보를 어떻게 사용할지 학자들이 연구에 매진하고 잇다.
    StoneForest = 5100,  //스톤을 이용하여 나무 생산량을 늘림. 모든 월드에서 나무 생산량이 +1 됨. 
    StoneMountain = 5200,
    StoneFarm =5300

}



public class UnitTileType   //타일 설명 윈도우 정보 출력을 위한 단위 클래스
{
    public TileType _TileType;
    public int _Grade;
    public string _ResultTileName; //현재 타일의 이름. 설명 을 위해서 여기서 넣어주었다.
    public string _ResultTileDes;  //현재 타일의 설명. 
    public string _TileProductDes; //현재 타일의 생산량에 관한 설명

    public UnitTileType()
    {
    }

    public UnitTileType(TileType _TileType, int _Grade, string _ResultTileName, string _ResultTileDes, string _TileProductDes)
    {
        this._TileType = _TileType;
        this._Grade = _Grade;
        this._ResultTileName = _ResultTileName;
        this._ResultTileDes = _ResultTileDes;
        this._TileProductDes = _TileProductDes;
    }

}


public class UnitCombine  //단위 조합. 
{
    public bool _IsUnLocked;
    public int _CombineGrade;
    public TileType _TileType1;
    public TileType _TileType2;
    public TileType _TileType3;

    public TileType _ResultTileType;
    public int _ResultTileGrade;



    public UnitCombine()
    {
    }

    public UnitCombine(bool _IsUnLocked, int _CombineGrade, TileType _TileType1, TileType _TileType2, TileType _TileType3, int _ResultTileGrade, TileType _ResultTileType)
    {
        this._IsUnLocked = _IsUnLocked;
        this._CombineGrade = _CombineGrade;
        this._TileType1 = _TileType1;
        this._TileType2 = _TileType2;
        this._TileType3 = _TileType3;

        this._ResultTileGrade = _ResultTileGrade;
        this._ResultTileType = _ResultTileType;


    }    
}

public class GameManager : MonoBehaviour
{

    //글로벌 파우제
    public bool _IsGlobalPause;


    //외부 입력
    public DataManager _DataManager;

    public TouchInputManager _TouchInputManager;
    public GameObject _3DRoot;
    public UnitMsgPlusResources _UnitMsgPlusResourcesPrefab;
    public TileInfoWindow _TileInfoWindow;
    public GameObject _NowLoading;

    public GameObject _ResultWindow;
    public Text _ResultGoldPerHourText;
    public Text _ResultValueText;
    public Text _ResultLandValueRankText;

    public GameObject _FreeCountOBJ; 
    public Text _FreeCountText; //몇개 프리 가 남았는지 보여주는 것.

    public UITimeOutWindow _UITimeOutWindow;
    public Text _TimerWarning;

    //타이머 시스템 관련
    public GameObject _TimerRoot;
    public GameObject _TimerGaugeBar;
    public Text _LeftTimeText;



    //내부 입력
    public UnitHexLand _UnitHexLandPrefab;  //프리팹
    public GameObject _WorldHexRoot; //월드 루트

    public Material _WhiteRefMaterial;
    public Material _YellowRefMaterial;
    public Material _RedRefMaterial;

    Material[] _WhiteRefMaterials;
    Material[] _YellowRefMaterials;
    Material[] _RedRefMaterials;



    //시스템
    public List<UnitTile> _CurrWorldMap; //인스턴시에이트 된 현재의 월드맵
    public List<UnitHexLand> _CurrUnitHexLandList; //draw된 헥스랜드 리스트    

    public List<UnitTile> _NotBoughtTileList; //아직 사지 않은 타일 카운트

    //타일 정보
    public List<UnitTile> _PlayerTileList; //플레이어의 타일 리스트 . 생산소든 아니든 전부 포함. 어두운 곳도 포함
    public List<UnitTile> _PlayerProductingTileList; //현재 생산중인 타일 리스트들. 생산이 가능한 타일만 포함. (그레이드 값이 있는..)
    public UnitTile _TheGenesisTile; //맨 처음 시작하는 마을이 있는 타일
    public List<UnitTile> _SelectedTileList;  //현재 선택된 타일들
    public List<UnitCombine> _SelectedUnitCombineList; //현재 손가락으로 그리고 있는 패턴이 해당되는 컴바인 리스트들을 저장해놓는 리스트    
    public List<AllowedUnitTileType> _NextTileTypes; //향후 3개까지의 생성된 타일. 0에서 뺴서 써주고, 하나씩 땡겨 주고, 뒤에서 새로 생성해서 추가해준다. 


    //저장하는 유저 정보&시스템 데이터
    public int _PlayerGold;
    public List<UnitIdealWorldMap> _IdealWorldMapList;  //현재 맵상에 생성된 월드맵 리스트.  여기서 소유 여부(클리어 여부 표기)
    public UnitIdealWorldMap _CurrOpeningWorldMap; //이건 이제 인게임에서 곧 열릴 월드맵임. 이것도 샅샅이 쪼개서 저장한다.
    public int _CountMakeIsland;

    //그외 유저정보

    public int _PlayerWood;
    public int _PlayerIron;
    public int _PlayerFood;

    public int _UnitProductGold;
    public int _UnitProductWood;
    public int _UnitProductIron;
    public int _UnitProductFood;

    public int _TotalScore;

    public Text _PlayerGoldText;
    public Text _PlayerWoodText;
    public Text _PlayerIronText;
    public Text _PlayerFoodText;
    public Text _PlayCostText;

    public Text _PlayerGoldProductText;
    public Text _PlayerWoodProductText;
    public Text _PlayerIronProductText;
    public Text _PlayerFoodProductText;

    public Text _PlayerTotalScoreText;
    public Text _LeftTilieText; //몇개 남았는지 표시해주는 텍스트

        
    //tutorial
    public UITutorial _UITutorial;
    public GameObject UIGoToWorld;

    //타이머 관련
    public int _MaxTimerSec; //이 월드의 최대 초 시간
    public int _CurrRemainedTimerSec; //남은 타이머 초 시간

    //UI 이중 터치 관련
    public bool _IsMenuOn;
    

    void Start()
    {
        _DataManager = GameObject.Find("DataManager").GetComponent<DataManager>();
        _DataManager.SceneChanger(1);

        _PlayerTileList = new List<UnitTile>();
        _SelectedTileList = new List<UnitTile>();
        _PlayerProductingTileList = new List<UnitTile>();
        _SelectedUnitCombineList = new List<UnitCombine>();

      

        InitMaterialColorSetting();

        Application.targetFrameRate = 60;


        StartCoroutine(Init());
        StartCoroutine(SlowUpdate());

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (UIGoToWorld.activeSelf)
            {
                CloseUIGoToWorld();
            }
            else
            {
                CallUIGoToWorld();

            }
        }
    }
    IEnumerator SlowUpdate()
    {
        yield return new WaitForSeconds(0.02f);

        if (_CurrOpeningWorldMap._EntranceGoldFee == 0)
        {
            _PlayCostText.text = "Free";

        }
        else
        {
            _PlayCostText.text = _CurrOpeningWorldMap._EntranceGoldFee + "";

        }
        for (; ; )
        {

            _PlayerIronText.text = _PlayerIron + "";
            _PlayerWoodText.text = _PlayerWood + "";
            _PlayerFoodText.text = _PlayerFood + "";
            _PlayerGoldText.text = _PlayerGold + "";

            _PlayerIronProductText.text = "+ " +( _UnitProductIron );
            _PlayerWoodProductText.text = "+ " +( _UnitProductWood );
            _PlayerFoodProductText.text = "+ " + (_UnitProductFood);
            _PlayerGoldProductText.text = "+ " + _UnitProductGold;

            yield return new WaitForSeconds(0.1f);

        }

    }

    IEnumerator SecCounter()
    {
        for(; ; )
        {
            yield return new WaitForSeconds(1);
            if (!_IsGlobalPause)
            {
                if (_CurrRemainedTimerSec >= 1)
                {
                    _CurrRemainedTimerSec -= 1;
                    if (_CurrRemainedTimerSec <= 4)
                    {
                        StartCoroutine(WarningTimeIENum());

                    }
                }
                else   //시간이 다 되어 죽었을 때 처리
                {
                    _UITimeOutWindow.CallUITimeOutWindow();

                }

                _TimerGaugeBar.transform.localScale = new Vector3( (float)_CurrRemainedTimerSec / _MaxTimerSec, 1, 1);
                //_LeftTimeText.text = _CurrRemainedTimerSec + "";
            }
        }
    }
    
    public IEnumerator WarningTimeIENum()
    {
#if UNITY_ANDROID
        if (_DataManager._VibrateOn)
        {
            Handheld.Vibrate();
        }
#endif

        Color _tempCol = _TimerWarning.color;
        float _alpha=0;
        float w = 3.1415f / 60;

        for(int i=0; i<60; i++)
        {
            yield return new WaitForEndOfFrame();
            
            

            _alpha = Mathf.Sin(i * w);
            _TimerWarning.color = new Color(_tempCol.r, _tempCol.g, _tempCol.b, _alpha* _alpha*0.7f);
            
        }

    }




    UnitIdealWorldMap _InstantiatedWorldForTest;
    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.02f);

        _DataManager.LoadPlayerData();  //일정 시간 뒤에 데이터를 읽어주자.
        if (_CurrOpeningWorldMap != null) //로딩 데이터가 존재할 경우
        {
            _CurrWorldMap = ImportIdealWorldMap(_CurrOpeningWorldMap._IdealWorldMap);
        }

        if (_CurrWorldMap == null || _CurrWorldMap.Count == 0)
        { //아무것도 없을때.

            Debug.Log("로딩 된 섬 정보가 없네.뭔가 이상.");
        }
        else
        {//데이터가 있을 때.
            InstantiateTilePriceExpandNumberList(_CurrOpeningWorldMap._TilePriceLogicType);

        }
        ScanAndArragementCurrentMap();  //제너시스 타일을 찾아서 세팅해준다.

        DrawCurrentWorldMapInitiating();  //월드맵 유닛 헥스 프리팹 등을 그려준다.
        ConnectAllNearTiles(); //옆타일들을 찾아서 연결해준다.
        
        RedrawCurrentWorldMap();
        _DataManager.CheckHeadQuarterProduct();

        CheckTotalPoducting(); //생산량 한번 체크해주고 시작.
        CalculateScore();  //점수를 계산해주고 시작한다.
        _NowLoading.SetActive(false);
        
        
        _NextTileTypes = new List<AllowedUnitTileType>();
        DecideNextTiles(1); //처음에 넥스트 타일 1개만 생성해준다. 

        if (_CurrOpeningWorldMap._EntranceGoldFee != 0)
        {
            _PlayCoinText.text = _CurrOpeningWorldMap._EntranceGoldFee + "";

        }
        else
        {
            _PlayCoinText.text = "FREE";

        }
        _CurrentCoinText.text = "(보유: " + _DataManager._UserCoin + ")";

        if (_DataManager._UserCoin >= _CurrOpeningWorldMap._EntranceGoldFee)
        {
           
            _RestartBTNCover.SetActive(false);
            _PlayCostText.color = Color.black;

            _RestartConfirmBTN.enabled = true;
            _RestartConfirmBTNCover.SetActive(false);
            _PlayCoinText.color = Color.white;

        }
        else
        {
            //_RestartConfirmBTN.enabled = false;
            //_RestartConfirmBTNCover.SetActive(true);
            _PlayCoinText.color = Color.red;


            _RestartBTNCover.SetActive(true);
            _PlayCostText.color = Color.red;

        }

        //call Tuto
        if (!_DataManager._IAmNotFirst)
        {
            _DataManager._IAmNotFirst = true;
            _UITutorial.CallUITutorial();

        }

        


        _MaxTimerSec = _CurrOpeningWorldMap._TimerSec;
        _CurrRemainedTimerSec = _MaxTimerSec;
        StartCoroutine(SecCounter());



    }




    public void DrawCurrentWorldMapInitiating()
    {
        _CurrUnitHexLandList = new List<UnitHexLand>();
        _NotBoughtTileList = new List<UnitTile>();

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            UnitHexLand tempHexLand = Instantiate(_UnitHexLandPrefab, _WorldHexRoot.transform);
            tempHexLand._CurrUnitTile = _CurrWorldMap[i];
            tempHexLand._CurrUnitTile._UnitHexLand = tempHexLand;
            tempHexLand.name = _CurrWorldMap[i]._Num + "";
            tempHexLand.transform.localPosition = new Vector3(tempHexLand._CurrUnitTile._PosX, 0, tempHexLand._CurrUnitTile._PosZ);
            tempHexLand._UniqueRotationDegree = (int)(Random.Range(0, 6)) * 60;  //초기 회전각을 잡아준다.
            tempHexLand._DoRedraw = true;

            _CurrUnitHexLandList.Add(tempHexLand);

            if (!_CurrWorldMap[i]._IsBoughtLand)  //사지 않은 땅일 경우, 리스트에 추가해둔다.
            {
                _NotBoughtTileList.Add(_CurrWorldMap[i]);
            }
        }

    }




    public void RedrawCurrentWorldMap()  //타일정보에 따라 헥스랜드들의 외모를 다시 표현해준다.
    {
        int tempRedrawCount = 0;

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            SetDarkTilePrice(_CurrWorldMap[i]); //조건은 메소드 안에서 처리하는걸로..

            if (_CurrWorldMap[i]._UnitHexLand._DoRedraw || _CurrWorldMap[i]._UnitHexLand._IsContactedWithLand)
            {
                _CurrWorldMap[i]._UnitHexLand.DisplayHexAppearance();  //데이터를 모두 넣었으면 표현도 해준다.
                _CurrWorldMap[i]._UnitHexLand._DoRedraw = false;
                tempRedrawCount++;
            }
        }

    }


    public int _FirstGivendTileCount;
    public void GiveTheseTilesToPlayer(UnitTile _TargetUnitTile, float _CheckRange) //_CheckRange=30 1칸, 150 2칸, 230 3칸  
    {
        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (_TargetUnitTile != null)
            {
                if (DistBetweenPointR(_TargetUnitTile._UnitHexLand.transform.position,
                    _CurrWorldMap[i]._UnitHexLand.transform.position) < _CheckRange)   //  체크 범주 안의 타일들을 모두 귀속시켜준다.
                {
                    _CurrWorldMap[i]._IsDiscovered = true;
                    _CurrWorldMap[i]._IsBoughtLand = true;
                    _PlayerTileList.Add(_CurrWorldMap[i]);

                    _FirstGivendTileCount++;

                }
                else
                {
                    if (Random.Range(0, 1000) < 0)  //아주 낮은 확률로 맵에 타일 넣는거 추가.
                    {
                        _CurrWorldMap[i]._IsDiscovered = true;
                        _CurrWorldMap[i]._IsBoughtLand = true;
                        _PlayerTileList.Add(_CurrWorldMap[i]);

                        _FirstGivendTileCount++;
                    }
                    else
                    {
                        _CurrWorldMap[i]._IsDiscovered = false;
                        _CurrWorldMap[i]._IsBoughtLand = false;
                    }
                }
            }
        }
    }


    List<int> _TilePriceExpandNumberList;
    void InstantiateTilePriceExpandNumberList(TilePriceExpandLogicType _Type) //0 is Natural Number, 1 is Odd Number, 2 is PrimeNumber
    {
        _TilePriceExpandNumberList = new List<int>();

        for(int i=0; i< _DataManager._AdditionalFreeTileCount; i++)
        {
            _TilePriceExpandNumberList.Add(0);

        }

        if (_Type == TilePriceExpandLogicType.NaturalNumberEasy)  //공짜 타일 두배
        {

            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);

            _TilePriceExpandNumberList.Add(1);
            _TilePriceExpandNumberList.Add(1);
            _TilePriceExpandNumberList.Add(1);

            for (int i = 1; i < 1000; i++)
            {
                _TilePriceExpandNumberList.Add(i);
            }
        }
        else if (_Type == TilePriceExpandLogicType.NaturalNumber)
        {
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);

            for (int i = 1; i < 1000; i++)
            {
                _TilePriceExpandNumberList.Add(i);
            }
        }


        else if (_Type == TilePriceExpandLogicType.OddNumber)
        {
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);

            _TilePriceExpandNumberList.Add(1);
            _TilePriceExpandNumberList.Add(1);
            _TilePriceExpandNumberList.Add(1);

            for (int i = 1; i < 1000; i++)
            {
                _TilePriceExpandNumberList.Add(i * 2 - 1);
            }

        }
        else if (_Type == TilePriceExpandLogicType.PrimeNumber)
        {
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);
            _TilePriceExpandNumberList.Add(0);

            _TilePriceExpandNumberList.Add(1);
            _TilePriceExpandNumberList.Add(2);

            for (int i = 3; i < 9999; i++)
            {
                for (int j = 2; j <= i; j++)
                {
                    if (i == j)
                    {
                        _TilePriceExpandNumberList.Add(i);
                        break;
                    }
                    if (i % j == 0)
                    {
                        break;
                    }
                }
            }
        }
    }


    public void SetDarkTilePrice(UnitTile _UnitTile) //개별적인 유닛 타일의 가격을 매겨주고, 관련된 해당 디스플레이를 처리해준다.
    {
        if (!_UnitTile._IsBoughtLand) //사지 못한 땅만 처리해준다.
        {
            if (CheckNearBoughtTile(_UnitTile)) //근처에 산 땅이 있다. 즉 접한경우이다.
            {
                if (_UnitTile._UniqueFingerPrintNumber % 17 < 2 && _CurrOpeningWorldMap._Grade >= 1)  //0등급에서는 골드를 요구하지 말자.
                {
                    _UnitTile._TilePriceType = ResourceType.Gold;

                }
                else if (_UnitTile._TileType == TileType.Forest)
                {
                    if (_UnitTile._UniqueFingerPrintNumber % 2 == 0)
                    {
                        _UnitTile._TilePriceType = ResourceType.Iron;

                    }
                    else
                    {
                        _UnitTile._TilePriceType = ResourceType.Food;

                    }
                }
                else if (_UnitTile._TileType == TileType.Mountain)
                {

                    if (_UnitTile._UniqueFingerPrintNumber % 2 == 0)
                    {

                        _UnitTile._TilePriceType = ResourceType.Wood;
                    }
                    else
                    {
                        _UnitTile._TilePriceType = ResourceType.Food;

                    }
                }
                else if (_UnitTile._TileType == TileType.FarmLand)
                {
                    if (_UnitTile._UniqueFingerPrintNumber % 2 == 0)
                    {
                        _UnitTile._TilePriceType = ResourceType.Wood;

                    }
                    else
                    {
                        _UnitTile._TilePriceType = ResourceType.Iron;

                    }
                }
                else  //일반적이지 않은 경우, 골드를 요구하게 함
                {

                    //if (_UnitTile._UniqueFingerPrintNumber % 3 == 0)
                    //{
                    //    _UnitTile._TilePriceType = ResourceType.Wood;

                    //}
                    //else if(_UnitTile._UniqueFingerPrintNumber % 3 == 1)
                    //{
                    //    _UnitTile._TilePriceType = ResourceType.Iron;

                    //}
                    //else
                    //{
                    //    _UnitTile._TilePriceType = ResourceType.Food;

                    //}

                    _UnitTile._TilePriceType = ResourceType.Gold;

                }

                // Debug.Log(_UnitTile._Num+ "  _UnitTile._TilePriceType : " + _UnitTile._TilePriceType);

                _UnitTile._TilePrice = _TilePriceExpandNumberList[_PlayerTileList.Count - _FirstGivendTileCount];  // 플레이어 타일 개수만큼 차징한다.  처음에 열어준 타일은 제거한다.

                _UnitTile._UnitHexLand._DisplayedNumberObjectList = _UnitTile._UnitHexLand._UnitNumber3D.DisplayNumber(_UnitTile._TilePrice); //일단 숫자부터 켜주고, 켜진 오브젝트들 받아서 저장.

                DisplayCanBuyOrNot(_UnitTile);  //여기 마커 켜주는 기능도 포함. 숫자 색넣어주는 것도 여기에서 가자.

                if (_UnitTile._TilePriceType == ResourceType.Wood)
                {
                    _UnitTile._UnitHexLand._WoodIcon3D.SetActive(true);
                    _UnitTile._UnitHexLand._IronIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._FoodIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._GoldIcon3D.SetActive(false);

                }
                else if (_UnitTile._TilePriceType == ResourceType.Iron)
                {
                    _UnitTile._UnitHexLand._WoodIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._IronIcon3D.SetActive(true);
                    _UnitTile._UnitHexLand._FoodIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._GoldIcon3D.SetActive(false);

                }
                else if (_UnitTile._TilePriceType == ResourceType.Food)
                {
                    _UnitTile._UnitHexLand._WoodIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._IronIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._FoodIcon3D.SetActive(true);
                    _UnitTile._UnitHexLand._GoldIcon3D.SetActive(false);

                }
                else if (_UnitTile._TilePriceType == ResourceType.Gold)
                {
                    _UnitTile._UnitHexLand._WoodIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._IronIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._FoodIcon3D.SetActive(false);
                    _UnitTile._UnitHexLand._GoldIcon3D.SetActive(true);
                 }
            }
            else
            {
                _UnitTile._UnitHexLand._UnitNumber3D.TurnOffDisplayNumber();
                _UnitTile._UnitHexLand._BuyOnHexSelection.SetActive(false);
                _UnitTile._UnitHexLand._BuyOnMarker.SetActive(false);


                _UnitTile._UnitHexLand._WoodIcon3D.SetActive(false);
                _UnitTile._UnitHexLand._IronIcon3D.SetActive(false);
                _UnitTile._UnitHexLand._FoodIcon3D.SetActive(false);
                _UnitTile._UnitHexLand._GoldIcon3D.SetActive(false);

            }
        }
        else
        {
            _UnitTile._TilePrice = 0;
            _UnitTile._UnitHexLand._UnitNumber3D.TurnOffDisplayNumber();
            _UnitTile._UnitHexLand._BuyOnHexSelection.SetActive(false);
            _UnitTile._UnitHexLand._BuyOnMarker.SetActive(false);


            _UnitTile._UnitHexLand._WoodIcon3D.SetActive(false);
            _UnitTile._UnitHexLand._IronIcon3D.SetActive(false);
            _UnitTile._UnitHexLand._FoodIcon3D.SetActive(false);
            _UnitTile._UnitHexLand._GoldIcon3D.SetActive(false);

        }

        CheckHowManyFreeConstrunctionLeft(); //그냥 같이 묻어볼까?
    }

    public GameObject _BurnBTN;
    public void CheckHowManyFreeConstrunctionLeft()//몇개 무료 건설이 남았는지 체크해서 화면에 표시해주는 로직.
    {
        int _tempFreeCount = 0;

        for(int i=0; i< 9999; i++)
        {
            if (_TilePriceExpandNumberList[_PlayerTileList.Count - _FirstGivendTileCount+i] == 0)
            {
                _tempFreeCount++;
            }
            else
            {
                break;
            }

        }
        if(_tempFreeCount > 0)
        {
            _FreeCountOBJ.SetActive(true);
            _FreeCountText.text = _tempFreeCount + "";
            _BurnBTN.gameObject.SetActive(false);
        }
        else
        {
            _FreeCountOBJ.SetActive(false);
            _BurnBTN.gameObject.SetActive(true);

        }

    }

    public void DisplayCanBuyOrNot(UnitTile _UnitTile)  //해당 타일이 살 수 있는지 없는지 마커 표시 해주는 부분
    {

        bool _IsCanBuyOrNot = false; //살수 있는지 없는지 

        _UnitTile._UnitHexLand._BuyOnHexSelection.SetActive(true);
        _UnitTile._UnitHexLand._BuyOnMarker.SetActive(false);
        _UnitTile._UnitHexLand._NotBoughtButBuyable = false;

        if (_UnitTile._TilePriceType == ResourceType.Wood)
        {
            if (_PlayerWood >= _UnitTile._TilePrice)
            {
                _IsCanBuyOrNot = true;

            }

        }
        else if (_UnitTile._TilePriceType == ResourceType.Iron)
        {
            if (_PlayerIron >= _UnitTile._TilePrice)
            {

                _IsCanBuyOrNot = true;

            }

        }
        else if (_UnitTile._TilePriceType == ResourceType.Food)
        {
            if (_PlayerFood >= _UnitTile._TilePrice)
            {
                _IsCanBuyOrNot = true;
            }
        }
        else if (_UnitTile._TilePriceType == ResourceType.Gold)
        {
            if (_PlayerGold >= _UnitTile._TilePrice)
            {
                _IsCanBuyOrNot = true;
            }
        }

        if (_IsCanBuyOrNot)  //살 수 있는 것으로 판정했을 때의 처리
        {
            _UnitTile._UnitHexLand._BuyOnMarker.SetActive(true);

            ChangeMaterialColor(_UnitTile._UnitHexLand._BuyOnHexSelection.GetComponent<MeshRenderer>(), "YELLOW");
            ChangeMaterialColor(_UnitTile._UnitHexLand._BuyOnMarker.GetComponent<MeshRenderer>(), "YELLOW");
            _UnitTile._UnitHexLand._NotBoughtButBuyable = true;

            _UnitTile._UnitHexLand._BuyOnHexSelection.GetComponent<QUTransformAnimation>().enabled = true;
            _UnitTile._UnitHexLand._BuyOnMarker.GetComponent<QUTransformAnimation>().enabled = true;
            _UnitTile._UnitHexLand._FoodIcon3D.GetComponent<QUTransformAnimation>().enabled = true;
            _UnitTile._UnitHexLand._WoodIcon3D.GetComponent<QUTransformAnimation>().enabled = true;
            _UnitTile._UnitHexLand._IronIcon3D.GetComponent<QUTransformAnimation>().enabled = true;
            _UnitTile._UnitHexLand._GoldIcon3D.GetComponent<QUTransformAnimation>().enabled = true;

            for (int i = 0; i < _UnitTile._UnitHexLand._DisplayedNumberObjectList.Count; i++)
            {
                ChangeMaterialColor(_UnitTile._UnitHexLand._DisplayedNumberObjectList[i].GetComponent<MeshRenderer>(), "YELLOW");
            }
        }
        else  //못 살때 처리
        {
            ChangeMaterialColor(_UnitTile._UnitHexLand._BuyOnHexSelection.GetComponent<MeshRenderer>(), "WHITE");


            _UnitTile._UnitHexLand._BuyOnHexSelection.GetComponent<QUTransformAnimation>().enabled = false;
            _UnitTile._UnitHexLand._BuyOnMarker.GetComponent<QUTransformAnimation>().enabled = false;

            _UnitTile._UnitHexLand._FoodIcon3D.GetComponent<QUTransformAnimation>().enabled = false;
            _UnitTile._UnitHexLand._WoodIcon3D.GetComponent<QUTransformAnimation>().enabled = false;
            _UnitTile._UnitHexLand._IronIcon3D.GetComponent<QUTransformAnimation>().enabled = false;
            _UnitTile._UnitHexLand._GoldIcon3D.GetComponent<QUTransformAnimation>().enabled = false;

            for (int i = 0; i < _UnitTile._UnitHexLand._DisplayedNumberObjectList.Count; i++)
            {
                ChangeMaterialColor(_UnitTile._UnitHexLand._DisplayedNumberObjectList[i].GetComponent<MeshRenderer>(), "RED");
            }
        }
    }

    public void ConnectAllNearTiles()  //옆 타일들을 연결해주는 메소드
    {
        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            _CurrWorldMap[i]._NearTiles = new UnitTile[6] { null, null, null, null, null, null };  // 우선 초기화
            _CurrWorldMap[i]._NearTileList = new List<UnitTile>();

            for (int j = 0; j < _CurrWorldMap.Count; j++)
            {
                if (_CurrWorldMap[i] != _CurrWorldMap[j])  //같지 않은 타일만 체크.
                {
                    if (DistBetweenPointR(_CurrWorldMap[i]._UnitHexLand.transform.position, _CurrWorldMap[j]._UnitHexLand.transform.position) < 30)   //  1칸 이내만 검색한다.
                    {
                        if (_CurrWorldMap[j]._UnitHexLand.transform.position.x - _CurrWorldMap[i]._UnitHexLand.transform.position.x > 0)  //대상이 더 오른쪽에 있다.
                        {
                            if (_CurrWorldMap[j]._UnitHexLand.transform.position.z - _CurrWorldMap[i]._UnitHexLand.transform.position.z > 0)  //대상이 더 높다. 
                            {
                                _CurrWorldMap[i]._NearTiles[1] = _CurrWorldMap[j];
                                _CurrWorldMap[i]._NearTileList.Add(_CurrWorldMap[j]);
                            }
                            else if (_CurrWorldMap[j]._UnitHexLand.transform.position.z - _CurrWorldMap[i]._UnitHexLand.transform.position.z < 0) //대상이 더 낮다.
                            {
                                _CurrWorldMap[i]._NearTiles[2] = _CurrWorldMap[j];
                                _CurrWorldMap[i]._NearTileList.Add(_CurrWorldMap[j]);

                            }
                        }
                        else if (_CurrWorldMap[j]._UnitHexLand.transform.position.x - _CurrWorldMap[i]._UnitHexLand.transform.position.x < 0)  //대상이 왼쪽
                        {
                            if (_CurrWorldMap[j]._UnitHexLand.transform.position.z - _CurrWorldMap[i]._UnitHexLand.transform.position.z > 0)  //마지막 타일
                            {
                                _CurrWorldMap[i]._NearTiles[5] = _CurrWorldMap[j];
                                _CurrWorldMap[i]._NearTileList.Add(_CurrWorldMap[j]);

                            }
                            else if (_CurrWorldMap[j]._UnitHexLand.transform.position.z - _CurrWorldMap[i]._UnitHexLand.transform.position.z < 0)  // 더 낮음
                            {
                                _CurrWorldMap[i]._NearTiles[4] = _CurrWorldMap[j];
                                _CurrWorldMap[i]._NearTileList.Add(_CurrWorldMap[j]);

                            }

                        }
                        else   //x=0 임
                        {
                            if (_CurrWorldMap[j]._UnitHexLand.transform.position.z - _CurrWorldMap[i]._UnitHexLand.transform.position.z > 0)  //대상이 더 높다. 이게 0번임
                            {
                                _CurrWorldMap[i]._NearTiles[0] = _CurrWorldMap[j];
                                _CurrWorldMap[i]._NearTileList.Add(_CurrWorldMap[j]);

                            }
                            else if (_CurrWorldMap[j]._UnitHexLand.transform.position.z - _CurrWorldMap[i]._UnitHexLand.transform.position.z < 0)  //대상이 더 낮다.
                            {
                                _CurrWorldMap[i]._NearTiles[3] = _CurrWorldMap[j];
                                _CurrWorldMap[i]._NearTileList.Add(_CurrWorldMap[j]);


                            }
                        }
                    }
                }
            }
        }
    }
    public bool CheckNearBoughtTile(UnitTile _UnitTile)
    {
        bool _return = false;
        _UnitTile._UnitHexLand._IsContactedWithLand = false;  //근처에 접한 땅이 없다고 디폴트로 설정해주고

        for (int i = 0; i < _UnitTile._NearTileList.Count; i++)   //여길 매 터치마다 다 도는게 비효율적임
        {
            if (_UnitTile._NearTileList[i] != null)
            {

                if (_UnitTile._NearTileList[i]._IsBoughtLand)  //근처 타일 중에 산 땅이 있다면...
                {
                    _return = true;
                    _UnitTile._UnitHexLand._IsContactedWithLand = true;  //근처에 접한 땅이 있다고 선언해준다.
                    break;
                }
            }
        }

        return _return;
    }


    bool _RedrawGo;  //다시 그려줄지를 결정하는 스위치
    public void CheckReleasingTiles()  // 드래그를 놓아줄 때 나오는 것.
    {
        int tempLowestWaterGrade;

        _RedrawGo = false;
        //먼저 세개가 달성될 경우, 합쳐준다. 지금은 비용을 청구하지 말고 해보자.
        if (_SelectedTileList.Count == 3)
        {
            _DataManager._BGMManager.SoundPlay((int)Random.Range(23,29));
            _RedrawGo = true;
            
            if (_PlayerProductingTileList.Contains(_SelectedTileList[0]))  //전체 생산 타일에서 이 타일을 빼준다.
            {
                _PlayerProductingTileList.Remove(_SelectedTileList[0]);
            }
            if (_PlayerProductingTileList.Contains(_SelectedTileList[1]))  //전체 생산 타일에서 이 타일을 빼준다.
            {
                _PlayerProductingTileList.Remove(_SelectedTileList[1]);
            }
            if (_PlayerProductingTileList.Contains(_SelectedTileList[2]))  //전체 생산 타일에서 이 타일을 빼준다.
            {
                _PlayerProductingTileList.Remove(_SelectedTileList[2]);
            }

            if (_SelectedTileList[0]._TileType == TileType.Water 
                && 
                !(_SelectedTileList[1]._TileType == TileType.Water && _SelectedTileList[2]._TileType == TileType.Water && _SelectedTileList[0]._TileGrade == _SelectedTileList[1]._TileGrade && _SelectedTileList[1]._TileGrade == _SelectedTileList[2]._TileGrade)
                &&
                (_SelectedTileList[0]._TileGrade> _SelectedTileList[1]._TileGrade && _SelectedTileList[0]._TileGrade> _SelectedTileList[2]._TileGrade  )
                ) //첫번째 타일이 워터이지만, 두번재 타일 세번째 타일도 워터가 아닌 경우. 그리고 첫번째 워터가 제일 등급이 높은 경우
            { //즉 물을 합쳐주는 경우.(강만들기)

                tempLowestWaterGrade = Mathf.Min(_SelectedTileList[0]._TileGrade, _SelectedTileList[1]._TileGrade, _SelectedTileList[2]._TileGrade, 0);
                            
                _SelectedTileList[1]._TileType = _SelectedTileList[0]._TileType;
                _SelectedTileList[1]._TileGrade = tempLowestWaterGrade;

                _SelectedTileList[2]._TileType = _SelectedTileList[0]._TileType;
                _SelectedTileList[2]._TileGrade = tempLowestWaterGrade;

                _SelectedTileList[0]._IsDiscovered = false;
                _SelectedTileList[0]._TileGrade = 0;
            }
            else
            { //물만들기 외의 일반적인 경우

                _SelectedTileList[0]._IsDiscovered = false;
                _SelectedTileList[1]._IsDiscovered = false;

                _SelectedTileList[0]._TileGrade = 0;
                _SelectedTileList[1]._TileGrade = 0;

                if (_SelectedUnitCombineList.Count >= 1)  //현재 타일 유닛 조합단계에 맞는게 있을 경우, 1이상이 저장되어 있다. 이제, 아래 명령어로 해당되는 리절트 타일을 불러와준다.
                {
                    _SelectedTileList[2]._TileType = _SelectedUnitCombineList[0]._ResultTileType;
                    _SelectedTileList[2]._TileGrade = _SelectedUnitCombineList[0]._ResultTileGrade;
                }
                

                if (_SelectedTileList[2]._TileType == TileType.Mountain || _SelectedTileList[2]._TileType == TileType.Forest || _SelectedTileList[2]._TileType == TileType.FarmLand || _SelectedTileList[2]._TileType == TileType.Town
                    || _SelectedTileList[2]._TileType == TileType.StoneMountain || _SelectedTileList[2]._TileType == TileType.StoneFarm || _SelectedTileList[2]._TileType == TileType.StoneForest)
                { //자원타일일 경우에만 생산 대상에 넣어주자.
                    if (!_PlayerProductingTileList.Contains(_SelectedTileList[2]))  //전체 생산 타일에 이걸 넣어준다. 단, 없을 경우에만.. 
                    {
                        _PlayerProductingTileList.Add(_SelectedTileList[2]);
                    }
                }
            }

         
            CheckTotalPoducting(); //생산량 재계산 해주자.
            GivePlayerResources(); //유저에게 현재 생산량에 맞는 자원을 지급하자.
            _SelectedTileList[0]._UnitHexLand._DoRedraw = true;  //다시 그려준다. 
            _SelectedTileList[1]._UnitHexLand._DoRedraw = true;
            _SelectedTileList[2]._UnitHexLand._DoRedraw = true;
            _SelectedTileList[2]._UnitHexLand._FlowerEffect.Play();
            if (_CurrRemainedTimerSec + 5 <= _MaxTimerSec)
            {
                _CurrRemainedTimerSec += 5;
            }
            else
            {
                _CurrRemainedTimerSec = _MaxTimerSec;
            }

            _TimerGaugeBar.transform.localScale = new Vector3((float)_CurrRemainedTimerSec / _MaxTimerSec, 1, 1);

        }


        for (int i = 0; i < _CurrUnitHexLandList.Count; i++)
        {
            _CurrUnitHexLandList[i]._IsSelected = false;
            _CurrUnitHexLandList[i].SetColorByIsSelected();

        }

        _SelectedTileList.Clear();
        if (_RedrawGo)
            RedrawCurrentWorldMap(); //외모를 다시 표현해준다.

        CalculateScore(); //점수를 계산해준다.
    }

    //넥스트 타일 관련 로직

    public void DecideNextTiles(int _Count)  //다음 타일을 정해준다. 분홍색 타일 터치할 때마다 새로 추가해준다. 게임 시작시에 3개를 미리 생성해준다.
    {

        //Debug.Log("DecideNextTiles");
        for (int i = 0; i < _Count; i++)
        {
            if (_NextTileTypes.Count <= 3)
            {
                _NextTileTypes.Add(RollAndGetAllowedUnitTileTypeInCurrent());
            }
        }
    }

    int _TileMadeCount; //매 생성시점마다 서로 다른 타일을 생성하게 해주는 변수
    AllowedUnitTileType RollAndGetAllowedUnitTileTypeInCurrent()  //확률에 따라 굴려서, 적절한 수준의 얼로우드 유닛타일타입을 반환한다.
    {
        float _refDiceNum = 1f;
        _TileMadeCount++;
        AllowedUnitTileType _return = _CurrOpeningWorldMap._AllowedUnitTileTypeList[0];

        for (int i = 0; i < _CurrOpeningWorldMap._AllowedUnitTileTypeList.Count; i++)
        {
           // if (Random.Range(0f, _refDiceNum) <= _CurrOpeningWorldMap._AllowedUnitTileTypeList[i]._Possibility)
            if (  (_CurrOpeningWorldMap._Num+1) * ( _PlayerTileList.Count +_TileMadeCount)* 1771% (int)(_refDiceNum*100)   <= _CurrOpeningWorldMap._AllowedUnitTileTypeList[i]._Possibility*100 )
            {
                _return = _CurrOpeningWorldMap._AllowedUnitTileTypeList[i];
                break;
            }
            else
            {
                _refDiceNum -= _CurrOpeningWorldMap._AllowedUnitTileTypeList[i]._Possibility;

            }
        }
        return _return;

    }

    //넥스트 타일 정보 관련 처리





    public void CheckTotalPoducting() //전체 생산량을 계산해준다. 그런데.. 여기서 샀는지 여부를 체크해주는 것도 나쁘지 않다.
    {
        int tempTotalIronP = 0;
        int tempTotalWoodP = 0;
        int tempTotalFoodP = 0;
        int tempTotalGoldP = 0;

        int tempEachIronP = 0;
        int tempEachWoodP = 0;
        int tempEachFoodP = 0;
        int tempEachGoldP = 0;

        int tempAdditionalRes = 0;

        for (int i = 0; i < _PlayerProductingTileList.Count; i++)  //이미 생산 타일들 리스트가 존재해야 함.
        {
            tempEachIronP = 0;
            tempEachWoodP = 0;
            tempEachFoodP = 0;
            tempEachGoldP = 0;

            _PlayerProductingTileList[i]._CurrentProductCount = 0;

            if (_PlayerProductingTileList[i]._TileGrade != 0  &&  _PlayerProductingTileList[i]._IsBoughtLand)
            {

                tempAdditionalRes = CheckNearTownForAdditionalProduct(_PlayerProductingTileList[i]);

                if (_PlayerProductingTileList[i]._TileType == TileType.Forest  || _PlayerProductingTileList[i]._TileType == TileType.StoneForest) //나무
                {
                    tempEachWoodP += tempAdditionalRes;
                    tempEachWoodP += _PlayerProductingTileList[i]._TileGrade * CheckNearSpecialTownForMultipleProduct(_PlayerProductingTileList[i]);
                    tempTotalWoodP += tempEachWoodP;

                    _PlayerProductingTileList[i]._CurrentProductCount = tempEachWoodP;  //현재 자원 타일의 생산량을 저장해준다.

                }
                else if (_PlayerProductingTileList[i]._TileType == TileType.Mountain || _PlayerProductingTileList[i]._TileType == TileType.StoneMountain )  //철광
                {
                    tempEachIronP += tempAdditionalRes;
                    tempEachIronP += _PlayerProductingTileList[i]._TileGrade * CheckNearSpecialTownForMultipleProduct(_PlayerProductingTileList[i]);
                    tempTotalIronP += tempEachIronP;

                    _PlayerProductingTileList[i]._CurrentProductCount = tempEachIronP;  //현재 자원 타일의 생산량을 저장해준다.

                }
                else if (_PlayerProductingTileList[i]._TileType == TileType.FarmLand || _PlayerProductingTileList[i]._TileType == TileType.StoneFarm )  //푸드
                {
                    tempEachFoodP += tempAdditionalRes;
                    tempEachFoodP += _PlayerProductingTileList[i]._TileGrade * CheckNearSpecialTownForMultipleProduct(_PlayerProductingTileList[i]);
                    tempEachFoodP += CheckNearWaterForAdditionalProduct(_PlayerProductingTileList[i]);  // 푸드와 마을은 물을 더 강하게 요구한다.
                    tempTotalFoodP += tempEachFoodP;

                    _PlayerProductingTileList[i]._CurrentProductCount = tempEachFoodP;  //현재 자원 타일의 생산량을 저장해준다.

                }
                else if (_PlayerProductingTileList[i]._TileType == TileType.Town)  //타운
                {
                    tempEachGoldP += tempAdditionalRes;
                    tempEachGoldP += _PlayerProductingTileList[i]._TileGrade * 1;  //타운 전문 타운이 없으므로 1배수로 리턴.
                    tempEachGoldP += CheckNearWaterForAdditionalProduct(_PlayerProductingTileList[i]);  // 푸드와 마을은 물을 더 강하게 요구한다.
                    tempTotalGoldP += tempEachGoldP;

                    _PlayerProductingTileList[i]._CurrentProductCount = tempEachGoldP;  //현재 자원 타일의 생산량을 저장해준다.

                   // Debug.Log("  _PlayerProductingTileList[i]._CurrentProductCount " + _PlayerProductingTileList[i]._CurrentProductCount);
                }
            }
        }

        //토탈 가산 생산량과 성장 생산량도 적용해준다.
        _UnitProductIron = tempTotalIronP + _DataManager._UnitProductIronByStone +_DataManager. _ProductIronByHead ;
        _UnitProductWood = tempTotalWoodP + _DataManager._UnitProductWoodByStone + _DataManager._ProductWoodByHead;
        _UnitProductFood = tempTotalFoodP + _DataManager._UnitProductFoodByStone + _DataManager._ProductFoodByHead;
        _UnitProductGold = tempTotalGoldP;


        //if (_CurrOpeningWorldMap._IdealWorldMap.Count > 30)
        //{
        //    _CurrOpeningWorldMap._WorldValue = (int)((_CurrOpeningWorldMap._FoodMultiple * _UnitProductFood + _CurrOpeningWorldMap._IronMultiple * _UnitProductIron + _CurrOpeningWorldMap._WoodMultiple * _UnitProductWood + _UnitProductGold) * 0.5f * _CurrOpeningWorldMap._ExpandMultiple /_CurrOpeningWorldMap._IdealWorldMap.Count*30 ); //마지막 상수는 PER를 의미함

        //}
        //else
        //{
        _CurrOpeningWorldMap._WorldValue = Mathf.Max( (int)((_CurrOpeningWorldMap._FoodMultiple * _UnitProductFood + _CurrOpeningWorldMap._IronMultiple * _UnitProductIron + _CurrOpeningWorldMap._WoodMultiple * _UnitProductWood + _UnitProductGold) * 0.17f * _CurrOpeningWorldMap._ExpandMultiple)+_DataManager._ClearIslandCount ,1); //마지막 상수는 PER를 의미함
        //}
        CalculateScore();


    }

    void CalculateScore()
    {
       // _TotalScore = (_UnitProductIron + _UnitProductWood + _UnitProductFood + _UnitProductGold) * 10;
        //_PlayerTotalScoreText.text = _TotalScore + "";
        _LeftTilieText.text = _NotBoughtTileList.Count + "";
    }

    ResourceType _TempResType;
    public void GivePlayerResources()
    {
        _TempResType = new ResourceType();

        _PlayerFood += _UnitProductFood  ;
        _PlayerIron += _UnitProductIron  ;
        _PlayerWood += _UnitProductWood ;
        _PlayerGold += _UnitProductGold;

        for (int i = 0; i < _PlayerProductingTileList.Count; i++)
        {
            if (_PlayerProductingTileList[i]._TileType == TileType.Forest || _PlayerProductingTileList[i]._TileType == TileType.StoneForest) 
            {
                _TempResType = ResourceType.Wood;

            }
            else if (_PlayerProductingTileList[i]._TileType == TileType.Mountain || _PlayerProductingTileList[i]._TileType == TileType.StoneMountain )
            {
                _TempResType = ResourceType.Iron;

            }
            else if (_PlayerProductingTileList[i]._TileType == TileType.FarmLand || _PlayerProductingTileList[i]._TileType == TileType.StoneFarm)
            {
                _TempResType = ResourceType.Food;

            }
            else if (_PlayerProductingTileList[i]._TileType == TileType.Town)
            {
                _TempResType = ResourceType.Gold;

            }

            _PlayerProductingTileList[i]._UnitHexLand.ShowMSG(_TempResType, _PlayerProductingTileList[i]._CurrentProductCount, 0.01f, 1, new Vector3(Random.Range(-0.01f, 0.01f), 1.0f, 0), 30, new Vector3(0, -0.08f, 0));


        }

    }







    public void PlayEffectAffectedByThisTile(UnitTile _UnitTile)
    {
        for(int i=0; i<_CurrWorldMap.Count; i++)
        {
            _CurrWorldMap[i]._UnitHexLand._FlowerEffectLong.Stop();

        }
        
        if (_UnitTile._TileType == TileType.Town)
        {
            for (int i = 0; i < _CurrWorldMap.Count; i++)
            {
                if (DistBetweenPointR(_UnitTile._UnitHexLand.transform.position,   _CurrWorldMap[i]._UnitHexLand.transform.position) < 30)
                {
                    if (_UnitTile != _CurrWorldMap[i])
                    {
                        if (
                            (_CurrWorldMap[i]._TileType == TileType.FarmLand || _CurrWorldMap[i]._TileType == TileType.Forest || _CurrWorldMap[i]._TileType == TileType.Mountain
                            || _CurrWorldMap[i]._TileType == TileType.StoneFarm|| _CurrWorldMap[i]._TileType == TileType.StoneForest|| _CurrWorldMap[i]._TileType == TileType.StoneMountain) 
                            && _CurrWorldMap[i]._TileGrade >= 1
                            )
                        {
                           if (!_CurrWorldMap[i]._UnitHexLand._FlowerEffect.isPlaying)
                                _CurrWorldMap[i]._UnitHexLand._FlowerEffectLong.Play();
                        }
                    }
                }
            }
        }
        else if (_UnitTile._TileType == TileType.TownForest )
        {
            for (int i = 0; i < _CurrWorldMap.Count; i++)
            {
                if (DistBetweenPointR(_UnitTile._UnitHexLand.transform.position, _CurrWorldMap[i]._UnitHexLand.transform.position) < 230)
                {
                    if (_UnitTile != _CurrWorldMap[i])
                    {
                        if (
                            (_CurrWorldMap[i]._TileType == TileType.Forest || _CurrWorldMap[i]._TileType == TileType.StoneForest)
                            && _CurrWorldMap[i]._TileGrade >= 1
                            )
                        {
                            if (!_CurrWorldMap[i]._UnitHexLand._FlowerEffect.isPlaying)
                                _CurrWorldMap[i]._UnitHexLand._FlowerEffectLong.Play();
                        }
                    }
                }
            }


        }
        else if (_UnitTile._TileType == TileType.TownMountain)
        {
            for (int i = 0; i < _CurrWorldMap.Count; i++)
            {
                if (DistBetweenPointR(_UnitTile._UnitHexLand.transform.position, _CurrWorldMap[i]._UnitHexLand.transform.position) < 230)
                {
                    if (_UnitTile != _CurrWorldMap[i])
                    {
                        if (
                            (_CurrWorldMap[i]._TileType == TileType.Mountain|| _CurrWorldMap[i]._TileType == TileType.StoneMountain)
                            && _CurrWorldMap[i]._TileGrade >= 1
                            )
                        {
                            if (!_CurrWorldMap[i]._UnitHexLand._FlowerEffect.isPlaying)
                                _CurrWorldMap[i]._UnitHexLand._FlowerEffectLong.Play();
                        }
                    }
                }
            }


        }
        else if (_UnitTile._TileType == TileType.TownFarm)
        {
            for (int i = 0; i < _CurrWorldMap.Count; i++)
            {
                if (DistBetweenPointR(_UnitTile._UnitHexLand.transform.position, _CurrWorldMap[i]._UnitHexLand.transform.position) < 230)
                {
                    if (_UnitTile != _CurrWorldMap[i])
                    {
                        if (
                            (_CurrWorldMap[i]._TileType == TileType.FarmLand || _CurrWorldMap[i]._TileType == TileType.StoneFarm)
                            && _CurrWorldMap[i]._TileGrade >= 1
                            )
                        {
                            if (!_CurrWorldMap[i]._UnitHexLand._FlowerEffect.isPlaying)
                                _CurrWorldMap[i]._UnitHexLand._FlowerEffectLong.Play();
                        }
                    }
                }
            }
        }
        else if (_UnitTile._TileType == TileType.Water)
        {
            for (int i = 0; i < _CurrWorldMap.Count; i++)
            {
                if (DistBetweenPointR(_UnitTile._UnitHexLand.transform.position, _CurrWorldMap[i]._UnitHexLand.transform.position) < 30)
                {
                    if (_UnitTile != _CurrWorldMap[i])
                    {
                        if (
                            (_CurrWorldMap[i]._TileType == TileType.FarmLand || _CurrWorldMap[i]._TileType == TileType.StoneFarm || _CurrWorldMap[i]._TileType == TileType.Town)
                            && _CurrWorldMap[i]._TileGrade >= 1
                            )
                        {
                            if (!_CurrWorldMap[i]._UnitHexLand._FlowerEffect.isPlaying)
                                _CurrWorldMap[i]._UnitHexLand._FlowerEffectLong.Play();
                        }
                    }
                }
            }
        }
    }




    int CheckNearTownForAdditionalProduct(UnitTile _UnitTile)  //주변에 타운이 있으면 해당 그레이드 리턴
    {
        int _return = 0;
        _UnitTile._Is2XBoosting = false;

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (DistBetweenPointR(_UnitTile._UnitHexLand.transform.position,
           _CurrWorldMap[i]._UnitHexLand.transform.position) < 30)
            {
                if (_UnitTile != _CurrWorldMap[i])
                {
                    if (_CurrWorldMap[i]._TileType == TileType.Town && _CurrWorldMap[i]._TileGrade >= 1)
                    {
                        _UnitTile._Is2XBoosting = true;
   
                        _return += _CurrWorldMap[i]._TileGrade;  //마을 효과 중복해주는 방향으로 수정

                    }
                }
            }
        }

        return _return;
    }

    int CheckNearSpecialTownForMultipleProduct(UnitTile _UnitTile)  //주변에 특수 타운이 있으면 해당 그레이드 리턴
    {
        int _return = 1; // 배수 리턴이므로 1을 적용
        _UnitTile._Is2XBoosting = false;

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (DistBetweenPointR(_UnitTile._UnitHexLand.transform.position,
           _CurrWorldMap[i]._UnitHexLand.transform.position) < 230)  //주변 3칸 이내로 확대. 확실한 베너핏을 준다.
            {
                if (_UnitTile != _CurrWorldMap[i])
                {
                    if (_UnitTile._TileType == TileType.Forest || _UnitTile._TileType == TileType.StoneForest )
                    {
                        if (_CurrWorldMap[i]._TileType == TileType.TownForest && _CurrWorldMap[i]._TileGrade >= 1)
                        {
                            _UnitTile._Is2XBoosting = true;
                            _return = _return * (_CurrWorldMap[i]._TileGrade + 1);  //특수 마을 효과도 중복을 해보자. 곱하기로..

                        }
                    }
                    else if (_UnitTile._TileType == TileType.Mountain || _UnitTile._TileType == TileType.StoneMountain )
                    {
                        if (_CurrWorldMap[i]._TileType == TileType.TownMountain && _CurrWorldMap[i]._TileGrade >= 1)
                        {
                            _UnitTile._Is2XBoosting = true;
                            _return = _return * (_CurrWorldMap[i]._TileGrade + 1);  //특수 마을 효과도 중복을 해보자. 곱하기로..

                        }
                    }
                    else if (_UnitTile._TileType == TileType.FarmLand || _UnitTile._TileType == TileType.StoneFarm )
                    {
                        if (_CurrWorldMap[i]._TileType == TileType.TownFarm && _CurrWorldMap[i]._TileGrade >= 1)
                        {
                            _UnitTile._Is2XBoosting = true;
                            _return = _return * (_CurrWorldMap[i]._TileGrade + 1);  //특수 마을 효과도 중복을 해보자. 곱하기로..


                        }
                    }
                }
            }
        }

        return _return;
    }

    int CheckNearWaterForAdditionalProduct(UnitTile _UnitTile)  //주변에 물(강)이 있으면 해당 그레이드 리턴
    {
        int _return = 0;
        _UnitTile._Is2XBoosting = false;

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (DistBetweenPointR(_UnitTile._UnitHexLand.transform.position,
           _CurrWorldMap[i]._UnitHexLand.transform.position) < 30)
            {
                if (_UnitTile != _CurrWorldMap[i])
                {
                    if (_CurrWorldMap[i]._TileType == TileType.Water )
                    {
                        _UnitTile._Is2XBoosting = true;
                            _return = 1;  //물 타일 효과는 중복이 불가능 하고 전부 1이므로, 바로 1리턴 하고 브레이크.

                        break;

                    }
                }
            }
        }

        return _return;
    }





    //클리어 쪽 로직
    public void CheckIsStageCleared()
    {
        
        for (int i = 0; i < _IdealWorldMapList.Count; i++) //전체 리스트에서 체크해서 지금 플레이하고 있는 맵의 소유 여부를 정해준다.
        {
            if (_IdealWorldMapList[i]._Num == _CurrOpeningWorldMap._Num)
            {
               // Debug.Log("_IdealWorldMapList[i]._Num " + _IdealWorldMapList[i]._Num);
            }
        }


        if (_NotBoughtTileList.Count == 0)
        {
            StageCleared(); //스테이지 클리어됨.
        }

    }

    void StageCleared() //클리어 처리
    {
        // Debug.Log("_StageIsCleared");
        _DataManager._BGMManager._CurrAudioSource.Stop();
        _DataManager._BGMManager.SoundPlay((int)Random.Range(11,13));

        //랭킹 점수를 발송한다.
        _DataManager.ReportScoreToLandValueRank(_CurrOpeningWorldMap._WorldValue);

        _CurrOpeningWorldMap._EarningGoldPerHour = Mathf.Max(1, (int)((_UnitProductGold+( _UnitProductFood ) * _CurrOpeningWorldMap._FoodMultiple + ( _UnitProductIron) * _CurrOpeningWorldMap._IronMultiple + (_UnitProductWood) *_CurrOpeningWorldMap._WoodMultiple)*0.17f* _CurrOpeningWorldMap._ExpandMultiple));
        // _DataManager._UserCoin += _CurrOpeningWorldMap._EarningGoldPerHour;

        _DataManager._ClearIslandCount++;
        _DataManager.ReportScoreToManyIsland(_DataManager._ClearIslandCount); //얼마나 많은 섬을 개척했는지
        CallResultUI(); //결과 팝업을 불러준다.


        for(int i=0; i<_CurrOpeningWorldMap._IdealWorldMap.Count; i++) //현재 월드에서 스톤 데이터들을 저장해서 커렌트 오프닝 월드로 넘겨준다.
        {
            if (_CurrWorldMap[i]._TileType == TileType.StoneForest)
            {
                _CurrOpeningWorldMap._UnitProductWoodByStoneInIsland += _CurrWorldMap[i]._TileGrade;

            }
            else if (_CurrWorldMap[i]._TileType == TileType.StoneMountain)
            {
                _CurrOpeningWorldMap._UnitProductIronByStoneInIsland += _CurrWorldMap[i]._TileGrade;

            }
            else if (_CurrWorldMap[i]._TileType == TileType.StoneFarm)
            {
                _CurrOpeningWorldMap._UnitProductFoodByStoneInIsland += _CurrWorldMap[i]._TileGrade;

            }

        }


        for (int i = 0; i < _IdealWorldMapList.Count; i++) //전체 리스트에서 체크해서 지금 플레이하고 있는 맵의 소유 여부를 정해준다.
        {
            if (_IdealWorldMapList[i]._Num == _CurrOpeningWorldMap._Num)
            {
                // _CurrOpeningWorldMap = null;

                _IdealWorldMapList[i]._IdealWorldMap = ExportCurrWorldMap(_CurrWorldMap); //지금 플레이한 맵을 뽑아서 저장할 수 있게 해주자.
                _IdealWorldMapList[i]._IsOwned = true;
                _IdealWorldMapList[i]._WorldValue = _CurrOpeningWorldMap._WorldValue;
                _IdealWorldMapList[i]._EarningGoldPerHour = _CurrOpeningWorldMap._EarningGoldPerHour;

                _IdealWorldMapList[i]._UnitProductWoodByStoneInIsland = _CurrOpeningWorldMap._UnitProductWoodByStoneInIsland;
                _IdealWorldMapList[i]._UnitProductIronByStoneInIsland = _CurrOpeningWorldMap._UnitProductIronByStoneInIsland;
                _IdealWorldMapList[i]._UnitProductFoodByStoneInIsland = _CurrOpeningWorldMap._UnitProductFoodByStoneInIsland;

                break;
            }
        }
        _DataManager.SavePlayerData();

    }

    public void CallResultUI()
    {
    
        _IsGlobalPause = true;
        _ResultWindow.SetActive(true);
        _ResultGoldPerHourText.text = " + " + _CurrOpeningWorldMap._EarningGoldPerHour + "/ 수금";
        _ResultValueText.text = "" + _CurrOpeningWorldMap._WorldValue;

    }

    public void CallLeaderboard(int num)
    {

        _DataManager.PlzShowLeaderboardUI(num);
    }







    //기능
    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.z - _refer.z) * (_target.z - _refer.z);

    }


    void InitMaterialColorSetting() //색상 머터리얼들의 초기값을 세팅해주고 나중에 여기서 넣어주자.
    {
        _WhiteRefMaterials = new Material[] { _WhiteRefMaterial };
        _YellowRefMaterials = new Material[] { _YellowRefMaterial };
        _RedRefMaterials = new Material[] { _RedRefMaterial };

    }

    public void ChangeMaterialColor(MeshRenderer _MeshRenderer, string _ColorString)  //대상 메쉬렌더러의 색을 바꿔줌. 색상은 대문자로.
    {
        if (_ColorString == "WHITE")
        {
            _MeshRenderer.materials = _WhiteRefMaterials;
        }
        else if (_ColorString == "YELLOW")
        {
            _MeshRenderer.materials = _YellowRefMaterials;
        }
        else if (_ColorString == "RED")
        {
            _MeshRenderer.materials = _RedRefMaterials;
        }
    }









    //관리 기능
    public AnimatedButton _CallUpUIGoToWorldBTN;
    public GameObject _UIGoToWorld;

    public void CallUIGoToWorld()
    {
        if (_IsMenuOn)
            return;

        _IsMenuOn = true;
        _IsGlobalPause = true;
        _DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

        _UIGoToWorld.SetActive(true);

    }

    public void CloseUIGoToWorld()
    {
        _IsGlobalPause = false;
        _UIGoToWorld.SetActive(false);
        _DataManager._BGMManager.SoundPlay(5);
        _IsMenuOn = false;

    }

    public void GoToWorld()
    {
        if (_CurrOpeningWorldMap._IsOwned)  //이미 소유하고 있는 상태에서만.. 저장해주는 로직.
        {
            for (int i = 0; i < _IdealWorldMapList.Count; i++) //전체 리스트에서 체크해서 지금 플레이하고 있는 맵의 소유 여부를 정해준다.
            {
                if (_IdealWorldMapList[i]._Num == _CurrOpeningWorldMap._Num)
                {
                    _CurrOpeningWorldMap = null;

                    _IdealWorldMapList[i]._IdealWorldMap = ExportCurrWorldMap(_CurrWorldMap); //지금 플레이한 맵을 뽑아서 저장할 수 있게 해주자.
                    _IdealWorldMapList[i]._IsOwned = true;
                    _IdealWorldMapList[i]._EarningGoldPerHour = _CurrOpeningWorldMap._EarningGoldPerHour;
                    break;
                }
            }
        }
        _DataManager._BGMManager.SoundPlay(7);
        _DataManager.SavePlayerData();
        SceneManager.LoadScene(0);
    }

    public AnimatedButton _RestartBTN; // 골드 부족할 때 막아준다.
    public GameObject _RestartBTNCover; // 골드 부족할 때 막아준다.

    
    public GameObject _UIRestartGame;
    public AnimatedButton _RestartConfirmBTN;
    public GameObject _RestartConfirmBTNCover; 

    public Text _PlayCoinText;
    public Text _CurrentCoinText;

    public void CallUIRestartGame()
    {
        if (_IsMenuOn)
            return;

        _IsMenuOn = true;
        _IsGlobalPause = true;
        _DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));
        _UIRestartGame.SetActive(true);

    }

    public void CloseUIRestartGame()
    {
        _IsGlobalPause = false;
        _UIRestartGame.SetActive(false);
        _DataManager._BGMManager.SoundPlay(5);

        _IsMenuOn = false;
    }


    public void ReStartBTN()
    {
        if (_DataManager._UserCoin >= _CurrOpeningWorldMap._EntranceGoldFee)
        {
            _DataManager._UserCoin -= _CurrOpeningWorldMap._EntranceGoldFee;
            _DataManager.SavePlayerData();
            _DataManager._BGMManager.SoundPlay(40);

            SceneManager.LoadScene(1);

        }
        else
        {
            _DataManager.ShowVideoAd(0);


        }
        
    }


    public List<UnitTileIdeal> ExportCurrWorldMap(List<UnitTile> _target)// currentWorldMap을 dulplicated버젼으로 바꿔준다.
    {
        List<UnitTileIdeal> _return = new List<UnitTileIdeal>();
        UnitTileIdeal _tempUnitTileIdeal;

        for (int i = 0; i < _target.Count; i++)
        {
            _tempUnitTileIdeal = new UnitTileIdeal();
            _tempUnitTileIdeal._Num = _target[i]._Num;
            _tempUnitTileIdeal._PosX = _target[i]._PosX;
            _tempUnitTileIdeal._PosZ = _target[i]._PosZ;
            _tempUnitTileIdeal._TileGrade = _target[i]._TileGrade;
            _tempUnitTileIdeal._TileType = _target[i]._TileType;

            _tempUnitTileIdeal._IsDiscovered = _target[i]._IsDiscovered;
            _tempUnitTileIdeal._IsBoughtLand = _target[i]._IsBoughtLand;
            _tempUnitTileIdeal._IsGenesisTile = _target[i]._IsGenesisTile;
            _tempUnitTileIdeal._TilePrice = _target[i]._TilePrice;
            _tempUnitTileIdeal._TilePriceType = _target[i]._TilePriceType;
            _tempUnitTileIdeal._UniqueFingerPrintNumber = _target[i]._UniqueFingerPrintNumber;
            _return.Add(_tempUnitTileIdeal);
        }
        return _return;
    }


    public List<UnitTile> ImportIdealWorldMap(List<UnitTileIdeal> _target)  //duplicated된 Ideal월드맵을 현재 월드맵으로 바꿔준다. 이건 인게임 로딩 및 월드맵에서 필요하다.  테스트 완료
    {
        List<UnitTile> _return = new List<UnitTile>();
        UnitTile _tempUnitTile;
        if (_target == null)
        {
            return _return;
        }
        for (int i = 0; i < _target.Count; i++)
        {
            _tempUnitTile = new UnitTile();
            _tempUnitTile._Num = _target[i]._Num;
            _tempUnitTile._PosX = _target[i]._PosX;
            _tempUnitTile._PosZ = _target[i]._PosZ;

            _tempUnitTile._TileGrade = _target[i]._TileGrade;
            _tempUnitTile._TileType = _target[i]._TileType;
            _tempUnitTile._UnitHexLand = new UnitHexLand();

            _tempUnitTile._IsDiscovered = _target[i]._IsDiscovered;
            _tempUnitTile._IsBoughtLand = _target[i]._IsBoughtLand;
            _tempUnitTile._IsGenesisTile = _target[i]._IsGenesisTile;

            _tempUnitTile._TilePrice = _target[i]._TilePrice;
            _tempUnitTile._TilePriceType = _target[i]._TilePriceType;
            _tempUnitTile._UniqueFingerPrintNumber = _target[i]._UniqueFingerPrintNumber;


            _return.Add(_tempUnitTile);  //near타일 찾는건 함수로 따로 해주고.. 생산타일 추가하는것도 따로 해주자.
        }

        return _return;
    }
    
    public void ScanAndArragementCurrentMap()  //월드맵 에서 제너시스 타일을 찾아서 세팅해주는 등 타일들을 정리해주자.
    {
        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (_CurrWorldMap[i]._IsGenesisTile)
            {
                _TheGenesisTile = _CurrWorldMap[i];
                if (_CurrOpeningWorldMap._Grade <= 5)
                {
                    _TheGenesisTile._TileType = TileType.Town;
                    _TheGenesisTile._TileGrade = 1;
                }
            }

            if (true) //초반에 추가해줄 때는 일단 모든 생산타일을 넣어줘도 될듯?
            {
                //생산 타일 추가.
                if (_CurrWorldMap[i]._TileType == TileType.Town)
                {
                    _PlayerProductingTileList.Add(_CurrWorldMap[i]);

                }
                else if (_CurrWorldMap[i]._TileType == TileType.Forest || _CurrWorldMap[i]._TileType == TileType.Mountain || _CurrWorldMap[i]._TileType == TileType.FarmLand
                     || _CurrWorldMap[i]._TileType == TileType.StoneForest || _CurrWorldMap[i]._TileType == TileType.StoneMountain || _CurrWorldMap[i]._TileType == TileType.StoneFarm)
                {
                    if (_CurrWorldMap[i]._TileGrade >= 1)
                    {
                        _PlayerProductingTileList.Add(_CurrWorldMap[i]);
                    }
                }
            }
        }

    }

    public GameObject _UIBurnIsland;
    public void CallUIBurnIsland()
    {
        if (_IsMenuOn)
            return;
        _IsMenuOn = true;

        _UIBurnIsland.SetActive(true);
        _DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));

    }

    public void CloseUIBurnIsland()
    {
        _UIBurnIsland.SetActive(false);
        _DataManager._BGMManager.SoundPlay(5);
        _IsMenuOn = false;

    }


    public void BurnAllBTN() //타일을 다 태워버리고 월드를 지워버리는 버튼.  일단 다 태우면 재만 남게 하자.
    {
        _IsGlobalPause = true;

        for(int i=0; i<_CurrWorldMap.Count; i++)
        {
            _CurrWorldMap[i]._UnitHexLand.BurnThisTile();

        }

        StartCoroutine(AfterBurnReLoadWorld());
        _BurnBTN.gameObject.SetActive(false);
        CloseUIBurnIsland();
        _RestartBTN.gameObject.SetActive(false);
        _RestartBTNCover.gameObject.SetActive(false);
        _TimerRoot.SetActive(false);
        _CallUpUIGoToWorldBTN.gameObject.SetActive(false);
        _DataManager._BGMManager.SoundPlay(7);
    }
    
    IEnumerator AfterBurnReLoadWorld()
    {
        yield return new WaitForSeconds(27);


        for (int i = 0; i < _IdealWorldMapList.Count; i++)
        {
            if (_IdealWorldMapList[i]._Num == _CurrOpeningWorldMap._Num)
            {
                _IdealWorldMapList.Remove(_IdealWorldMapList[i]);
                Debug.Log("del success");
                break;
            }

        }

        _DataManager.SavePlayerData();
        yield return new WaitForSeconds(0.2f);
        GoToWorld();

    }




}


