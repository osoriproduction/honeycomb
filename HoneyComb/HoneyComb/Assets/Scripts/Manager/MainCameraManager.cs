﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraManager : MonoBehaviour {

    public GameManager _GameManager;
    public float _Low;   //구 반지름
    public float _Pie;  // 천정각 파이.
    public float _Theta;  //theta는 라디안 디멘션임.  얘를 조정

    public float _varX;
    public float _varY;
    public float _varZ;
      

    public Vector3 _MedPosVector3;
    public Vector3 _MedRotVector3;


    public bool _OnTitleScene;

    void Start () {

        if (_OnTitleScene)
        {
            _Low = 80;

        }
        else
        {
            _Low = Mathf.Sqrt(23 * 23 + 16 * 16);

        }
        _Pie = Mathf.Deg2Rad * 40f;
        _MedPosVector3 = new Vector3(0, 0, 0);
        _MedRotVector3 = new Vector3(0, 0, 0);
        _Theta = -90 * Mathf.Deg2Rad;

    }



    void Update()
    {
       // _Theta += Random.Range(0,1f);
        _MedRotVector3.x = 60;
        _MedRotVector3.y = -1 * _Theta * Mathf.Rad2Deg - 90;
        _MedRotVector3.z = 0;
        this.transform.localRotation = Quaternion.Euler(_MedRotVector3);
        
        _MedPosVector3.x = _Low * Mathf.Sin(_Pie) * Mathf.Cos(_Theta) + _varX;
        _MedPosVector3.y = _Low * Mathf.Cos(_Pie) + _varY;
        _MedPosVector3.z = _Low * Mathf.Sin(_Pie) * Mathf.Sin(_Theta) + _varZ;
        
        this.transform.localPosition = _MedPosVector3;
    }



    public void MoveThisCamera( float _dX , float _dZ)
    {
        _varX += _dX * Mathf.Cos(this.transform.localRotation.eulerAngles.y * Mathf.Deg2Rad) + _dZ * Mathf.Sin(this.transform.localRotation.eulerAngles.y * Mathf.Deg2Rad);
        _varZ+= _dZ * Mathf.Cos(this.transform.localRotation.eulerAngles.y * Mathf.Deg2Rad) - _dX * Mathf.Sin(this.transform.localRotation.eulerAngles.y * Mathf.Deg2Rad);

        
    }

    public void SetPosThisCamera(float _dX,float _dY, float _dZ, float _dLow)
    {
        _varX = _dX * Mathf.Cos(this.transform.localRotation.eulerAngles.y * Mathf.Deg2Rad) + _dZ * Mathf.Sin(this.transform.localRotation.eulerAngles.y * Mathf.Deg2Rad);
        _varY = _dY;
        _varZ = _dZ * Mathf.Cos(this.transform.localRotation.eulerAngles.y * Mathf.Deg2Rad) - _dX * Mathf.Sin(this.transform.localRotation.eulerAngles.y * Mathf.Deg2Rad);
        _Low = _dLow;
    }


}
