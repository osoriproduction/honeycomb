﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;





public class UnitUpgrade  //헤드쿼터에서 업그레이드할 때의 유닛 업그레이드.
{
    public int _TypeNum;
    public string _Header;
    public string _Des;

    public int _CurrLevel;
    public int _CurrValue;
    public int _NeedCoinToUpgrade;

    public UnitUpgrade()
    {

    }

    public UnitUpgrade( int _TypeNum, string _Header, string _Des, int _CurrLevel, int _CurrValue, int _NeedCoinToUpgrade)
    {
        this._TypeNum = _TypeNum;
        this._Header = _Header;
        this._Des = _Des;
        this._CurrLevel = _CurrLevel;
        this._CurrValue = _CurrValue;
        this._NeedCoinToUpgrade = _NeedCoinToUpgrade;

    }

}

public class DataManager : MonoBehaviour {  //싱글톤임
    
    private static DataManager instance = null; //정적 변수
    public static DataManager Instance
    {  //인스턴스 접근 프로퍼티
        get
        {
            return instance;
        }
    }

    //외부입력
    public BGMManager _BGMManager;



    //내부입력
    public string _UserIDName;
    public int _UserCoin;  //이게 바로 HC임.
    public int _CountMakeIsland;  //섬이 생성된 개수. 계속해서 지속적으로 쌓여간다.

    public bool _IAmNotFirst; //뉴비면 튜토리얼을 보여준다. 

    public bool _IsKorean; // true: korean, false: eng
    public bool _AlreadyReview;
    public bool _PurchasedNoAD;
    public int _ClearIslandCount;

    public bool _VibrateOn;


    //비행석 관련. 전체 통합 생산량.
    public int _UnitProductWoodByStone;
    public int _UnitProductIronByStone;
    public int _UnitProductFoodByStone;

    


    //수금 관련
    public int _TotalRevenueCoinPerTick; //현재 총 골드 생산량
    public float _TickTimeToCollectCoin;//콜렉팅 타임
    public float _RemainedTime; //콜렉트까지 남은 시간
    public System.DateTime _CollectAvailableTime; //콜렉트 가능한 시간


    //헤드쿼터 업그레이드 관련
    public List<UnitUpgrade> _CurrUpgradeList;
    public List<int> _UpgradedCurrLevel;  //타입별로 저장

    

    //시스템
    public int _SceneNum; //0이면 타이틀. 1이면 인게임 메인

    public GameManager _GameManager;
    public TitleManager _TitleManager;


    //GPGS
    PlayGamesClientConfiguration _GPGConfig;




    void Start()
    {
        if (Application.systemLanguage == SystemLanguage.Korean)
        {
            _IsKorean = true;
        }
        else
        {
            _IsKorean = false;
        }
        if (instance)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);


        //여기서부터 본론
        _UserCoin = 10;
        InputUpgradeList();

        InputTileTypeList();
        InputTileCombineList();
        InputListOfAllowedTileTypeList();

        StartCoroutine(GPGSInit());

        Advertisement.Initialize("2836242");

    }

    IEnumerator GPGSInit()
    {
        //gpgs-and only
        yield return new WaitForSeconds(0.02f);

        _GPGConfig = new PlayGamesClientConfiguration.Builder()
            .EnableSavedGames()
            .Build();

        PlayGamesPlatform.InitializeInstance(_GPGConfig);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();

        SignIn();  //일단 로그인 시키고.
        yield return new WaitForSeconds(0.1f);



    }


    void InputUpgradeList()
    {
        _UpgradedCurrLevel = new List<int>();
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);

        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);
        _UpgradedCurrLevel.Add(0);

        _CurrUpgradeList = new List<UnitUpgrade>();
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "레벨업 할 경우, 게임 안에서 매턴 수확되는 나무 생산량이 증가됩니다. ", 0, 0, 15));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 1, 1, 20));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 2, 2, 30));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 3, 3, 50));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 4, 4, 70));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 5, 5, 100));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 6, 6, 150));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 7, 7, 200));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 8, 8, 300));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 9, 9, 500));
        _CurrUpgradeList.Add(new UnitUpgrade(0, "나무 생산 기술 ", "게임 안에서 매턴 수확되는 나무 생산량을 증가시켜줍니다. ", 10, 10, 99999));
     

        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "레벨업 할 경우, 게임 안에서 매턴 수확되는 철 생산량이 증가됩니다. ", 0, 0, 15));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 1, 1, 20));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 2, 2, 30));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 3, 3, 50));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 4, 4, 70));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 5, 5, 100));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 6, 6, 150));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 7, 7, 200));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 8, 8, 300));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 9, 9, 500));
        _CurrUpgradeList.Add(new UnitUpgrade(1, "철 생산 기술 ", "게임 안에서 매턴 수확되는 철 생산량을 증가시켜줍니다. ", 10, 10, 99999));


        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "레벨업 할 경우, 게임 안에서 매턴 수확되는 식량 생산량이 증가됩니다. ", 0, 0, 15));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 1, 1, 20));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 2, 2, 30));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 3, 3, 50));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 4, 4, 70));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 5, 5, 100));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 6, 6, 150));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 7, 7, 200));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 8, 8, 300));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 9, 9, 500));
        _CurrUpgradeList.Add(new UnitUpgrade(2, "식량 생산 기술 ", "게임 안에서 매턴 수확되는 식량 생산량을 증가시켜줍니다. ", 10, 10, 99999));


        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "레벨업 할 경우, 수금 시 획득하는 코인의 양이 추가로 증가합니다. ", 0, 0, 15));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다.", 1, 1, 20));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 2, 2, 30));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 3, 3, 50));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 4, 4, 70));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 5, 5, 100));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 6, 6, 150));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 7, 7, 200));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 8, 8, 300));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 9, 9, 500));
        _CurrUpgradeList.Add(new UnitUpgrade(3,"효율적 코인 수금", "수금 시 획득하는 코인의 양이 추가로 증가합니다 ", 10, 10, 99999));
        
        _CurrUpgradeList.Add(new UnitUpgrade(4, "하늘 탐색 기법", "발견할 수 있는 하늘 섬의 수가 증가합니다. ", 0, 0, 15));
        _CurrUpgradeList.Add(new UnitUpgrade(4, "하늘 탐색 기법", "발견할 수 있는 하늘 섬의 수가 증가합니다.", 1, 1, 30));
        _CurrUpgradeList.Add(new UnitUpgrade(4, "하늘 탐색 기법", "발견할 수 있는 하늘 섬의 수가 증가합니다. ", 2, 2, 50));
        _CurrUpgradeList.Add(new UnitUpgrade(4, "하늘 탐색 기법", "발견할 수 있는 하늘 섬의 수가 증가합니다. ", 3, 3, 100));
        _CurrUpgradeList.Add(new UnitUpgrade(4, "하늘 탐색 기법", "발견할 수 있는 하늘 섬의 수가 증가합니다. ", 4, 4, 200));
        _CurrUpgradeList.Add(new UnitUpgrade(4, "하늘 탐색 기법", "발견할 수 있는 하늘 섬의 수가 증가합니다. ", 5, 5, 99999));
                
        _CurrUpgradeList.Add(new UnitUpgrade(5, "알뜰한 시작", "레벨업 할 경우, 게임 시작 시, 무료 타일 수가 증가합니다. ", 0, 0, 20));
        _CurrUpgradeList.Add(new UnitUpgrade(5, "알뜰한 시작", "게임 시작 시, 무료 타일 수가 증가합니다.", 1, 1, 30));
        _CurrUpgradeList.Add(new UnitUpgrade(5, "알뜰한 시작", "게임 시작 시, 무료 타일 수가 증가합니다. ", 2, 2, 50));
        _CurrUpgradeList.Add(new UnitUpgrade(5, "알뜰한 시작", "게임 시작 시, 무료 타일 수가 증가합니다. ", 3, 3, 100));
        _CurrUpgradeList.Add(new UnitUpgrade(5, "알뜰한 시작", "게임 시작 시, 무료 타일 수가 증가합니다. ", 4, 4, 200));
        _CurrUpgradeList.Add(new UnitUpgrade(5, "알뜰한 시작", "게임 시작 시, 무료 타일 수가 증가합니다.", 5, 5, 99999));
    }


    public List<UnitTileType> _TileTypeList; //타일 타입 리스트. 설명을 위해서 별도로 입력해준다.
    void InputTileTypeList()
    {
        _TileTypeList = new List<UnitTileType>();
        _TileTypeList.Add(new UnitTileType(TileType.Forest, 0, "거친 숲 섬", "숲이 울창한 섬입니다. 세 개를 모아서 목재소를 건설할 수 있습니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.Forest, 1, "숲 섬 Lv.1", "목재소가 있는 숲 섬입니다. 더 낮은 레벨의 다른 숲 섬과 자리를 바꿀 수 있습니다.", "현재 나무 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Forest, 2, "숲 섬 Lv.2", "목재소가 있는 숲 섬입니다. 더 낮은 레벨의 다른 숲 섬과 자리를 바꿀 수 있습니다.", "현재 나무 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Forest, 3, "숲 섬 Lv.3", "목재소가 있는 숲 섬입니다. 더 낮은 레벨의 다른 숲 섬과 자리를 바꿀 수 있습니다.", "현재 나무 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Forest, 4, "숲 섬 Lv.4", "목재소가 있는 숲 섬입니다. 더 낮은 레벨의 다른 숲 섬과 자리를 바꿀 수 있습니다.", "현재 나무 생산량: "));
        

        _TileTypeList.Add(new UnitTileType(TileType.Mountain, 0, "야산 섬", "인적이 드문 야산 섬입니다. 세 개를 모아서 광산 섬을 건설할 수 있습니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.Mountain, 1, "광산 섬 Lv.1", "철을 생산하는 광산 섬입니다. 더 낮은 레벨의 다른 광산 섬과 자리를 바꿀 수 있습니다. ", "현재 철 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Mountain, 2, "광산 섬 Lv.2", "철을 생산하는 광산 섬입니다. 더 낮은 레벨의 다른 광산 섬과 자리를 바꿀 수 있습니다.", "현재 철 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Mountain, 3, "광산 섬 Lv.3", "철을 생산하는 광산 섬입니다. 더 낮은 레벨의 다른 광산 섬과 자리를 바꿀 수 있습니다.", "현재 철 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Mountain, 4, "광산 섬 Lv.4", "철을 생산하는 광산 섬입니다. 더 낮은 레벨의 다른 광산 섬과 자리를 바꿀 수 있습니다.", "현재 철 생산량: "));


        _TileTypeList.Add(new UnitTileType(TileType.FarmLand, 0, "들판 섬", "아무것도 없는 들판 섬입니다. 세 개를 모아서 농장 섬을 건설할 수 있습니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.FarmLand, 1, "농장 섬 Lv.1", "식량을 생산하는 섬입니다. 더 낮은 레벨의 다른 농장 섬과 자리를 바꿀 수 있습니다. ", "현재 식량 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.FarmLand, 2, "농장 섬 Lv.2", "식량을 생산하는 섬입니다. 더 낮은 레벨의 다른 농장 섬과 자리를 바꿀 수 있습니다. ", "현재 식량 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.FarmLand, 3, "농장 섬 Lv.3", "식량을 생산하는 섬입니다. 더 낮은 레벨의 다른 농장 섬과 자리를 바꿀 수 있습니다. ", "현재 식량 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.FarmLand, 4, "농장 섬 Lv.4", "식량을 생산하는 섬입니다. 더 낮은 레벨의 다른 농장 섬과 자리를 바꿀 수 있습니다. ", "현재 식량 생산량: "));

        _TileTypeList.Add(new UnitTileType(TileType.Water, 0, "맑은 물", "맑은 물입니다. 더 낮은 레벨의 섬들을 삼켜서 강을 낼 수 있습니다. 또, 주변 1거리 이하의 마을과 농지의 생산량을 +1만큼 증가시켜 줍니다. 효과는 중복되지 않습니다.", null));  //물은 합쳐도 물이 됨. 뭔가 물을 이용한 걸 할 수도 있다.
        _TileTypeList.Add(new UnitTileType(TileType.Water, 1, "물 Lv.1", "맑은 물입니다. 더 낮은 레벨의 섬들을 삼켜서 강을 낼 수 있습니다. 또, 주변 1거리 이하의 마을과 농지의 생산량을 +2만큼 증가시켜 줍니다. 효과는 중복되지 않습니다.", null));  //물은 합쳐도 물이 됨. 뭔가 물을 이용한 걸 할 수도 있다.
        _TileTypeList.Add(new UnitTileType(TileType.Water, 2, "물 Lv.2", "맑은 물입니다. 더 낮은 레벨의 섬들을 삼켜서 강을 낼 수 있습니다. 또, 주변 1거리 이하의 마을과 농지의 생산량을 +3만큼 증가시켜 줍니다. 효과는 중복되지 않습니다.", null));  //물은 합쳐도 물이 됨. 뭔가 물을 이용한 걸 할 수도 있다.
        _TileTypeList.Add(new UnitTileType(TileType.Water, 3, "물 Lv.3", "맑은 물입니다. 더 낮은 레벨의 섬들을 삼켜서 강을 낼 수 있습니다. 또, 주변 1거리 이하의 마을과 농지의 생산량을 +4만큼 증가시켜 줍니다. 효과는 중복되지 않습니다.", null));  //물은 합쳐도 물이 됨. 뭔가 물을 이용한 걸 할 수도 있다.
        _TileTypeList.Add(new UnitTileType(TileType.Water, 4, "물 Lv.4", "맑은 물입니다. 더 낮은 레벨의 섬들을 삼켜서 강을 낼 수 있습니다. 또, 주변 1거리 이하의 마을과 농지의 생산량을 +5만큼 증가시켜 줍니다. 효과는 중복되지 않습니다.", null));  //물은 합쳐도 물이 됨. 뭔가 물을 이용한 걸 할 수도 있다.

        _TileTypeList.Add(new UnitTileType(TileType.Town, 1, "마을 섬 Lv.1", "더 낮은 레벨의 다른 섬과 자리를 바꿀 수 있습니다. 골드를 생산합니다. 또, 주변 1칸 거리의 생산 타일들의 생산량을 +1씩 증가시켜 줍니다.", "현재 골드 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Town, 2, "마을 섬 Lv.2", "더 낮은 레벨의 다른 섬과 자리를 바꿀 수 있습니다. 골드를 생산합니다. 또, 주변 1칸 거리의 생산 타일들의 생산량을 +2씩 증가시켜 줍니다.", "현재 골드 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Town, 3, "마을 섬 Lv.3", "더 낮은 레벨의 다른 섬과 자리를 바꿀 수 있습니다. 골드를 생산합니다. 또, 주변 1칸 거리의 생산 타일들의 생산량을 +3씩 증가시켜 줍니다.", "현재 골드 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Town, 4, "마을 섬 Lv.4", "더 낮은 레벨의 다른 섬과 자리를 바꿀 수 있습니다. 골드를 생산합니다. 또, 주변 1칸 거리의 생산 타일들의 생산량을 +4씩 증가시켜 줍니다.", "현재 골드 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Town, 5, "마을 섬 Lv.5", "더 낮은 레벨의 다른 섬과 자리를 바꿀 수 있습니다. 골드를 생산합니다. 또, 주변 1칸 거리의 생산 타일들의 생산량을 +5씩 증가시켜 줍니다.", "현재 골드 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.Town, 6, "마을 섬 Lv.6", "더 낮은 레벨의 다른 섬과 자리를 바꿀 수 있습니다. 골드를 생산합니다. 또, 주변 1칸 거리의 생산 타일들의 생산량을 +6씩 증가시켜 줍니다.", "현재 골드 생산량: "));
        

        _TileTypeList.Add(new UnitTileType(TileType.TownForest, 1, "목재 연구소 Lv.1", "목공 전문가들이 나무를 연구하는 공간입니다. 주변 3칸 거리의 숲 타일들의 생산량을 2배로 증가시켜줍니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.TownForest, 2, "목재 연구소 Lv.2", "목공 전문가들이 나무를 연구하는 공간입니다. 주변 3칸 거리의 숲 타일들의 생산량을 3배로 증가시켜줍니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.TownForest, 3, "목재 연구소 Lv.3", "목공 전문가들이 나무를 연구하는 공간입니다. 주변 3칸 거리의 숲 타일들의 생산량을 4배로 증가시켜줍니다.", null));

        _TileTypeList.Add(new UnitTileType(TileType.TownMountain, 1, "광업 연구소 Lv.1", "광업 전문가들이 철을 연구하는 공간입니다. 주변 3칸 거리의 광산 타일들의 생산량을 2배로 증가시켜줍니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.TownMountain, 2, "광업 연구소 Lv.2", "광업 전문가들이 철을 연구하는 공간입니다. 주변 3칸 거리의 광산 타일들의 생산량을 3배로 증가시켜줍니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.TownMountain, 3, "광업 연구소 Lv.3", "광업 전문가들이 철을 연구하는 공간입니다. 주변 3칸 거리의 광산 타일들의 생산량을 4배로 증가시켜줍니다.", null));

        _TileTypeList.Add(new UnitTileType(TileType.TownFarm, 1, "농업 연구소 Lv.1", "농업에 일가견이 있는 사람들이 작물 수확을 연구합니다. 주변 3칸 거리의 농장 타일들의 생산량을 2배로 증가시켜줍니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.TownFarm, 2, "농업 연구소 Lv.2", "농업에 일가견이 있는 사람들이 작물 수확을 연구합니다. 주변 3칸 거리의 농장 타일들의 생산량을 3배로 증가시켜줍니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.TownFarm, 3, "농업 연구소 Lv.3", "농업에 일가견이 있는 사람들이 작물 수확을 연구합니다. 주변 3칸 거리의 농장 타일들의 생산량을 4배로 증가시켜줍니다.", null));

        _TileTypeList.Add(new UnitTileType(TileType.FlyingStone, 0, "소량의 비행석", "비행석이 잔뜩 매장되어 있는 지역입니다. 비행석과 자원 타일을 합쳐서 무언가 만들 수 있을 것 같습니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.FlyingStone, 1, "비행석 채굴지 Lv.1", "비행석이 잔뜩 매장되어 있는 지역입니다. 비행석과 자원 타일을 합쳐서 무언가 만들 수 있을 것 같습니다.", null));
        _TileTypeList.Add(new UnitTileType(TileType.FlyingStone, 2, "비행석 채굴지 Lv.2", "비행석이 잔뜩 매장되어 있는 지역입니다. 비행석과 자원 타일을 합쳐서 무언가 만들 수 있을 것 같습니다.", null));

        _TileTypeList.Add(new UnitTileType(TileType.StoneForest, 1, "천공의 숲 섬 Lv.1", "비행석으로 강화되어 멀리 떨어져 있는 곳까지 자원을 보낼 수 있는 목재 섬입니다. 다른 섬 개척 시의 자원 생산량이 증가합니다.", "현재 나무 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.StoneForest, 2, "천공의 숲 섬 Lv.2", "비행석으로 강화되어 멀리 떨어져 있는 곳까지 자원을 보낼 수 있는 목재 섬입니다. 다른 섬 개척 시의 자원 생산량이 증가합니다. ", "현재 나무 생산량: "));

        _TileTypeList.Add(new UnitTileType(TileType.StoneMountain, 1, "천공의 광산 섬 Lv.1", "비행석으로 강화되어 멀리 떨어져 있는 곳까지 자원을 보낼 수 있는 광산 섬입니다. 다른 섬 개척 시의 자원 생산량이 증가합니다.", "현재 철 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.StoneMountain, 2, "천공의 광산 섬 Lv.2", "비행석으로 강화되어 멀리 떨어져 있는 곳까지 자원을 보낼 수 있는 광산 섬입니다. 다른 섬 개척 시의 자원 생산량이 증가합니다.", "현재 철 생산량: "));

        _TileTypeList.Add(new UnitTileType(TileType.StoneFarm, 1, "천공의 농장 섬 Lv.1", "비행석으로 강화되어 멀리 떨어져 있는 곳까지 자원을 보낼 수 있는 농장 섬입니다. 다른 섬 개척 시의 자원 생산량이 증가합니다. ", "현재 식량 생산량: "));
        _TileTypeList.Add(new UnitTileType(TileType.StoneFarm, 2, "천공의 농장 섬 Lv.2", "비행석으로 강화되어 멀리 떨어져 있는 곳까지 자원을 보낼 수 있는 농장 섬입니다. 다른 섬 개척 시의 자원 생산량이 증가합니다. ", "현재 식량 생산량: "));


        //열외
        _TileTypeList.Add(new UnitTileType(TileType.Town, 0, "마을 기본", "나오면 안되는 마을 타일입니다.", null));

    }


    public List<UnitCombine> _TileCombineList; //타일 컴바인 리스트
    void InputTileCombineList()
    {
        _TileCombineList = new List<UnitCombine>();
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.Water, TileType.Water, TileType.Water, 1, TileType.Water));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Water, TileType.Water, TileType.Water, 2, TileType.Water));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Water, TileType.Water, TileType.Water, 3 , TileType.Water));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Water, TileType.Water, TileType.Water, 4, TileType.Water));


        _TileCombineList.Add(new UnitCombine(true, 0, TileType.Forest, TileType.Forest, TileType.Forest, 1, TileType.Forest));
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.Mountain, TileType.Mountain, TileType.Mountain, 1, TileType.Mountain));
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.FarmLand, TileType.FarmLand, TileType.FarmLand, 1, TileType.FarmLand));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Town, TileType.Town, TileType.Town, 2, TileType.Town));

        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.Forest, TileType.Forest, 2, TileType.Forest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.Mountain, TileType.Mountain, 2, TileType.Mountain));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.FarmLand, TileType.FarmLand, 2, TileType.FarmLand));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Town, TileType.Town, TileType.Town, 3, TileType.Town));

        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Forest, TileType.Forest, TileType.Forest, 3, TileType.Forest));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Mountain, TileType.Mountain, TileType.Mountain, 3, TileType.Mountain));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.FarmLand, TileType.FarmLand, TileType.FarmLand, 3, TileType.FarmLand));
                
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.Mountain, TileType.FarmLand, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.FarmLand, TileType.Mountain, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.Forest, TileType.FarmLand, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.FarmLand, TileType.Forest, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.Mountain, TileType.Forest, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.Forest, TileType.Mountain, 1, TileType.Town));


        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Forest, TileType.Mountain, TileType.FarmLand, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Forest, TileType.FarmLand, TileType.Mountain, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Mountain, TileType.Forest, TileType.FarmLand, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Mountain, TileType.FarmLand, TileType.Forest, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.FarmLand, TileType.Mountain, TileType.Forest, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.FarmLand, TileType.Forest, TileType.Mountain, 2, TileType.Town));

        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Forest, TileType.Mountain, TileType.FarmLand, 3, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Forest, TileType.FarmLand, TileType.Mountain, 3, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Mountain, TileType.Forest, TileType.FarmLand, 3, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Mountain, TileType.FarmLand, TileType.Forest, 3, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.FarmLand, TileType.Mountain, TileType.Forest, 3, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.FarmLand, TileType.Forest, TileType.Mountain, 3, TileType.Town));



        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.Town, TileType.Forest, 1, TileType.TownMountain));  //타속성 자원을 합쳐서 해당 자원 이득을 가져가는 순환 구조.
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.Town, TileType.FarmLand, 1, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.Town, TileType.FarmLand, 1, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.Town, TileType.Mountain, 1, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.Town, TileType.Mountain, 1, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.Town, TileType.Forest, 1, TileType.TownFarm));

        _TileCombineList.Add(new UnitCombine(true, 2, TileType.FarmLand, TileType.Town, TileType.Forest, 2, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Forest, TileType.Town, TileType.FarmLand, 2, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Mountain, TileType.Town, TileType.FarmLand, 2, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.FarmLand, TileType.Town, TileType.Mountain, 2, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Forest, TileType.Town, TileType.Mountain, 2, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Mountain, TileType.Town, TileType.Forest, 2, TileType.TownFarm));

        _TileCombineList.Add(new UnitCombine(true, 3, TileType.FarmLand, TileType.Town, TileType.Forest, 3, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Forest, TileType.Town, TileType.FarmLand, 3, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Mountain, TileType.Town, TileType.FarmLand, 3, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.FarmLand, TileType.Town, TileType.Mountain, 3, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Forest, TileType.Town, TileType.Mountain, 3, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Mountain, TileType.Town, TileType.Forest, 3, TileType.TownFarm));




        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Town, TileType.FarmLand, TileType.Forest, 1, TileType.TownMountain));   // 타운 앞
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Town, TileType.Forest, TileType.FarmLand, 1, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Town, TileType.Mountain, TileType.FarmLand, 1, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Town, TileType.FarmLand, TileType.Mountain, 1, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Town, TileType.Forest, TileType.Mountain, 1, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Town, TileType.Mountain, TileType.Forest, 1, TileType.TownFarm));

        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Town, TileType.FarmLand, TileType.Forest, 2, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Town, TileType.Forest, TileType.FarmLand, 2, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Town, TileType.Mountain, TileType.FarmLand, 2, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Town, TileType.FarmLand, TileType.Mountain, 2, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Town, TileType.Forest, TileType.Mountain, 2, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Town, TileType.Mountain, TileType.Forest, 2, TileType.TownFarm));

        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Town, TileType.FarmLand, TileType.Forest, 3, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Town, TileType.Forest, TileType.FarmLand, 3, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Town, TileType.Mountain, TileType.FarmLand, 3, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Town, TileType.FarmLand, TileType.Mountain, 3, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Town, TileType.Forest, TileType.Mountain, 3, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Town, TileType.Mountain, TileType.Forest, 3, TileType.TownFarm));



        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.Forest, TileType.Town, 1, TileType.TownMountain));  //타운 뒤
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.FarmLand, TileType.Town, 1, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.FarmLand, TileType.Town, 1, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.Mountain, TileType.Town, 1, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.Mountain, TileType.Town, 1, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.Forest, TileType.Town, 1, TileType.TownFarm));

        _TileCombineList.Add(new UnitCombine(true, 2, TileType.FarmLand, TileType.Forest, TileType.Town, 2, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Forest, TileType.FarmLand, TileType.Town, 2, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Mountain, TileType.FarmLand, TileType.Town, 2, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.FarmLand, TileType.Mountain, TileType.Town, 2, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Forest, TileType.Mountain, TileType.Town, 2, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.Mountain, TileType.Forest, TileType.Town, 2, TileType.TownFarm));

        _TileCombineList.Add(new UnitCombine(true, 3, TileType.FarmLand, TileType.Forest, TileType.Town, 3, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Forest, TileType.FarmLand, TileType.Town, 3, TileType.TownMountain));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Mountain, TileType.FarmLand, TileType.Town, 3, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.FarmLand, TileType.Mountain, TileType.Town, 3, TileType.TownForest));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Forest, TileType.Mountain, TileType.Town, 3, TileType.TownFarm));
        _TileCombineList.Add(new UnitCombine(true, 3, TileType.Mountain, TileType.Forest, TileType.Town, 3, TileType.TownFarm));

        



        _TileCombineList.Add(new UnitCombine(true, 0, TileType.FlyingStone, TileType.FlyingStone, TileType.FlyingStone, 1, TileType.FlyingStone));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FlyingStone, TileType.FlyingStone, TileType.FlyingStone, 2, TileType.FlyingStone));

        _TileCombineList.Add(new UnitCombine(true, 0, TileType.FlyingStone, TileType.Forest, TileType.Forest, 1, TileType.StoneForest));
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.FlyingStone, TileType.Mountain, TileType.Mountain, 1, TileType.StoneMountain));
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.FlyingStone, TileType.FarmLand, TileType.FarmLand, 1, TileType.StoneFarm));

        _TileCombineList.Add(new UnitCombine(true, 0, TileType.Forest, TileType.FlyingStone, TileType.Forest, 1, TileType.StoneForest));
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.Mountain, TileType.FlyingStone, TileType.Mountain, 1, TileType.StoneMountain));
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.FarmLand, TileType.FlyingStone, TileType.FarmLand, 1, TileType.StoneFarm));

        _TileCombineList.Add(new UnitCombine(true, 0, TileType.Forest, TileType.Forest, TileType.FlyingStone, 1, TileType.StoneForest));
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.Mountain, TileType.Mountain, TileType.FlyingStone, 1, TileType.StoneMountain));
        _TileCombineList.Add(new UnitCombine(true, 0, TileType.FarmLand, TileType.FarmLand, TileType.FlyingStone, 1, TileType.StoneFarm));

        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FlyingStone, TileType.Forest, TileType.Forest, 2, TileType.StoneForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FlyingStone, TileType.Mountain, TileType.Mountain, 2, TileType.StoneMountain));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FlyingStone, TileType.FarmLand, TileType.FarmLand, 2, TileType.StoneFarm));

        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.FlyingStone, TileType.Forest, 2, TileType.StoneForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.FlyingStone, TileType.Mountain, 2, TileType.StoneMountain));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.FlyingStone, TileType.FarmLand, 2, TileType.StoneFarm));

        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Forest, TileType.Forest, TileType.FlyingStone, 2, TileType.StoneForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.Mountain, TileType.Mountain, TileType.FlyingStone, 2, TileType.StoneMountain));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.FarmLand, TileType.FarmLand, TileType.FlyingStone, 2, TileType.StoneFarm));




        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneForest, TileType.StoneForest, TileType.StoneForest, 2, TileType.StoneForest));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneMountain, TileType.StoneMountain, TileType.StoneMountain, 2, TileType.StoneMountain));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneFarm, TileType.StoneFarm, TileType.StoneFarm, 2, TileType.StoneFarm));

        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneForest, TileType.StoneMountain, TileType.StoneFarm, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneForest, TileType.StoneFarm, TileType.StoneMountain, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneMountain, TileType.StoneFarm, TileType.StoneForest, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneMountain, TileType.StoneForest, TileType.StoneFarm, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneForest, TileType.StoneFarm, TileType.StoneMountain, 1, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 1, TileType.StoneForest, TileType.StoneMountain, TileType.StoneFarm, 1, TileType.Town));

        _TileCombineList.Add(new UnitCombine(true, 2, TileType.StoneForest, TileType.StoneMountain, TileType.StoneFarm, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.StoneForest, TileType.StoneFarm, TileType.StoneMountain, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.StoneMountain, TileType.StoneFarm, TileType.StoneForest, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.StoneMountain, TileType.StoneForest, TileType.StoneFarm, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.StoneForest, TileType.StoneFarm, TileType.StoneMountain, 2, TileType.Town));
        _TileCombineList.Add(new UnitCombine(true, 2, TileType.StoneForest, TileType.StoneMountain, TileType.StoneFarm, 2, TileType.Town));



    }




    //타일 배치 관련/. 타일 등장 여부
    public List<List<AllowedUnitTileType>> _ListOfAllowedUnitTileTypeList; //리스트의 리스트. 리스트에 넣어진 순서대로 번호를 가진다.
    void InputListOfAllowedTileTypeList()   //리스트의 리스트. 리스트에 넣어진 순서대로 번호를 가지는데, 이 리스트에 순서대로 데이터를 넣어준다. 그레이드에 따른다.
    {
        _ListOfAllowedUnitTileTypeList = new List<List<AllowedUnitTileType>>();
        List<AllowedUnitTileType> _tempList;


        //0.5등급
        _tempList = new List<AllowedUnitTileType>();
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.FarmLand, 0), 0.34f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Forest, 0), 0.33f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Mountain, 0), 0.33f));

        _ListOfAllowedUnitTileTypeList.Add(_tempList);



        //1등급
        _tempList = new List<AllowedUnitTileType>();  
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.FarmLand, 0), 0.32f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Forest, 0), 0.32f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Mountain, 0), 0.31f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.FlyingStone, 0), 0.05f));
        _ListOfAllowedUnitTileTypeList.Add(_tempList);


        //2등급
        _tempList = new List<AllowedUnitTileType>();
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.FarmLand, 0), 0.28f));   
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Forest, 0), 0.28f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Mountain, 0), 0.28f)); 
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Water, 1), 0.1f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.FlyingStone, 0), 0.06f));
        _ListOfAllowedUnitTileTypeList.Add(_tempList);


        //3등급
        _tempList = new List<AllowedUnitTileType>();
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.FarmLand, 0), 0.27f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Forest, 0), 0.27f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Mountain, 0), 0.27f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Water, 1), 0.06f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.Water, 2), 0.03f));
        _tempList.Add(new AllowedUnitTileType(FindProperUnitTileType(TileType.FlyingStone, 0), 0.1f));


        _ListOfAllowedUnitTileTypeList.Add(_tempList);



    }


    UnitTileType FindProperUnitTileType(TileType _TileType, int _Grade)  //_TileTypeList에서 적절하게 부합하는 타일타입을 찾아서 리턴해주는 메소드.
    {
        UnitTileType _return = null;

        for (int i = 0; i < _TileTypeList.Count; i++)
        {
            if (_TileTypeList[i]._TileType == _TileType && _TileTypeList[i]._Grade == _Grade)
            {
                _return = _TileTypeList[i];
                break;
            }
        }
        return _return;

    }
     


    //비행석 관련

    public void CalculateFlyingStone()
    {
        _UnitProductWoodByStone = 0;
        _UnitProductIronByStone = 0;
        _UnitProductFoodByStone = 0;

        for (int i=0; i<_TitleManager._IdealWorldMapList.Count; i++)
        {
            if (_TitleManager._IdealWorldMapList[i]._IsOwned)
            {
                for( int j=0; j< _TitleManager._IdealWorldMapList[i]._IdealWorldMap.Count; j++)
                {
                    if(_TitleManager._IdealWorldMapList[i]._IdealWorldMap[j]._TileType== TileType.StoneForest)
                    {
                        _UnitProductWoodByStone += _TitleManager._IdealWorldMapList[i]._IdealWorldMap[j]._TileGrade;
                    }
                    else if (_TitleManager._IdealWorldMapList[i]._IdealWorldMap[j]._TileType == TileType.StoneMountain)
                    {
                        _UnitProductIronByStone += _TitleManager._IdealWorldMapList[i]._IdealWorldMap[j]._TileGrade;

                    }
                    else if (_TitleManager._IdealWorldMapList[i]._IdealWorldMap[j]._TileType == TileType.StoneFarm)
                    {
                        _UnitProductFoodByStone += _TitleManager._IdealWorldMapList[i]._IdealWorldMap[j]._TileGrade;

                    }
                }
            }
        }


    }
    

    //헤드쿼터 성장 변수
    public int _ProductWoodByHead;
    public int _ProductFoodByHead;
    public int _ProductIronByHead;
    public int _ProductCoinByHead;  //추가 생산 코인
    public int _AdditionalSearchingCount;  //추가로 발견하는 섬의 개수
    public int _AdditionalFreeTileCount; //게임에 추가되는 무료 타일 개수


    public void CheckHeadQuarterProduct()
    {
        //나무
        for (int i = 0; i < _CurrUpgradeList.Count; i++)
        {
            if (_CurrUpgradeList[i]._TypeNum == 0)
            {
                if (_CurrUpgradeList[i]._CurrLevel == _UpgradedCurrLevel[0])
                {
                    _ProductWoodByHead = _CurrUpgradeList[i]._CurrValue;
                }
            }
        }

        //철
        for (int i = 0; i < _CurrUpgradeList.Count; i++)
        {
            if (_CurrUpgradeList[i]._TypeNum == 1)
            {
                if (_CurrUpgradeList[i]._CurrLevel == _UpgradedCurrLevel[1])
                {
                    _ProductIronByHead = _CurrUpgradeList[i]._CurrValue;
                }
            }
        }

        //식량
        for (int i = 0; i < _CurrUpgradeList.Count; i++)
        {
            if (_CurrUpgradeList[i]._TypeNum == 2)
            {
                if (_CurrUpgradeList[i]._CurrLevel == _UpgradedCurrLevel[2])
                {
                    _ProductFoodByHead = _CurrUpgradeList[i]._CurrValue;
                }
            }
        }

        //코인
        for (int i = 0; i < _CurrUpgradeList.Count; i++)
        {
            if (_CurrUpgradeList[i]._TypeNum == 3)
            {
                if (_CurrUpgradeList[i]._CurrLevel == _UpgradedCurrLevel[3])
                {
                    _ProductCoinByHead = _CurrUpgradeList[i]._CurrValue;
                }
            }
        }

        //추가 발견 섬
        for (int i = 0; i < _CurrUpgradeList.Count; i++)
        {
            if (_CurrUpgradeList[i]._TypeNum == 4)
            {
                if (_CurrUpgradeList[i]._CurrLevel == _UpgradedCurrLevel[4])
                {
                    _AdditionalSearchingCount = _CurrUpgradeList[i]._CurrValue;
                }
            }
        }

        //추가 무료 타일
        for (int i = 0; i < _CurrUpgradeList.Count; i++)
        {
            if (_CurrUpgradeList[i]._TypeNum == 5)
            {
                if (_CurrUpgradeList[i]._CurrLevel == _UpgradedCurrLevel[5])
                {
                    _AdditionalFreeTileCount = _CurrUpgradeList[i]._CurrValue;
                }
            }
        }

    }







    //유니티 애즈


    public void ShowVideoAd(int iType)
    {

        if (iType == 0)
        { //StartAD 5초짜리 스킵형 광고
            if (Advertisement.IsReady("video"))
            {
                var options = new ShowOptions { resultCallback = HandleShowResultVideoADinStart };
                Advertisement.Show("video", options);
            }
        }     
        else if (iType == 2)
        {
            //보상 광고
            if (Advertisement.IsReady("rewardedVideo"))
            {
                var options = new ShowOptions { resultCallback = HandleShowResultRewaredAD };
                Advertisement.Show("rewardedVideo", options);
            }

        }

    }


    void HandleShowResultVideoADinStart(ShowResult result) //5초짜리 광고
    {
        switch (result)
        {
            case ShowResult.Finished:
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                if (Random.Range(0, 100) < 7)
                {
                    _UserCoin += 110;
                }
                else
                {
                    _UserCoin += 10;
                }

                SavePlayerData();
                _BGMManager.SoundPlay(40);

                SceneManager.LoadScene(1);

                break;
            case ShowResult.Skipped:

                SavePlayerData();
                _BGMManager.SoundPlay(40);

                SceneManager.LoadScene(1);

                break;
            case ShowResult.Failed:
                break;
        }
    }
    
    private void HandleShowResultRewaredAD(ShowResult result)  //보상형 광고 타이틀
    {
        switch (result)
        {
            case ShowResult.Finished:
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.

                //일단 시간 틱을 0으로 줄여줌 -> 제거
                //_CollectAvailableTime = System.DateTime.Now + System.TimeSpan.FromSeconds(_TickTimeToCollectCoin);
                //_RemainedTime = 0;

                _TitleManager.ReBatchIsland();

                if (Random.Range(0, 100) < 7)
                {
                    _UserCoin += 100;
                }
                else
                {
                    _UserCoin += 10;
                }
                          
                SavePlayerData();


                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Failed:
                break;
        }
    }




    //GPGS

    public void SignIn()
    {

        if (!Social.Active.localUser.authenticated)
        {
            Social.Active.localUser.Authenticate(
                (bool success, string strErrorMSG) => {

                    if (success)
                    {
                        ReportScoreToRichRank(_UserCoin); //부자 랭킹 등록을 위해서 스코어를 보고한다.
                        ReportScoreToManyIsland(_ClearIslandCount); //얼마나 많은 섬을 개척했는지
                    }
                    else
                    {

                    }

                }
            );
        }
        else
        {

        }

    }
    
    public void ReportScoreToRichRank(int score)
    {

        if (Social.Active.localUser.authenticated)
        {
            Social.Active.ReportScore((long)score, GPGSIds.leaderboard,

                (bool success) => {
                    SetRichRank();
                }
            );         
        }
    }

    public string _RichRank;  //현재 부자 순위 스트링
    void SetRichRank()
    {
        Social.Active.LoadScores(GPGSIds.leaderboard, ((scores) => {
            if (scores != null)
            {
                foreach (IScore score in scores)
                {
                    if (score.userID.Equals(Social.Active.localUser.id))
                    {
                        if (score.rank > 10000)
                        {
                            _RichRank ="0";
                        }
                        else
                        {
                            _RichRank = score.rank + "";
                        }
                    }
                }
            }
            else
            {
                _RichRank ="0";
            }
        }));
    }



    public void ReportScoreToLandValueRank(int score)
    {

        if (Social.Active.localUser.authenticated)
        {
            Social.Active.ReportScore((long)score, GPGSIds.leaderboard_2,

                (bool success) => {
                    SetLandValueRank();
                }
            );
        }
    }

    public string _LandValueRank;  //현재 최고 땅값 스트링
    void SetLandValueRank()
    {
        Social.Active.LoadScores(GPGSIds.leaderboard_2, ((scores) => {
            if (scores != null)
            {
                foreach (IScore score in scores)
                {
                    if (score.userID.Equals(Social.Active.localUser.id))
                    {
                        if (score.rank > 10000)
                        {
                            _LandValueRank = "0";
                        }
                        else
                        {
                            _LandValueRank = score.rank + "";
                        }
                    }
                }
            }
            else
            {
                _LandValueRank = "0";
            }
        }));

        StartCoroutine( RefreshResultLandValueRank());
    }


    public IEnumerator RefreshResultLandValueRank()
    {
        for (int i = 0; i < 20; i++) {
            yield return new WaitForSeconds(1);
            if (_LandValueRank == "0")
            {
                _GameManager._ResultLandValueRankText.text = "순위권 외";

            }
            else if (_LandValueRank == "")
            {
                _GameManager._ResultLandValueRankText.text = "잠시 기다려주세요";

            }
            else
            {
                _GameManager._ResultLandValueRankText.text = "최고 비싼 섬 " + _LandValueRank + "위";

            }
        }

    }

    
    public void ReportScoreToManyIsland(int score)
    {

        if (Social.Active.localUser.authenticated)
        {
            Social.Active.ReportScore((long)score, GPGSIds.leaderboard_3,

                (bool success) => {
                }
            );
        }
    }


    public void PlzShowLeaderboardUI(int iNum)
    {
        if (!Social.Active.localUser.authenticated) //로그인 안되어 있으면
        {
            Social.Active.localUser.Authenticate((bool success) =>  //로그인 시켜주자.
            {
                if (success)
                {
                    if (iNum == 1)
                    {
                        PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard);
                        return;
                    }
                    else if (iNum == 2)
                    {
                        PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_2);
                        return;

                    }
                    else
                    {
                        PlayGamesPlatform.Instance.ShowLeaderboardUI();
                        return;

                    }
                }
                else
                {
                    return;

                }
            });
        }
        else
        {
            if (iNum == 1)
            {
                PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard);
                return;
            }
            else if (iNum == 2)
            {
                PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_2);
                return;

            }
            else
            {
                PlayGamesPlatform.Instance.ShowLeaderboardUI();
                return;

            }
        }
    }


    public void UnlockAchievement(string strID)
    {

        if (Social.Active.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ReportProgress(strID, 100f, null);

        }

    }



    //저장 로드
    public void SavePlayerData()
    {
        Dictionary<string, object> PlayerData = new Dictionary<string, object>();
        PlayerData.Add("_UserCoin", _UserCoin);
        PlayerData.Add("_CountMakeIsland", _CountMakeIsland);

        if (_SceneNum == 0)
        {
            PlayerData.Add("_CurrOpeningWorldMap", _TitleManager._CurrOpeningWorldMap);
            PlayerData.Add("_IdealWorldMapList", _TitleManager._IdealWorldMapList);
        }
        else if (_SceneNum == 1)
        {
            PlayerData.Add("_CurrOpeningWorldMap", _GameManager._CurrOpeningWorldMap);
            PlayerData.Add("_IdealWorldMapList", _GameManager._IdealWorldMapList);

        }

        PlayerData.Add("_CollectAvailableTime", _CollectAvailableTime.ToLocalTime());
        PlayerData.Add("_IAmNotFirst", _IAmNotFirst);
        PlayerData.Add("_ClearIslandCount", _ClearIslandCount);

        PlayerData.Add("_MusicOn", _BGMManager._MusicOn);
        PlayerData.Add("_SoundOn", _BGMManager._SoundOn);
        PlayerData.Add("_VibrateOn", _VibrateOn);

        PlayerData.Add("_AlreadyReview", _AlreadyReview);
        PlayerData.Add("_UpgradedCurrLevel", _UpgradedCurrLevel);

        
        UtilManager.Instance.CreateJsonFile("PlayerData", PlayerData);
        Debug.Log("System: PlayerData Save Completed");

    }

    public void LoadPlayerData()
    {

        string tempMessage = UtilManager.Instance.LoadJsonFile("PlayerData");
        if (string.IsNullOrEmpty(tempMessage))
        {

        }
        else
        {
            PlayerData PlayerDataRaw = JsonFx.Json.JsonReader.Deserialize<PlayerData>(tempMessage);
            if (PlayerDataRaw == null)
            {
                return;
            }

            _UserCoin = PlayerDataRaw._UserCoin;
            _CountMakeIsland = PlayerDataRaw._CountMakeIsland;

            if (_SceneNum == 0)
            {
                _TitleManager._CurrOpeningWorldMap = PlayerDataRaw._CurrOpeningWorldMap;
                _TitleManager._IdealWorldMapList = PlayerDataRaw._IdealWorldMapList;


            }else if (_SceneNum == 1)
            {
                _GameManager._CurrOpeningWorldMap = PlayerDataRaw._CurrOpeningWorldMap;
                _GameManager._IdealWorldMapList = PlayerDataRaw._IdealWorldMapList;

            }

            _CollectAvailableTime = PlayerDataRaw._CollectAvailableTime.ToLocalTime();
            _IAmNotFirst = PlayerDataRaw._IAmNotFirst;
            _ClearIslandCount = PlayerDataRaw._ClearIslandCount;

            _BGMManager._SoundOn = PlayerDataRaw._SoundOn;
            _BGMManager._MusicOn = PlayerDataRaw._MusicOn;
            _VibrateOn = PlayerDataRaw._VibrateOn;

            _AlreadyReview = PlayerDataRaw._AlreadyReview;
            _UpgradedCurrLevel = PlayerDataRaw._UpgradedCurrLevel;


        }

        Debug.Log("System: PlayerData Load Completed");

    }


    public void SceneChanger(int _SceneNum) //씬 번호가 중요해졌다.
    {
        this._SceneNum = _SceneNum;
        if (_SceneNum == 0)
        {
            _TitleManager = GameObject.Find("TitleManager").GetComponent<TitleManager>();

        }else if (_SceneNum == 1)
        {
            _GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        }
    }







}
public class PlayerData
{
    public int _UserCoin;
    public int _CountMakeIsland; //이 유저가 섬을 만든 총 횟수

    public List<UnitIdealWorldMap> _IdealWorldMapList; // 월드맵 리스트
    public UnitIdealWorldMap _CurrOpeningWorldMap;

    public System.DateTime _CollectAvailableTime;
    public bool _IAmNotFirst; //뉴비면 튜토리얼을 보여준다. 

    public int _ClearIslandCount;
    public bool _MusicOn;
    public bool _SoundOn;
    public bool _VibrateOn;

    public bool _AlreadyReview;

    public List<int> _UpgradedCurrLevel;  //업그레이드 상태


    public PlayerData()
    {

    }

}