﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.SceneManagement;


public class TouchInputManagerTitle : MonoBehaviour {

    //기존 변수
    public Camera _MainCamera;
    public MainCameraManager _MainCameraManager;
    public TitleManager _TitleManager;


    Vector3 startPos, deltaPos, nowPos;
    public bool isDragged;
    const float dragAccuracy = 100;

    float iDragSPD;
    float _RotatingSPD;
    int _RotatingDirFactor;

    public float fGravityDistance;
    public float fGravitySPD;

    float fOldDistance;
    float fNewDistance;




    float _OldDistanceX;
    float _OldDistanceY;
    float _NewDistanceX;
    float _NewDistanceY;
    float _DeltaDistX;
    float _DeltaDistY;
    float _WelocityDeltaDistY;

    RaycastHit hit;
    Ray ray;
    public int pointerID;

    //시스템
    public bool _IsOnTileTouch; //메인터치용 

    void Awake()
    {
        _TitleManager = GetComponent<TitleManager>();
        iDragSPD = -2.3f;
        fGravityDistance = 0.8f;

        fGravitySPD = 1.0f;

        _RotatingSPD = 0.001f;

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
        pointerID = -1;
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
		pointerID = 0;
#endif        
    }
    Touch tempTouchs;
    void Update()
    {
    
        _WelocityDeltaDistY = _WelocityDeltaDistY * 0.93f; //한 손으로 누르고 있으면 마찰로 감속시킨다.
        _MainCameraManager._Theta += _WelocityDeltaDistY * _RotatingSPD * _RotatingDirFactor;  //화면 회전 로직


        if (pointerID == 0)//안드 등 모바일 환경
        {
            if (Input.touchCount == 1)
            {
                _WelocityDeltaDistY = _WelocityDeltaDistY * 0.93f; //한 손으로 누르고 있으면 마찰로 감속시킨다.


                tempTouchs = Input.GetTouch(0);

                if (EventSystem.current.IsPointerOverGameObject(tempTouchs.fingerId))
                {
                    if (tempTouchs.phase == TouchPhase.Ended)
                    {

                    }
                    return;
                }


                if (tempTouchs.phase == TouchPhase.Ended)
                {
                    if (isDragged)
                    {
                        isDragged = false;
                    }                   

                }
                else if (tempTouchs.phase == TouchPhase.Began)
                {
                    startPos = tempTouchs.position;
                    isDragged = false;
                }
                else if ((tempTouchs.phase == TouchPhase.Moved || tempTouchs.phase == TouchPhase.Stationary) && !_IsOnTileTouch)  //온타일 터치가 아닌 경우에만 드래그를 한다.
                {
                    nowPos = tempTouchs.position;
                    deltaPos = nowPos - startPos;

                    if (deltaPos.sqrMagnitude > dragAccuracy)
                    {
                        if (deltaPos.x < 0)
                        {
                            if (_MainCamera.transform.localPosition.x <= 500f)
                            {
                                _MainCameraManager.MoveThisCamera(deltaPos.x * 0.02f * iDragSPD, 0);
                            }
                        }
                        else
                        {
                            if (_MainCamera.transform.localPosition.x > -500f)
                            {
                                _MainCameraManager.MoveThisCamera(deltaPos.x * 0.02f * iDragSPD, 0);

                            }
                        }
                        if (deltaPos.y < 0)
                        {
                            if (_MainCamera.transform.localPosition.z <= 500f)
                            {
                                _MainCameraManager.MoveThisCamera(0, deltaPos.y * 0.02f * iDragSPD);

                            }
                        }
                        else
                        {
                            if (_MainCamera.transform.localPosition.z > -500f)
                            {
                                _MainCameraManager.MoveThisCamera(0, deltaPos.y * 0.02f * iDragSPD);

                            }
                        }
                        isDragged = true;
                        startPos = nowPos;
                    }
                }
            }
            else if (Input.touchCount == 2)
            {
                if (Input.touches[0].phase == TouchPhase.Began || Input.touches[1].phase == TouchPhase.Began)
                {
                    _NewDistanceX = Mathf.Abs(Input.touches[1].position.x - Input.touches[0].position.x);
                    _OldDistanceX = _NewDistanceX;

                    _NewDistanceY = Input.touches[1].position.y - Input.touches[0].position.y;
                    _OldDistanceY = _NewDistanceY;

                }
                else
                {

                    _NewDistanceX = Mathf.Abs(Input.touches[1].position.x - Input.touches[0].position.x);
                    _NewDistanceY = Input.touches[1].position.y - Input.touches[0].position.y;

                }

                if (Input.touches[0].position.x >= Input.touches[1].position.x)
                {
                    _RotatingDirFactor = 1;
                }
                else
                {
                    _RotatingDirFactor = -1;

                }
                _DeltaDistX = _NewDistanceX - _OldDistanceX;
                _DeltaDistY = _NewDistanceY - _OldDistanceY;
                //_WelocityDeltaDistY += _DeltaDistY;


                if (_DeltaDistX > 0)
                {
                    if (_MainCamera.transform.position.y > 10)
                    {
                        _MainCameraManager._Low += _DeltaDistX * 0.02f * iDragSPD;
                    }
                }
                else
                {
                    if (_MainCamera.transform.position.y < 200)
                    {
                        _MainCameraManager._Low += _DeltaDistX * 0.02f * iDragSPD;

                    }
                }
                _OldDistanceX = _NewDistanceX;

                _OldDistanceY = _NewDistanceY;


                if (Input.touches[0].phase == TouchPhase.Ended)
                {
                    startPos = Input.touches[1].position;

                }
                else if (Input.touches[1].phase == TouchPhase.Ended)
                {
                    startPos = Input.touches[0].position;
                }

                isDragged = true;
            }
        }
        else  //유니티 에디터
        {

            if (!EventSystem.current.IsPointerOverGameObject(pointerID))
            {
                if (Input.GetMouseButtonUp(0))
                {
                    if (isDragged)
                    {
                        isDragged = false;
                    }
                    if (_IsOnTileTouch)  //이거 레이 쏘는거 없으니까 업데이트로 보완해주는 방법으로 여기에 추가해준다.
                    {
                        _IsOnTileTouch = false;  
                    }


                }
                else if (Input.GetMouseButton(0) && !_IsOnTileTouch)
                {
                    nowPos = (Vector2)Input.mousePosition;

                    if (!isDragged)
                    {
                        isDragged = true;
                        startPos = nowPos;  //드래그 시작 포지션을 고정해주는 수식
                    }

                    deltaPos = nowPos - startPos;

                    if (deltaPos.sqrMagnitude > dragAccuracy)
                    {
                        if (deltaPos.x < 0)
                        {
                            if (_MainCamera.transform.localPosition.x <= 500f)
                            {
                                _MainCameraManager.MoveThisCamera(deltaPos.x * 0.02f * iDragSPD, 0);
                            }
                        }
                        else
                        {
                            if (_MainCamera.transform.localPosition.x > -500f)
                            {
                                _MainCameraManager.MoveThisCamera(deltaPos.x * 0.02f * iDragSPD, 0);

                            }
                        }
                        if (deltaPos.y < 0)
                        {
                            if (_MainCamera.transform.localPosition.z <= 500f)
                            {
                                _MainCameraManager.MoveThisCamera(0, deltaPos.y * 0.02f * iDragSPD);

                            }
                        }
                        else
                        {
                            if (_MainCamera.transform.localPosition.z > -500f)
                            {
                                _MainCameraManager.MoveThisCamera(0, deltaPos.y * 0.02f * iDragSPD);

                            }
                        }
                        isDragged = true;
                        startPos = nowPos;
                    }
                }

            }
            else
            {  //is not pointing a Object
                if (Input.GetMouseButtonUp(0))
                {
                    if (isDragged)
                    {
                        isDragged = false;
                    }

                }
            }
        }
    }

    void FixedUpdate()
    {
        if (pointerID == 0) // 안드나 폰 환경
        {
            if (Input.touchCount == 1)
            {
                Touch tempTouchs = Input.GetTouch(0);

                if (EventSystem.current.IsPointerOverGameObject(pointerID))
                {
                    return;
                }

                if (tempTouchs.phase == TouchPhase.Began)
                {
                    if (!isDragged)
                    {
                        hit = new RaycastHit();
                        ray = _MainCamera.ScreenPointToRay(tempTouchs.position);

                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.tag == "UnitHex")
                            {                                
                                hit.collider.transform.GetComponentInParent<UnitWorldLand>().TagThisWorldToCurrOpeningWorldMap(); //여기서 UI콜업까지 같이 시켜주자.                           

                            }else if (hit.collider.tag == "HeadQuarter")
                            {
                                _TitleManager._UIHeadQuarter.CallUIHeadQuarter();                 


                            }
                        }

                    }
                }
                if ((tempTouchs.phase == TouchPhase.Moved || tempTouchs.phase == TouchPhase.Stationary) && _IsOnTileTouch)
                {
                    if (!isDragged)
                    {
                        hit = new RaycastHit();
                        ray = _MainCamera.ScreenPointToRay(tempTouchs.position);

                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.tag == "UnitHex")
                            {
                            }
                        }
                    }
                }
                else if (tempTouchs.phase == TouchPhase.Ended && _IsOnTileTouch)
                {
              
                }
            }
        }
        else  //pointerID = -1 UNITYEDITOR		
        {
            if (!EventSystem.current.IsPointerOverGameObject(pointerID))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (!isDragged)
                    {
                        hit = new RaycastHit();
                        ray = _MainCamera.ScreenPointToRay(Input.mousePosition);

                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.tag == "UnitHex")
                            {
                                hit.collider.transform.GetComponentInParent<UnitWorldLand>().TagThisWorldToCurrOpeningWorldMap();
                         
                            }
                            else if (hit.collider.tag == "HeadQuarter")
                            {
                                _TitleManager._UIHeadQuarter.CallUIHeadQuarter();


                            }
                        }
                    }
                }

                if (Input.GetMouseButton(0) && _IsOnTileTouch)
                {
                    if (!isDragged)
                    {
                        hit = new RaycastHit();
                        ray = _MainCamera.ScreenPointToRay(Input.mousePosition);

                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.tag == "UnitHex")
                            {
                                //hit.collider.transform.GetComponent<UnitHexLand>().MainTouchBTN();
                            }
                        }
                    }
                }
            }

        }
    }


    //function

    bool dXisMoreLongerThandY(Vector2 deltaPos)
    {

        float tempX;
        float tempY;

        if (deltaPos.x < 0)
        {

            tempX = -deltaPos.x;
        }
        else
        {
            tempX = deltaPos.x;


        }

        if (deltaPos.y < 0)
        {

            tempY = -deltaPos.y;

        }
        else
        {
            tempY = deltaPos.y;


        }


        if (tempX >= tempY)
        {

            return true;
        }
        else
        {

            return false;
        }


    }


    public float DistBetweenR(Vector3 target, Vector3 refer)
    {

        float tempReturnDist = (target.x - refer.x) * (target.x - refer.x)
            + (target.y - refer.y) * (target.y - refer.y)
            + (target.z - refer.z) * (target.z - refer.z);

        return tempReturnDist;

    }

}
