﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class AllowedUnitTileType
{
    public UnitTileType _UnitTileType;  //대상 타일 타입
    public float _Possibility; //1이 최고다.

    public AllowedUnitTileType() { }
    public AllowedUnitTileType(UnitTileType _UnitTileType, float _Possibility)
    {
        this._UnitTileType = _UnitTileType;
        this._Possibility = _Possibility;
    }
}


public class TitleManager : MonoBehaviour {

    public DataManager _DataManager;

    //내부연결
    public UnitHexLand _UnitHexLandPrefab;  //프리팹
    public GameObject _WorldHexRoot; //월드 루트
    public UnitWorldLand _UniWorldLandPrefab;   //프리팹

    public TouchInputManagerTitle _TouchInputManagerTitle;

    //시스템
    public bool _IsPause;

    UnitIdealWorldMap _TempUnitIdealWorldMap;
    public UnitWorldLand _CurrUnitWorldLand;
    public List<UnitWorldLand> _UnitWorldLandList; //월드들 리스트

    //유저 정보
    public int _PlayerWood;
    public int _PlayerIron;
    public int _PlayerFood;

    public int _UnitProductGold;
    public Text _PlayerGoldText;

    public Text _RichRankText;


    //islandEnterUI
    public GameObject _IslandEnterBTN;
    public GameObject _IslandValueDriverRoot;
    public GameObject _IslandValueRoot;
    public GameObject _IslandGoldOutPutRoot;
    public GameObject _IslandEnterFeeRoot;

    public Text _IslandNumText;

    public Text _WoodMultipleText;
    public Text _IronMultipleText;
    public Text _FoodMultipleText;
    public Text _ExpandMultipleText;

    public Text _EntraceFeeText;
    public Text _GoldOutPutText;
    public Text _CurrValueText;

    public List<GameObject> _GradeStarList;


    //저장하는 유저 정보&시스템 데이터
    public int _PlayerGold;
    public List<UnitIdealWorldMap> _IdealWorldMapList;  //현재 맵상에 생성된 월드맵 리스트.  여기서 소유 여부(클리어 여부 표기)
    public UnitIdealWorldMap _CurrOpeningWorldMap; //이건 이제 인게임에서 곧 열릴 월드맵임. 이것도 샅샅이 쪼개서 저장한다.

    //회수 관련
      
    public AnimatedButton _CollectingBTN;
    public GameObject _CollectBTNCover;

    public GameObject _CollectingGaugeRemainTime;
    public Text _CollecingMoneyText;
    public GameObject _LoadingMSG;

    //리뷰
    public UIReview _UIReview;
    public UIOption _UIOption;

    //배 이동.  sleeping boat
    public SleepingShip _SleepingShip;
    public SleepingShip _SleepingShip3;

    //메뉴 온
    public bool _IsMenuOn; //메뉴가 켜진 상태.

    //헤드쿼터
    public UIHeadQuarter _UIHeadQuarter;


    void Start () {        
        StartCoroutine(Init());
        Application.targetFrameRate = 60;
        _DataManager = GameObject.Find("DataManager").GetComponent<DataManager>();
        _DataManager.SceneChanger(0);

        _DataManager._TickTimeToCollectCoin = 7200;  //틱은 두 시간으로 좀 길게 해서 광고 시청을 유도해볼까? 
        _DataManager._RemainedTime = _DataManager._TickTimeToCollectCoin;

        _DataManager._CollectAvailableTime = System.DateTime.Now + System.TimeSpan.FromSeconds(_DataManager._TickTimeToCollectCoin);
        _DataManager._VibrateOn = false;
        
        StartCoroutine(SecCounter());

        _DataManager._BGMManager.MusicStart(2);

    }

    private void Update()
    {
        _PlayerGoldText.text = _DataManager._UserCoin + "";

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_UIOption.gameObject.activeSelf)
            {
                _UIOption.CloseUIOption ();

            }
            else
            {
                _UIOption.CallUIOption();
            }
        }
    }
    

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.05f);
        
        _DataManager.LoadPlayerData();
        if (_IdealWorldMapList == null || _IdealWorldMapList.Count == 0)  //맵 저장 데이터가 비어있을 경우, 새로 시작하는 경우이므로 월드를 열어준다.
        {
            InputInitialWorld();
            _DataManager.SavePlayerData();
        }
        else  // 이미 저장된 데이터가 있을 경우, 로드해준다.
        {
            for(int i=0; i < _IdealWorldMapList.Count; i++)
            {
                _TempWorldMap = ImportIdealWorldMapAsShown(_IdealWorldMapList[i]._IdealWorldMap);

                DrawCurrentWorldMapInitiating(_TempWorldMap, _IdealWorldMapList[i]).transform.localPosition 
                    = new Vector3(_IdealWorldMapList[i]._WorldRadius* Mathf.Cos(_IdealWorldMapList[i]._WorldAngleWelocity* _IdealWorldMapList[i]._WorldCurrAngle)
                    , _IdealWorldMapList[i]._WorldRefY
                    , _IdealWorldMapList[i]._WorldRadius * Mathf.Sin(_IdealWorldMapList[i]._WorldAngleWelocity* _IdealWorldMapList[i]._WorldCurrAngle));
                RedrawCurrentWorldMap(_TempWorldMap, _IdealWorldMapList[i]._IsOwned);
                
            }
            int _tempNotOwnedWorldCount = CheckNotOwnedWorldCount();
            if (_tempNotOwnedWorldCount <= 1 )  // 게임 접속시 오픈 섬이 0개면.. 1개 채워준다.
            {
                SupplyWorldInTitle(2);
            }            
        }
        _DataManager.CalculateFlyingStone();
        _DataManager.CheckHeadQuarterProduct();
        CheckAndDisplayProductByStoneTotally();
        _LoadingMSG.SetActive(false);

        if (!_DataManager._AlreadyReview && _DataManager._ClearIslandCount>=3 && Random.Range(0,100)<30 )
        {
            _UIReview.CallUIReview();
        }

    }

    IEnumerator SecCounter()
    {
        _CollectBTNCover.SetActive(true);
        _CollectingBTN.enabled = false;

        yield return new WaitForSeconds(0.05f);
        //코인 회수 관련

        ComplieCollectingCoin();  //코인 얼마 먹을 수 있는 지 집계해줍니다.
        CalculateRemainedTime(); //현재 남은 시간을 계산해줍니다.

        for (; ; )
        {
            if (_DataManager._RemainedTime == 0)
            {
                _CollectBTNCover.SetActive(false);
                _CollectingBTN.enabled = true;
                _CollectingLeftTime.text = "";

            }
            else if (_DataManager._TotalRevenueCoinPerTick == 0)
            {
                //_CollectBTNCover.SetActive(true);
                //_CollectingBTN.enabled = false;
                _CollectingLeftTime.text = "섬을 개척해보세요  ";
            }
            else
            {
                _CollectBTNCover.SetActive(true);
                _CollectingBTN.enabled = false;

                _CollectingLeftTime.text = SecToText((int)_DataManager._RemainedTime) + " 뒤 수금 가능 ";

            }

            yield return new WaitForSeconds(1);
            CalculateRemainedTime(); //현재 남은 시간을 계산해줍니다.

            if (_DataManager._RichRank == "")
            {
                _RichRankText.text = "";

            }
            else if (_DataManager._RichRank == "0")
            {
                _RichRankText.text = "순위 없음";

            }
            else
            {
                _RichRankText.text = "최고 부자 " + _DataManager._RichRank + "위";

            }
        }
    }



    void InputInitialWorld()
    {
        _IdealWorldMapList = new List<UnitIdealWorldMap>();
        _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, 0, 1000, 230, 100, 50, Random.Range(-0.0005f,0.0005f) ,310, 30, Random.Range(-3,3) ,TilePriceExpandLogicType.NaturalNumberEasy,null );
        _IdealWorldMapList.Add(_CurrOpeningWorldMap);

        _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, 0, 1000, 160, 100, 50, Random.Range(-0.0005f, 0.0005f), Random.Range(-360, 360), 45, Random.Range(-3, 3), TilePriceExpandLogicType.NaturalNumberEasy, null);
        _IdealWorldMapList.Add(_CurrOpeningWorldMap);

        _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, 0, 1000, 210, 90, 50, Random.Range(-0.0005f, 0.0005f), Random.Range(-360, 360), 85, Random.Range(-2, 2), TilePriceExpandLogicType.NaturalNumberEasy, null);
        _IdealWorldMapList.Add(_CurrOpeningWorldMap);

        _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, 1, 1000, 320, 80, 50, Random.Range(-0.0005f, 0.0005f), Random.Range(-360, 360), 110, Random.Range(-2, 2), TilePriceExpandLogicType.NaturalNumber, null);
        _IdealWorldMapList.Add(_CurrOpeningWorldMap);

        _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, 1, 1000, 270, 70, 150, Random.Range(-0.0005f, 0.0005f), Random.Range(-360, 360), 150, Random.Range(-2, 2), TilePriceExpandLogicType.NaturalNumber, null);
        _IdealWorldMapList.Add(_CurrOpeningWorldMap);

        _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, 1, 1000, 350, 60, 150, Random.Range(-0.0005f, 0.0005f), Random.Range(-360, 360), 180, Random.Range(-2, 2), TilePriceExpandLogicType.NaturalNumber, null);
        _IdealWorldMapList.Add(_CurrOpeningWorldMap);

        UnitPosXZ _tempBanPos = new UnitPosXZ(-10, -10, 130);
        List<UnitPosXZ> _tempBanList = new List<UnitPosXZ>();
        _tempBanList.Add(_tempBanPos);
        _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, 2, 1000, 550, 80, 150, Random.Range(-0.0005f, 0.0005f), Random.Range(-360, 360), 240, Random.Range(-3, 3), TilePriceExpandLogicType.OddNumber, _tempBanList);
        _IdealWorldMapList.Add(_CurrOpeningWorldMap);


        //_CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, 2, 1000, 800, 70, 50, Random.Range(-0.0005f, 0.0005f), Random.Range(-360, 360), 170, Random.Range(-30, 30), TilePriceExpandLogicType.OddNumber, null);
        //_IdealWorldMapList.Add(_CurrOpeningWorldMap);



    }

    void SupplyWorldInTitle(int _Count) //부족한 월드를 보충해준다.  //그레이드는 보상과 비례해야 한다. 확장 로직과도 조금 페어링?
    {
        TilePriceExpandLogicType _tempExpandLogic;
        int _tempGrade;

        for (int i = 0; i < _Count; i++)
        {
            _tempExpandLogic = TilePriceExpandLogicType.NaturalNumberEasy;
            _tempGrade = 0;

            if (Random.Range(0, 100) < 10)  //무조건 10퍼는 기본 타일 0~ 1 등급 나오게 해준다.
            {
                if (Random.Range(0, 100) < 70)
                {
                    _tempGrade = 0;
                    _tempExpandLogic = TilePriceExpandLogicType.NaturalNumberEasy;

                }
                else
                {
                    _tempGrade = 1;
                    _tempExpandLogic = TilePriceExpandLogicType.NaturalNumber;

                }
            }
            else if (Random.Range(0, 100) < 70 - _DataManager._ClearIslandCount*3f)  //23판째부터는 잘 안나온다.
            {
                _tempGrade = 0;
                _tempExpandLogic = TilePriceExpandLogicType.NaturalNumberEasy;
                       

            }
            else if (Random.Range(0, 100) < 70 - _DataManager._ClearIslandCount*2f ) //35판부터는 잘 안나온다.
            {
                _tempGrade = 1;
                _tempExpandLogic = TilePriceExpandLogicType.NaturalNumber;

               
            }
            else if (Random.Range(0, 100) < 70 - _DataManager._ClearIslandCount * 1f) //70판째부터는 잘 안나온다.
            {
                _tempGrade = 2;        
                _tempExpandLogic = TilePriceExpandLogicType.OddNumber;

              
            }
            else  //210판부터는 0.9의 확률로 이 등급만 나온다. 
            {
                _tempGrade = 3;
                _tempExpandLogic = TilePriceExpandLogicType.PrimeNumber;


            }

            if (_DataManager._CountMakeIsland*113%100 <20)
            {
                UnitPosXZ _tempBanPos = new UnitPosXZ(_DataManager._CountMakeIsland * 113%20 -10 , _DataManager._CountMakeIsland * 71 % 20 - 10, _DataManager._CountMakeIsland * 31 % 150 + 20);
                List<UnitPosXZ> _tempBanList = new List<UnitPosXZ>();
                _tempBanList.Add(_tempBanPos);
                _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, _tempGrade, 1000, _DataManager._CountMakeIsland * 971 % 600 + 100 + _DataManager._ClearIslandCount * 20  , (int)Random.Range(50, 100), _DataManager._CountMakeIsland * 1121 % 100 + 50, Random.Range(-0.0001f, 0.0001f), Random.Range(-360, 360), Random.Range(120, 500), Random.Range(-1, 1), _tempExpandLogic, _tempBanList);

            }  
            else
            {

                _CurrOpeningWorldMap = CallOneUnitIdealWorldMap(_DataManager._CountMakeIsland, _tempGrade, 1000, _DataManager._CountMakeIsland * 1131 % 200 + 150 + _DataManager._ClearIslandCount *10, (int)Random.Range(50, 100), _DataManager._CountMakeIsland * 1121 % 50 + 50, Random.Range(-0.0001f, 0.0001f), Random.Range(-360, 360),Random.Range(50,500), Random.Range(-1, 1), _tempExpandLogic, null);

            }
            _IdealWorldMapList.Add(_CurrOpeningWorldMap);


        }
    }

    int CheckNotOwnedWorldCount()  //현재 소유되지 않은 월드의 개수를 구한다.
    {
        int _return = 0;

        for(int i=0; i < _IdealWorldMapList.Count; i++)
        {
            if (!_IdealWorldMapList[i]._IsOwned)
            {
                _return++;
            }

        }
        return _return;
    }


    public void SellCurrSelectedWorld()
    {
        _DataManager._BGMManager.SoundPlay(32);

        _DataManager._UserCoin += _CurrOpeningWorldMap._WorldValue;
        TurnOffEnterUIBTN();
        Destroy(_CurrUnitWorldLand.gameObject);
        DeleteCurrOpeningWorldMapInList();
        _DataManager.SavePlayerData();

        _DataManager.CalculateFlyingStone();
        CheckAndDisplayProductByStoneTotally();
        ComplieCollectingCoin();
        _DataManager.ReportScoreToRichRank(_DataManager._UserCoin);

    }


    void DeleteCurrOpeningWorldMapInList()  //현재 월드맵 리스트에서 커렌트오프닝월드를 지워주자.
    {
        if (_IdealWorldMapList.Contains(_CurrOpeningWorldMap)){
            _IdealWorldMapList.Remove(_CurrOpeningWorldMap);

        }
        else
        {
            Debug.Log("there are no CurrIdealWorld in List");
        }
    }


    public void ReBatchIsland()// 섬들을 싹 갈아준다.
    {
        _CurrOpeningWorldMap = null;
        Debug.Log("_UnitWorldLandList.Count " + _UnitWorldLandList.Count);
        Debug.Log("_IdealWorldMapList.Count " + _IdealWorldMapList.Count);
        int tempCount = _UnitWorldLandList.Count;

        for (int i=0; i< tempCount; i++)
        { 
            if(!_UnitWorldLandList[0]._UnitIdealWorldMap._IsOwned)
            {
                if (_IdealWorldMapList.Contains(_UnitWorldLandList[0]._UnitIdealWorldMap))
                {
                    _IdealWorldMapList.Remove(_UnitWorldLandList[0]._UnitIdealWorldMap);
                }            
           

                Destroy(_UnitWorldLandList[0].gameObject,0.1f);
                if (_UnitWorldLandList.Contains(_UnitWorldLandList[0]))
                {
                    _UnitWorldLandList.Remove(_UnitWorldLandList[0]);

                }         
            }        
        }
        //Debug.Log("_UnitWorldLandList.Count " + _UnitWorldLandList.Count);
        //Debug.Log("_IdealWorldMapList.Count " + _IdealWorldMapList.Count);

        SupplyWorldInTitle(6); //광고는 업그레이드와 상관없이 6개.

    }

    public UnitIdealWorldMap CallOneUnitIdealWorldMap(int _Num, int _Grade, int _LandSizeCount, float _LandSizeMaxDistance, int _LandDensity, float _InitOpenLandDistance, float _Welocity, float _CurrAngle, float _Radius, float _RefY  , TilePriceExpandLogicType _TilePriceExpandLogic, List<UnitPosXZ> _BanPosList)
    {//새로운 섬을 소환해준다.

        _TempUnitIdealWorldMap = new UnitIdealWorldMap(_Num, _Grade, _LandSizeCount, _LandSizeMaxDistance, _LandDensity, _InitOpenLandDistance, _TilePriceExpandLogic, _BanPosList);
        _TempUnitIdealWorldMap._AllowedUnitTileTypeList = _DataManager._ListOfAllowedUnitTileTypeList[_Grade]; //여기서 허용된 타일을 가져오므로, 이 다음부터 맵 배치 가능.
        BatchTilesInAllowed(_TempUnitIdealWorldMap); //맵에 각 타일들을 배치해준다.
        ReBatchTilesForMinimumMatch(_TempUnitIdealWorldMap, 2);   //맵에서 타일들 세개가 이어지도록 정리해준다.


        _TempWorldMap = ImportIdealWorldMapAsShown(_TempUnitIdealWorldMap._IdealWorldMap);
        _TempUnitIdealWorldMap._WorldRadius = _Radius;
        _TempUnitIdealWorldMap._WorldCurrAngle = _CurrAngle/( _Welocity+0.001f);
        _TempUnitIdealWorldMap._WorldRefY = _RefY;
        _TempUnitIdealWorldMap._WorldAngleWelocity = _Welocity;

        DrawCurrentWorldMapInitiating(_TempWorldMap, _TempUnitIdealWorldMap).transform.localPosition =
            new Vector3(_TempUnitIdealWorldMap._WorldRadius * Mathf.Cos(_TempUnitIdealWorldMap._WorldAngleWelocity * _TempUnitIdealWorldMap._WorldCurrAngle)
                    , _TempUnitIdealWorldMap._WorldRefY
                    , _TempUnitIdealWorldMap._WorldRadius * Mathf.Sin(_TempUnitIdealWorldMap._WorldAngleWelocity* _TempUnitIdealWorldMap._WorldCurrAngle));
        RedrawCurrentWorldMap(_TempWorldMap , _TempUnitIdealWorldMap._IsOwned);

        _DataManager._CountMakeIsland++;

        return _TempUnitIdealWorldMap;  //생성된 종합적인 월드맵이다.
    }


    public void ReBatchTilesForMinimumMatch(UnitIdealWorldMap _UnitIdealWorldMap, int _DoCount)  //이 월드의 시작 시점에 최소한 세개가 이어진 타일이 몇개는 있도록 조정해준다.
    {
        for (int k = 0; k < _DoCount; k++)
        {

            UnitTileIdeal _FirstTile = _UnitIdealWorldMap._IdealWorldMap[(int)Random.Range(0, _UnitIdealWorldMap._IdealWorldMap.Count)];
            UnitTileIdeal _SecondTile = null;
            UnitTileIdeal _ThirdTile = null;

            for (int i = 0; i < _UnitIdealWorldMap._IdealWorldMap.Count; i++)
            {
                if (_UnitIdealWorldMap._IdealWorldMap[i] != _FirstTile)
                {
                    if (DistRBetweenUnitTileIdeal(_UnitIdealWorldMap._IdealWorldMap[i], _FirstTile) < 50)
                    {
                        _SecondTile = _UnitIdealWorldMap._IdealWorldMap[i];

                        for (int j = 0; j < _UnitIdealWorldMap._IdealWorldMap.Count; j++)
                        {
                            if (  (_UnitIdealWorldMap._IdealWorldMap[j] != _FirstTile) && (_UnitIdealWorldMap._IdealWorldMap[j] != _SecondTile))
                            {
                                if (DistRBetweenUnitTileIdeal(_UnitIdealWorldMap._IdealWorldMap[j], _SecondTile) < 50)
                                {

                                    _ThirdTile = _UnitIdealWorldMap._IdealWorldMap[j];
                                    break;
                                }
                            }
                        }
                    }
                }

                if (_ThirdTile != null)
                {
                    _SecondTile._TileType = _FirstTile._TileType;
                    _ThirdTile._TileType = _FirstTile._TileType;

                    _SecondTile._TileGrade = _FirstTile._TileGrade;
                    _ThirdTile._TileGrade = _FirstTile._TileGrade;

                    break;

                }
            }
                  

            //Debug.Log(" TileRebatched: "+k);
        }
    }

    public float DistRBetweenUnitTileIdeal(UnitTileIdeal _target, UnitTileIdeal _refer)
    {
        return (_target._PosX - _refer._PosX) * (_target._PosX - _refer._PosX) + (_target._PosZ - _refer._PosZ) * (_target._PosZ - _refer._PosZ);

    }

public void BatchTilesInAllowed(UnitIdealWorldMap _UnitIdealWorldMap) //이 월드의 각 타일에게 적절한 타입과 그레이드를 배치해준다.
    {
        AllowedUnitTileType _TempAllowedUnitTileType = null;
        for(int i=0; i< _UnitIdealWorldMap._IdealWorldMap.Count; i++)
        {
            _TempAllowedUnitTileType = RollAndGetAllowedUnitTileType(_UnitIdealWorldMap);
            _UnitIdealWorldMap._IdealWorldMap[i]._TileType = _TempAllowedUnitTileType._UnitTileType._TileType;
            _UnitIdealWorldMap._IdealWorldMap[i]._TileGrade = _TempAllowedUnitTileType._UnitTileType._Grade;
        }

    }

    AllowedUnitTileType RollAndGetAllowedUnitTileType(UnitIdealWorldMap _UnitIdealWorldMap )  //확률에 따라 굴려서, 적절한 수준의 얼로우드 유닛타일타입을 반환한다.
    { //확률문제
        float _refDiceNum = 1f;
        AllowedUnitTileType _return = _UnitIdealWorldMap._AllowedUnitTileTypeList[0];

        for (int i = 0; i < _UnitIdealWorldMap._AllowedUnitTileTypeList.Count; i++)
        {
            if (Random.Range(0f, _refDiceNum) <= _UnitIdealWorldMap._AllowedUnitTileTypeList[i]._Possibility)
            {
                _return = _UnitIdealWorldMap._AllowedUnitTileTypeList[i];
                break;
            }
            else
            {
                _refDiceNum -= _UnitIdealWorldMap._AllowedUnitTileTypeList[i]._Possibility;

            }
        }
        return _return;

    }


    public List<UnitTile> _TempWorldMap; //인스턴시에이트 된 현재의 월드맵
    public List<UnitTile> ImportIdealWorldMapAsShown(List<UnitTileIdeal> _target)  //duplicated된 Ideal월드맵을 현재 월드맵으로 바꿔준다. 이건 인게임 로딩 및 월드맵에서 필요하다.  테스트 완료
    {
        List<UnitTile> _return = new List<UnitTile>();
        UnitTile _tempUnitTile;
        if (_target == null)
        {
            return _return;
        }
        for (int i = 0; i < _target.Count; i++)
        {
            _tempUnitTile = new UnitTile();
            _tempUnitTile._Num = _target[i]._Num;
            _tempUnitTile._PosX = _target[i]._PosX;
            _tempUnitTile._PosZ = _target[i]._PosZ;

            _tempUnitTile._TileGrade = _target[i]._TileGrade;
            _tempUnitTile._TileType = _target[i]._TileType;
            _tempUnitTile._UnitHexLand = new UnitHexLand();

            _tempUnitTile._IsDiscovered = _target[i]._IsDiscovered;
            _tempUnitTile._IsBoughtLand = _target[i]._IsBoughtLand;
            _tempUnitTile._IsGenesisTile = _target[i]._IsGenesisTile;

            _tempUnitTile._TilePrice = _target[i]._TilePrice;
            _tempUnitTile._TilePriceType = _target[i]._TilePriceType;
            _tempUnitTile._UniqueFingerPrintNumber = _target[i]._UniqueFingerPrintNumber;
            _return.Add(_tempUnitTile);  //near타일 찾는건 함수로 따로 해주고.. 생산타일 추가하는것도 따로 해주자.
        }

        return _return;
    }

     
    public UnitWorldLand DrawCurrentWorldMapInitiating(List<UnitTile> _targetWorld, UnitIdealWorldMap _UnitIdealWorldMap)
    {
        UnitWorldLand tempWorldLand = Instantiate(_UniWorldLandPrefab, _WorldHexRoot.transform);
        tempWorldLand._UnitIdealWorldMap = _UnitIdealWorldMap;

        for (int i = 0; i < _targetWorld.Count; i++)
        {
            UnitHexLand tempHexLand = Instantiate(_UnitHexLandPrefab, tempWorldLand.transform);
            tempHexLand._CurrUnitTile = _targetWorld[i];
            tempHexLand._CurrUnitTile._UnitHexLand = tempHexLand;
            tempHexLand.name = _targetWorld[i]._Num + "";
            tempHexLand.transform.localPosition = new Vector3(tempHexLand._CurrUnitTile._PosX, 0, tempHexLand._CurrUnitTile._PosZ);
            tempHexLand._UniqueRotationDegree = (int)(Random.Range(0, 6)) * 60;  //초기 회전각을 잡아준다.
            tempHexLand._DoRedraw = true;
            tempHexLand._OnTitleScene = true;
        }

        return tempWorldLand;
    }

    public void RedrawCurrentWorldMap(List<UnitTile> _targetWorld ,bool _IsMine)  //타일정보에 따라 헥스랜드들의 외모를 다시 표현해준다.
    {
        for (int i = 0; i < _targetWorld.Count; i++)
        {                    
            _targetWorld[i]._UnitHexLand.DisplayHexAppearance();  //데이터를 모두 넣었으면 표현도 해준다.
            if (!_IsMine) //이게 어둡게 칠해주는부분인듯.
            {
                _targetWorld[i]._UnitHexLand.ShowHexAsDark();
            }
        }

    }

    //외부에서 터치할 경우, 처리

    public GameObject _IslandEnterUI;
    public float _RememberedCamPosX;
    public float _RememberedCamPosY;
    public float _RememberedCamPosZ;
    public float _RememberedCamLow; //반지름거리

    public AnimatedButton _PlayBTN; //게임 시작 버튼
    public GameObject _PlayBTNCover;


    public void CallUpThisIslandUIBTN()
    {
     

        if (!_IsMenuOn)
        {
            _IsMenuOn = true;
            _IsPause = true;

            _DataManager._BGMManager.SoundPlay((int)Random.Range(0, 4));
            

            _RememberedCamPosX = _TouchInputManagerTitle._MainCameraManager._varX;
            _RememberedCamPosY = _TouchInputManagerTitle._MainCameraManager._varY;
            _RememberedCamPosZ = _TouchInputManagerTitle._MainCameraManager._varZ;
            _RememberedCamLow = _TouchInputManagerTitle._MainCameraManager._Low;

            _IslandEnterUI.SetActive(true);
            _TouchInputManagerTitle._MainCameraManager.SetPosThisCamera(_CurrUnitWorldLand.transform.transform.localPosition.x + 17f, -28, _CurrUnitWorldLand.transform.transform.localPosition.z + 30,80);
            _IslandNumText.text = "#" + _CurrOpeningWorldMap._Num;



            if (_CurrOpeningWorldMap._IsOwned) //소유 시
            {
                _IslandValueRoot.SetActive(true);
                _IslandGoldOutPutRoot.SetActive(true);
                _IslandValueDriverRoot.SetActive(false);
                _IslandEnterFeeRoot.SetActive(false);

                _IslandEnterBTN.SetActive(false);
                _GoldOutPutText.text = "+ " + _CurrOpeningWorldMap._EarningGoldPerHour + "/ 2시간";
                _CurrValueText.text = _CurrOpeningWorldMap._WorldValue + "";
            }
            else  //무 소유 시
            {
                _IslandValueRoot.SetActive(false);
                _IslandGoldOutPutRoot.SetActive(false);
                _IslandValueDriverRoot.SetActive(true);
                _IslandEnterFeeRoot.SetActive(true);

                _FoodMultipleText.text = "x"+_CurrOpeningWorldMap._FoodMultiple;
                _IronMultipleText.text = "x" + _CurrOpeningWorldMap._IronMultiple;
                _WoodMultipleText.text = "x" + _CurrOpeningWorldMap._WoodMultiple;

                _ExpandMultipleText.text = "x" + _CurrOpeningWorldMap._ExpandMultiple;

                _IslandGoldOutPutRoot.SetActive(false);
                _IslandEnterBTN.SetActive(true);

                for(int i=0; i<_GradeStarList.Count; i++) //그레이드를 모두 꺼준후에, 아이콘을 켜준다.
                {
                    _GradeStarList[i].SetActive(false);
                }
                _GradeStarList[_CurrOpeningWorldMap._Grade].SetActive(true);
         

            }

            if (_CurrOpeningWorldMap._EntranceGoldFee == 0)
            {
                _EntraceFeeText.text = "Free";
                _EntraceFeeText.color = Color.white;

            }
            else
            {
                _EntraceFeeText.text = "" + _CurrOpeningWorldMap._EntranceGoldFee;
                if(_DataManager._UserCoin >= _CurrOpeningWorldMap._EntranceGoldFee )
                {
                    _EntraceFeeText.color = Color.white;
                }
                else
                {
                    _EntraceFeeText.color = Color.red;

                }
            }

            if (_DataManager._UserCoin >= _CurrOpeningWorldMap._EntranceGoldFee)
            {
                _PlayBTN.enabled = true;
                _PlayBTNCover.SetActive(false);
            }
            else
            {
                _PlayBTN.enabled = false;
                _PlayBTNCover.SetActive(true);

            }

            CheckAndDisplayProductByStone();
        }
    }

    

    public GameObject _ResourceInfo;
    public Text _WoodByStoneInIslandText;
    public Text _IronByStoneInIslandText;
    public Text _FoodByStoneInIslandText;

    public Text _UnitProductWoodByStoneText;
    public Text _UnitProductIronByStoneText;
    public Text _UnitProductFoodByStoneText;

    public void CheckAndDisplayProductByStoneTotally()
    {

        _UnitProductWoodByStoneText.text =  "+"+(_DataManager._UnitProductWoodByStone+ _DataManager._ProductWoodByHead) ;
         _UnitProductIronByStoneText.text = "+" + (_DataManager._UnitProductIronByStone + _DataManager._ProductIronByHead);
        _UnitProductFoodByStoneText.text = "+" + (_DataManager._UnitProductFoodByStone + _DataManager._ProductFoodByHead);


    }


    public void CheckAndDisplayProductByStone()
    {
        if(_CurrOpeningWorldMap._UnitProductFoodByStoneInIsland+ _CurrOpeningWorldMap._UnitProductIronByStoneInIsland + _CurrOpeningWorldMap._UnitProductWoodByStoneInIsland >0)
        {
            _ResourceInfo.SetActive(true);
            _WoodByStoneInIslandText.text = "+"+_CurrOpeningWorldMap._UnitProductWoodByStoneInIsland;
            _IronByStoneInIslandText.text = "+" + _CurrOpeningWorldMap._UnitProductIronByStoneInIsland;
            _FoodByStoneInIslandText.text = "+" + _CurrOpeningWorldMap._UnitProductFoodByStoneInIsland;

        }
        else
        {
            _ResourceInfo.SetActive(false);


        }


    }


    public void EnterThisIslandBTN()
    {
        if(_DataManager._UserCoin >= _CurrOpeningWorldMap._EntranceGoldFee)
        {
            _DataManager._UserCoin -= _CurrOpeningWorldMap._EntranceGoldFee;
            _DataManager.SavePlayerData();
            _DataManager._BGMManager.SoundPlay((int)Random.Range(7, 11));
            _DataManager._BGMManager.MusicStart((int)Random.Range(0, 2));

            SceneManager.LoadScene("MainScene");
         

        }
        else
        {
            _DataManager._BGMManager.SoundPlay(16);


        }
    }

    public void TurnOffEnterUIBTN()
    {
        _IslandEnterUI.SetActive(false);
        _TouchInputManagerTitle._MainCameraManager.SetPosThisCamera(_RememberedCamPosX, _RememberedCamPosY, _RememberedCamPosZ, _RememberedCamLow);

        _IsMenuOn = false;
        _IsPause = false;

        _DataManager._BGMManager.SoundPlay(5);

    }


    public void CallLeaderboard(int num)
    {
        _DataManager.PlzShowLeaderboardUI(num);
    }




    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.z - _refer.z) * (_target.z - _refer.z);

    }

    public Text _CollectingLeftTime; //콜렉팅까지 남은 시간
    public Text _WhyNotCollectGuide;  
    public void GuideWhyNotCollect()  //왜 회수할 수 없는지 안내해주는 텍스트
    {
        _WhyNotCollectGuide.gameObject.SetActive(true);

    }



    //코인 회수(콜렉팅) 관련 로직
    public void ComplieCollectingCoin()  //현재 코인이 얼마인지 집계합니다.
    {
        int _tempTotalCollect = 0;
        for (int i = 0; i < _IdealWorldMapList.Count; i++)
        {
            if (_IdealWorldMapList[i]._IsOwned)
            {
                _tempTotalCollect += _IdealWorldMapList[i]._EarningGoldPerHour;
            }
        }

        _DataManager._TotalRevenueCoinPerTick = _tempTotalCollect;
        DisplayCollectingText();

    }

    void DisplayCollectingText()
    {
        if (_DataManager._TotalRevenueCoinPerTick + _DataManager._ProductCoinByHead == 0)
        {
            _CollecingMoneyText.text = "-";

        }       
        else
        {
            _CollecingMoneyText.text = (_DataManager._TotalRevenueCoinPerTick + _DataManager._ProductCoinByHead) + "";

        }

    }

    public void CollectCoin() //코인을 회수합니다.
    {
        ComplieCollectingCoin();
        CalculateRemainedTime();

        if (_DataManager._RemainedTime==0 )
        {
            _DataManager._UserCoin += _DataManager._TotalRevenueCoinPerTick+_DataManager._ProductCoinByHead;

            _DataManager._CollectAvailableTime = System.DateTime.Now.ToLocalTime()+ System.TimeSpan.FromSeconds(_DataManager._TickTimeToCollectCoin);
            _DataManager._RemainedTime = _DataManager._TickTimeToCollectCoin;
            _CollectBTNCover.SetActive(true);
            _CollectingBTN.enabled = false;

            _DataManager._BGMManager.SoundPlay(32);
            UpCoinAcquired();

            //int _tempNotOwnedWorldCount = CheckNotOwnedWorldCount();
            //if (_tempNotOwnedWorldCount+3+ _DataManager._AdditionalSearchingCount <= 20)  // 콜렉팅 시 검정 섬이 개 이하면.. 개까지 새로 채워준다.
            //{

            //    SupplyWorldInTitle(3+_DataManager._AdditionalSearchingCount);

            //}
            //else
            //{
            //    SupplyWorldInTitle(20 - _tempNotOwnedWorldCount);

            //}

        }
        else
        {
            //틱타임이 다 되지 않았을 경우 메시지를 보여준다.

        }
        _DataManager.ReportScoreToRichRank(_DataManager._UserCoin);
        _DataManager.SavePlayerData();
    }

    Vector3 _GaugeScaleVec;

    void CalculateRemainedTime()  //현재 남은 시간을 계산하고 게이지를 표기해줍니다.
    {
        _CollectingBTN.gameObject.SetActive(true);

        //if (_DataManager._TotalRevenueCoinPerTick == 0)  //현재 수금량이 0 일 경우. ( 섬이 없는 경우) 시간을 다시 초기화해준다.
        //{
        //    _DataManager._RemainedTime = _DataManager._TickTimeToCollectCoin;
        //    _DataManager._CollectAvailableTime = System.DateTime.Now + System.TimeSpan.FromSeconds(_DataManager._TickTimeToCollectCoin);
        //}
        //else
        //{
            _DataManager._RemainedTime = Mathf.Max((float)(_DataManager._CollectAvailableTime - System.DateTime.Now.ToLocalTime()).TotalSeconds, 0);
               
        //}
        _GaugeScaleVec.x = 1 - _DataManager._RemainedTime / _DataManager._TickTimeToCollectCoin;
        _GaugeScaleVec.y = 1;
        _GaugeScaleVec.z = 1;
        _CollectingGaugeRemainTime.transform.localScale = _GaugeScaleVec;


    }

    //코인 획득 애니메이션
    public GameObject _CoinAcquired;
    public void UpCoinAcquired()
    {
        _CoinAcquired.SetActive(true);

    }




    //시간


    public string SecToText(int iTimeSec)
    {

        string returnValue = "";

        int year;
        int month;
        int day;
        int hour;
        int min;
        int sec;
        if (iTimeSec < 0)
        {
            iTimeSec = (-1) * iTimeSec;
            returnValue += "- ";
        }

        year = iTimeSec / (60 * 60 * 24 * 31 * 12);
        month = (iTimeSec - year * (60 * 60 * 24 * 31 * 12)) / (60 * 60 * 24 * 31);
        day = (iTimeSec - year * (60 * 60 * 24 * 31 * 12) - month * (60 * 60 * 24 * 31)) / (60 * 60 * 24);
        hour = (iTimeSec - year * (60 * 60 * 24 * 31 * 12) - month * (60 * 60 * 24 * 31) - day * (60 * 60 * 24)) / (60 * 60);
        min = (iTimeSec - year * (60 * 60 * 24 * 31 * 12) - month * (60 * 60 * 24 * 31) - day * (60 * 60 * 24) - hour * (60 * 60)) / 60;
        sec = iTimeSec % 60;

        if (year != 0)
        {

            returnValue += year + "년 ";
        }
        if (month != 0)
        {

            returnValue += month + "월 ";
        }
        if (day != 0)
        {

            returnValue += day + "일 ";
        }
        if (hour != 0)
        {

            returnValue += hour + "시간 ";
        }
        if (min != 0)
        {

            returnValue += min + "분 ";
        }
        if (sec != 0)
        {

            if (iTimeSec < 60)
            {
                returnValue += sec + "초";
            }
        }

        return returnValue;


    }

    public string SecToTextEng(int iTimeSec)
    {

        string returnValue = "";

        int year;
        int month;
        int day;
        int hour;
        int min;
        int sec;

        year = iTimeSec / (60 * 60 * 24 * 31 * 12);
        month = (iTimeSec - year * (60 * 60 * 24 * 31 * 12)) / (60 * 60 * 24 * 31);
        day = (iTimeSec - year * (60 * 60 * 24 * 31 * 12) - month * (60 * 60 * 24 * 31)) / (60 * 60 * 24);
        hour = (iTimeSec - year * (60 * 60 * 24 * 31 * 12) - month * (60 * 60 * 24 * 31) - day * (60 * 60 * 24)) / (60 * 60);
        min = (iTimeSec - year * (60 * 60 * 24 * 31 * 12) - month * (60 * 60 * 24 * 31) - day * (60 * 60 * 24) - hour * (60 * 60)) / 60;
        sec = iTimeSec % 60;

        if (year != 0)
        {

            returnValue += year + "year ";
        }
        if (month != 0)
        {

            returnValue += month + "Month ";
        }
        if (day != 0)
        {

            returnValue += day + "day ";
        }
        if (hour != 0)
        {

            returnValue += hour + "hour ";
        }
        if (min != 0)
        {

            returnValue += min + "min ";
        }
        if (sec != 0)
        {
            if (iTimeSec < 60)
            {
                returnValue += sec + "sec";
            }
        }

        return returnValue;


    }




}



