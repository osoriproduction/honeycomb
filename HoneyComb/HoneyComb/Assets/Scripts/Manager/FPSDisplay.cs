﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSDisplay : MonoBehaviour {

	float deltaTime = 0.0f;
	float averageFPS;
	float totalFPS;
	float fps ;
	int iCount;


	void Update()
	{

//		if (m_GameManager.bIsGlobalPaused) {
//
//			return;
//		}


		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		iCount++;
		totalFPS = totalFPS + fps;
		averageFPS = (totalFPS) / iCount;

	}

	void OnGUI()
	{
		int w = Screen.width, h = Screen.height;

		GUIStyle style = new GUIStyle();

		Rect rect = new Rect(30,0, w, h * 2 / 100);
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h * 4 / 100;
		style.normal.textColor = new Color (1,1,1, 1.0f);
		float msec = deltaTime * 1000.0f;
		fps = 1.0f / deltaTime;
		string text = string.Format("{0:0.0} ms ({1:0.} fps) ({2:0. })", msec, fps,averageFPS);
		GUI.Label(rect, text, style);

	}
}
