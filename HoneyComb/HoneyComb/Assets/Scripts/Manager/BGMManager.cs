﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMManager : MonoBehaviour {

    public AudioSource[] _AudioSources;
    public AudioSource _CurrAudioSource;

    public bool _MusicOn;
    public bool _SoundOn;
    
    public AudioSource[] _SoundSources;
    public GameManager _GameManager;
    public TitleManager _TitleManager;


    void Awake()
    {
        _MusicOn = true;
        _SoundOn = true;
        
        _CurrAudioSource = new AudioSource();
    }

    public void GetManager()
    {
        if (GameObject.Find("GameManager") != null)
        {
            _GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }

        if (GameObject.Find("TitleManager") != null)
        {
            _TitleManager = GameObject.Find("TitleManager").GetComponent<TitleManager>();
        }
    }



    public void SoundPlay(int iNum)
    {
        if (_SoundOn)
        {
            _SoundSources[iNum].Play();
        }
    }


    public void MusicStart(int iNum)
    {
        if (_MusicOn)
        {
            if (_CurrAudioSource != null)
                StartCoroutine(MusicOffIEnum(_CurrAudioSource));

            StartCoroutine(MusicOnIENum(_AudioSources[iNum]));
            _CurrAudioSource = _AudioSources[iNum];

        }
    }


    IEnumerator MusicOnIENum(AudioSource Music)
    {
        yield return new WaitForSecondsRealtime(0.3f);

        Music.Play();
        float tempVolume = 0.00f;
        Music.volume = tempVolume;

        for (int i = 0; i <= 20; i++)
        {

            tempVolume += 0.02f;
            Music.volume = tempVolume;

            yield return new WaitForSecondsRealtime(0.3f);

        }       

    }


    IEnumerator MusicOffIEnum(AudioSource Music)
    {
        float tempVolume = 0.00f;
        Music.volume = tempVolume;

        //for (int i = 0; i <= 9; i++)
        //{
        //    tempVolume -= 0.1f;
        //    Music.volume = tempVolume;

        //    yield return new WaitForSecondsRealtime(0.1f);

        //}
        yield return new WaitForSecondsRealtime(0.3f);

        Music.Stop();

    }



}
