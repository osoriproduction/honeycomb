﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
#if UNITY_EDITOR 
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class UtilManager : MonoBehaviour 
{
	private static UtilManager instanceRef = null;
	public static UtilManager Instance
	{
		get 
		{
			if(instanceRef == null )
			{
				instanceRef = FindObjectOfType(typeof(UtilManager)) as UtilManager;

				if(instanceRef == null)
				{
					GameObject obj = new GameObject();
					obj.name = "UtilManager";
					instanceRef = obj.AddComponent<UtilManager>();
				}
			}
			return instanceRef;
		}
	}
	
	void OnApplicationQuit()
	{
		instanceRef = null;
	}
	
	void Awake()
	{
		//DontDestroyOnLoad(this);
	}

	public int GetCurrentTime()
	{
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		int iCurrentTime = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		
		return iCurrentTime;
	}


	public void CreateJsonFile( string strFileName, object objValue )
	{
		
#if UNITY_EDITOR
		string strFilePath = Application.dataPath + "/Resources/Data/" + strFileName + ".txt";

#elif UNITY_ANDROID
		string strFilePath = pathForDocumentsFile( strFileName );
#endif
		string strFileData = JsonFx.Json.JsonWriter.Serialize( objValue );

		FileStream fs = new FileStream( strFilePath, FileMode.Create );
		StreamWriter w = new StreamWriter( fs, Encoding.UTF8 );

		w.WriteLine( strFileData );
		w.Flush();
		w.Close();

#if UNITY_EIDTOR 
		if( !Application.isPlaying  && Application.isEditor )
		EditorUtility.DisplayDialog("Map Export Success", "정보를 " + strFileName + ".txt 에 저장했습니다.", "확인");
#endif 

	}




	public string LoadJsonFile(string strFileName)
	{
		string tempMessage = "";

#if UNITY_EDITOR
		string strFilePath = Application.dataPath + "/Resources/Data/" + strFileName + ".txt";

#elif UNITY_ANDROID
		string strFilePath = pathForDocumentsFile( strFileName );

#endif

		FileStream fs = new FileStream(strFilePath, FileMode.OpenOrCreate );
		StreamReader r = new StreamReader( fs, Encoding.UTF8 );

	
		if (r != null)
			tempMessage = r.ReadLine ();

		r.Close();
		fs.Close(); 

		return tempMessage;
	}



	public void DeleteJsonFile(string strFileName)
	{

		#if UNITY_EDITOR
		string strFilePath = Application.dataPath + "/Resources/Data/" + strFileName + ".txt";

		#elif UNITY_ANDROID
		string strFilePath = pathForDocumentsFile( strFileName );

		#endif


		File.Delete (strFilePath);

	
	}



	public string pathForDocumentsFile( string filename ) 
	{ 
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			string path = Application.dataPath.Substring( 0, Application.dataPath.Length - 5 );
			path = path.Substring( 0, path.LastIndexOf( '/' ) );
			return Path.Combine( Path.Combine( path, "Documents" ), filename );
		}

		else if(Application.platform == RuntimePlatform.Android)
		{
			string path = Application.persistentDataPath; 
			path = path.Substring(0, path.LastIndexOf( '/' ) ); 
			return Path.Combine (path, filename);
		} 

		else 
		{
			string path = Application.dataPath; 
			path = path.Substring(0, path.LastIndexOf( '/' ) );
			return Path.Combine (path, filename);
		}
	}

}
