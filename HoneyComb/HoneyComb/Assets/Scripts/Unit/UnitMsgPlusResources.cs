﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMsgPlusResources : MonoBehaviour {

    public UnitNumber3D _ReferedNumber3D; //연결된 번호

    public GameObject _ResTypeWood;
    public GameObject _ResTypeIron;
    public GameObject _ResTypeFood;
    public GameObject _ResTypeGold;


    public void SetResourceType( ResourceType _ResType)
    {
        _ResTypeWood.SetActive(false);
        _ResTypeIron.SetActive(false);
        _ResTypeFood.SetActive(false);
        _ResTypeGold.SetActive(false);

        if (_ResType == ResourceType.Wood)
        {
            _ResTypeWood.SetActive(true);

        }
        else if (_ResType == ResourceType.Iron)
        {
            _ResTypeIron.SetActive(true);

        }
        else if (_ResType == ResourceType.Food)
        {
            _ResTypeFood.SetActive(true);

        }
        else if (_ResType == ResourceType.Gold)
        {
            _ResTypeGold.SetActive(true);

        }

    }


}
