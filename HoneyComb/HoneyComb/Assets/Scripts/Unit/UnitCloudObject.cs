﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCloudObject : MonoBehaviour {

    public float _SPDx;
    public float _SPDz;

    public bool _OnTitleScene;

	void Start () {

        if (_OnTitleScene)
        {
            this.gameObject.transform.localPosition = new Vector3(Random.Range(-300, 300), Random.Range(-3, 3), Random.Range(-300,300));

        }
        else
        {
            this.gameObject.transform.localPosition = new Vector3(Random.Range(-60, 60), Random.Range(-3, 3), Random.Range(-60, 60));

        }

        _SPDx = Random.Range(-0.01f, 0.01f);
        _SPDz = Random.Range(-0.005f, 0.005f);
        MovSPDVec3 = new Vector3(_SPDx, 0, _SPDz);

        if (Random.Range(0, 100) < 50)
        {
            this.gameObject.SetActive(false);
        }

    }
    Vector3 MovSPDVec3;

	void Update () {
        if (this.gameObject.activeSelf)
            this.gameObject.transform.Translate(MovSPDVec3);

    }
}
