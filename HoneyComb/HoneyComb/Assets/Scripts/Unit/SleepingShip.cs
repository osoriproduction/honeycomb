﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepingShip : MonoBehaviour {

    public TitleManager _TitleManager;

    public float _MaxVelocity;
    public float _CurrVelocity;
    public float _Acceleration;

    public bool _MoveOn;

    public bool _IsAI; //이 배는 AI이다. 

    public float _A;
    public float _W;
    int _frame;

    public int _remainedWillingCount;
    public UnitWorldLand _CurrTargetWorld; 

    void Start () {
        _CurrTargetWorld = null;
        _TitleManager = GetComponentInParent<TitleManager>();
        _A = 0.07f;
        _W = 0.03f;
        _remainedWillingCount = 5;
        if (_IsAI)
        {
            StartCoroutine(AIBrain());
            this.transform.localPosition = new Vector3(Random.Range(-300f, 300f), 0, Random.Range(-300f, 300f));
        }
    }

    IEnumerator AIBrain()
    {
        for(; ; )
        {
            yield return new WaitForSeconds(1);

            if (Random.Range(0, 100) < 20)
            {
                if (_remainedWillingCount == 0)
                {
                    _remainedWillingCount = 5;
                    SetTarget(_TitleManager._UnitWorldLandList[(int)Random.Range(0, _TitleManager._UnitWorldLandList.Count)]);

                }
                else
                {
                    _remainedWillingCount--;

                }
            }
        }

    }

	void Update () {
        _frame++;

        if (_MoveOn && _CurrTargetWorld != null)
        {
            if (_CurrVelocity <= _MaxVelocity)
            {
                _CurrVelocity += _Acceleration;
            }
            this.transform.Translate(new Vector3(0, 0, _CurrVelocity));

        }
        else
        {
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, _A * Mathf.Sin(_frame * _W), this.transform.localPosition.z);

        }
               
    }

    public void SetTarget(UnitWorldLand _UnitWorldLand)
    {
        _MoveOn = true;
        _remainedWillingCount = 10;
        _CurrVelocity = 0;
        
        this.transform.LookAt(_UnitWorldLand.transform);
        _CurrTargetWorld = _UnitWorldLand;
    }
   
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("UnitHex"))
        {
            _MoveOn = false;
            _CurrVelocity = 0;
            _remainedWillingCount = 0;
            _CurrTargetWorld = null;
        }
    }
}
