﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHexLand : MonoBehaviour {


    //넥스트타일에 배속된 헥스랜드들
    public bool _IsShowOnNextTile;

    //외부 참조
    public GameManager _GameManager;
    public UnitNumber3D _UnitNumber3D;


    //내부 참조
    public GameObject _BuyOnMarker;
    public GameObject _BuyOnHexSelection;

    public GameObject _WoodIcon3D;
    public GameObject _IronIcon3D;
    public GameObject _FoodIcon3D;
    public GameObject _GoldIcon3D;

    public ParticleSystem _FlowerEffect; //땅 건축되었을 때의 이펙트
    public ParticleSystem _FlowerEffectLong;

    public List<ParticleSystem> _BurnEffectList; //번 이펙트 리스트

    public GameObject _NotBought;
    public GameObject _NotDiscoveried;
    public List<GameObject> _TileTypeTown;
    public List<GameObject> _TileTypeForest;
    public List<GameObject> _TileTypeMountain;
    public List<GameObject> _TileTypeFarmLand;
    public List<GameObject> _TileTypeWater;
    public List<GameObject> _TileTypeFlyingStone;

    public List<GameObject> _TileTypeTownForest;
    public List<GameObject> _TileTypeTownMountain;
    public List<GameObject> _TileTypeTownFarm;

    public List<GameObject> _TileTypeStoneForest;
    public List<GameObject> _TileTypeStoneMountain;
    public List<GameObject> _TileTypeStoneFarmLand;



    public MeshRenderer[] _SavedMeshRenderers;  //저장될 메쉬렌더러들
    public List<Material> _SavedMaterialList;  //메쉬렌더러의 매터리얼들
    public Material _SelectingMaterial;  //선택한 매터리얼(넣어줄)
    Material[] _SelectingMaterials; //선택한 매터리얼스
    Material[] _TempMaterials;

    public Material _DarkMaterial; //어두운 머터리얼
    Material[] _DarkMaterials; //선택한 매터리얼스


    public List<GameObject> _DisplayedNumberObjectList; //현재 이 타일에서 받은 번호 리스트들을 임시로 저장하는 공간

    public GameObject _TileInfoRoot;

    //시스템
    public UnitTile _CurrUnitTile;

    public int _UniqueRotationDegree;//헥스별로 유니크한 회전 값. 활성화된 타일에만 적용된다.

    public bool _IsSelected; //일단 현재  선택되었는지 표시
    public bool _NotBoughtButBuyable; // 아직 사진 않았지만 살 수 있는 상태. 
    public bool _IsContactedWithLand; //랜드와 맞닿아있는 바다.

    public bool _DoRedraw; //이게 표시되어 있으면 다시 그려야 한다.

    public bool _OnTitleScene; //타이틀씬에서 초청했을 경우 체크

  



    void Start () {
        _GameManager = GetComponentInParent<GameManager>();
        ResetMeshRenderers();

        _DisplayedNumberObjectList = new List<GameObject>();
        if (!_OnTitleScene)
        {
            InitMsgBoxes();
            if (!_IsShowOnNextTile) //이거 빨간색 떠서. 임시로 막아놓음.  넥스트 정보타일임
            {
                if (_CurrUnitTile._TileType == TileType.Town)
                {
                    _CurrUnitTile._Is2XBoosting = true;
                }
            }
        }
    }


    public void DisplayHexAppearance()   //현재 타일 정보에 맞게 외모를 표현해준다.
    {
        //일단은 모든 표현체를 꺼준다.

        _NotBought.SetActive(false);
        _NotDiscoveried.SetActive(false);

        for (int i = 0; i < _TileTypeTown.Count; i++)
        {
            if(_TileTypeTown[i].activeSelf)
            _TileTypeTown[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeForest.Count; i++)
        {
            if (_TileTypeForest[i].activeSelf)
                _TileTypeForest[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeMountain.Count; i++)
        {
            if (_TileTypeMountain[i].activeSelf)
                _TileTypeMountain[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeFarmLand.Count; i++)
        {
            if (_TileTypeFarmLand[i].activeSelf)
                _TileTypeFarmLand[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeWater.Count; i++)
        {
            if (_TileTypeWater[i].activeSelf)
                _TileTypeWater[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeFlyingStone.Count; i++)
        {
            if (_TileTypeFlyingStone[i].activeSelf)
                _TileTypeFlyingStone[i].SetActive(false);

        }

        for (int i = 0; i < _TileTypeTownFarm.Count; i++)
        {
            if (_TileTypeTownFarm[i].activeSelf)
                _TileTypeTownFarm[i].SetActive(false);

        }

        for (int i = 0; i < _TileTypeTownForest.Count; i++)
        {
            if (_TileTypeTownForest[i].activeSelf)
                _TileTypeTownForest[i].SetActive(false);

        }

        for (int i = 0; i < _TileTypeTownMountain.Count; i++)
        {
            if (_TileTypeTownMountain[i].activeSelf)
                _TileTypeTownMountain[i].SetActive(false);

        }
        for (int i = 0; i < _TileTypeStoneForest.Count; i++)
        {
            if (_TileTypeStoneForest[i].activeSelf)
                _TileTypeStoneForest[i].SetActive(false);

        }
        for (int i = 0; i < _TileTypeStoneFarmLand.Count; i++)
        {
            if (_TileTypeStoneFarmLand[i].activeSelf)
                _TileTypeStoneFarmLand[i].SetActive(false);

        }
        for (int i = 0; i < _TileTypeStoneMountain.Count; i++)
        {
            if (_TileTypeStoneMountain[i].activeSelf)
                _TileTypeStoneMountain[i].SetActive(false);

        }




        //현재 타일 타입에 맞게 조정해준다.

        if (!_CurrUnitTile._IsBoughtLand)  //구매한 땅이 아니다.
        {
            _NotBought.SetActive(true);
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, -2, this.transform.localPosition.z);
        }
        else if (!_CurrUnitTile._IsDiscovered)  //발견된 땅이 아니다.
        {
            _UnitNumber3D.TurnOffDisplayNumber();

            _NotDiscoveried.SetActive(true);
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, 0, this.transform.localPosition.z);
        }
        else  //발견되었으므로 표현형에 따라 보여준다.
        {
            _UnitNumber3D.TurnOffDisplayNumber();
            this.transform.localRotation = Quaternion.Euler(new Vector3(0, _UniqueRotationDegree, 0));

            if (this.gameObject.activeSelf)
                StartCoroutine(MovingToAltitudeForTile(_CurrUnitTile._TileGrade * 2, _CurrUnitTile._TileGrade));

            if (_CurrUnitTile._TileType == TileType.Town)
            {
                _TileTypeTown[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.Forest)
            {
                _TileTypeForest[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.Mountain)
            {
                _TileTypeMountain[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.FarmLand)
            {
                _TileTypeFarmLand[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.Water)
            {
                _TileTypeWater[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.FlyingStone)
            {
                _TileTypeFlyingStone[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.TownForest)
            {
                _TileTypeTownForest[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.TownMountain)
            {
                _TileTypeTownMountain[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.TownFarm)
            {
                _TileTypeTownFarm[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.StoneForest)
            {
                _TileTypeStoneForest[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.StoneMountain)
            {
                _TileTypeStoneMountain[_CurrUnitTile._TileGrade].SetActive(true);

            }
            else if (_CurrUnitTile._TileType == TileType.StoneFarm)
            {
                _TileTypeStoneFarmLand[_CurrUnitTile._TileGrade].SetActive(true);

            }

        }
        ResetMeshRenderers();
    }


    IEnumerator MovingToAltitudeForTile(float _TargetAltitude, int _GotTileGrade)
    {
        //if(_CurrUnitTile._Num == 10000000)
       // Debug.Log("MovingToAltitudeForTile _TargetAltitude: " + _TargetAltitude);

        float _velocityConst = (_TargetAltitude - this.transform.localPosition.y) * 0.03f;
        float _adjustedY = this.transform.localPosition.y;


        for ( int i=0; i<10000; i++)
        {
            yield return new WaitForEndOfFrame();

         
            if(_GotTileGrade == _CurrUnitTile._TileGrade)
            {
                _adjustedY = this.transform.localPosition.y + _velocityConst;
                this.transform.localPosition = new Vector3(this.transform.localPosition.x, _adjustedY, this.transform.localPosition.z);


                if (this.transform.localPosition.y > _TargetAltitude)
                {
                    this.transform.localPosition = new Vector3(this.transform.localPosition.x, _TargetAltitude, this.transform.localPosition.z);
                    break;
                }
            }
            else
            {
                _TargetAltitude = _CurrUnitTile._TileGrade * 2;
                _adjustedY = this.transform.localPosition.y - _velocityConst;
                this.transform.localPosition = new Vector3(this.transform.localPosition.x, _adjustedY, this.transform.localPosition.z);


                if (this.transform.localPosition.y < _TargetAltitude)
                {
                    this.transform.localPosition = new Vector3(this.transform.localPosition.x, _TargetAltitude, this.transform.localPosition.z);
                    break;
                }
            }


        }
        
    }







    public bool MainTouchUpBtn()  //터치를 떼어낼때 작동하는 기능
    {
        bool _return = false;
        if (_NotDiscoveried.activeSelf || _NotBought.activeSelf )
        {
            _CurrUnitTile._UnitHexLand._DoRedraw = true;
        }

        
        bool _IsConfirmedToBuy = false; //이게 참이면 사는 허락 받은것.

        if (!_CurrUnitTile._IsBoughtLand)//아직 구매하지 않은 땅.
        {
            if (_CurrUnitTile._TilePriceType == ResourceType.Wood)
            {
                if (_GameManager._PlayerWood >= _CurrUnitTile._TilePrice)
                {
                    _GameManager._PlayerWood -= _CurrUnitTile._TilePrice;
                    _IsConfirmedToBuy = true;
                }
            }
            else if (_CurrUnitTile._TilePriceType == ResourceType.Iron)
            {
                if (_GameManager._PlayerIron >= _CurrUnitTile._TilePrice)
                {
                    _GameManager._PlayerIron -= _CurrUnitTile._TilePrice;
                    _IsConfirmedToBuy = true;

                }
            }
            else if (_CurrUnitTile._TilePriceType == ResourceType.Food)
            {
                if (_GameManager._PlayerFood >= _CurrUnitTile._TilePrice)
                {
                    _GameManager._PlayerFood -= _CurrUnitTile._TilePrice;
                    _IsConfirmedToBuy = true;

                }
            }
            else if (_CurrUnitTile._TilePriceType == ResourceType.Gold)
            {
                if (_GameManager._PlayerGold >= _CurrUnitTile._TilePrice)
                {
                    _GameManager._PlayerGold -= _CurrUnitTile._TilePrice;
                    _IsConfirmedToBuy = true;

                }
            }
     
            if (_IsConfirmedToBuy) //구매 행위 시동
            {
                _GameManager._DataManager._BGMManager.SoundPlay(39);
                _FlowerEffect.Play();

                _CurrUnitTile._IsBoughtLand = true;
                _CurrUnitTile._IsDiscovered = true;
                _CurrUnitTile._UnitHexLand._NotBoughtButBuyable = false;
                _CurrUnitTile._UnitHexLand._IsContactedWithLand = false;

                _GameManager._PlayerTileList.Add(_CurrUnitTile);
                _GameManager.RedrawCurrentWorldMap();

                if (_GameManager._NotBoughtTileList.Contains(_CurrUnitTile))
                {
                    _GameManager._NotBoughtTileList.Remove(_CurrUnitTile);  //안산 타일에서 빼준다.
                    _GameManager.CheckIsStageCleared(); //스테이지가 클리어 되었는지 체크해준다.
                }
                _GameManager.CheckTotalPoducting();
                _IsConfirmedToBuy = false;
            }

        }
        else  //구매한 땅
        {
            if (!_CurrUnitTile._IsDiscovered)  //아직 발견되지 않은 땅.
            {
                _CurrUnitTile._IsDiscovered = true;
                _CurrUnitTile._Is2XBoosting = false;
                BatchNextTileOnTheCurrTouched(_CurrUnitTile); // 순서에 있는 타일을 배치해준다.

                _GameManager.RedrawCurrentWorldMap();  //다시 그려준다.

                _return = true;

                _GameManager._DataManager._BGMManager.SoundPlay(39);
                _FlowerEffect.Play();


            }
            else
            {

                _GameManager._TileInfoWindow.gameObject.SetActive(false);

            }
        }
        return _return;
    }

    void BatchNextTileOnTheCurrTouched(UnitTile _CurrUnitTile)  //현재 분홍 타일을 터치한 이 순간에 다음 타일을 배치해준다.
    {
        _CurrUnitTile._TileType = _GameManager._NextTileTypes[0]._UnitTileType._TileType;
        _CurrUnitTile._TileGrade = _GameManager._NextTileTypes[0]._UnitTileType._Grade;
        _GameManager._NextTileTypes.RemoveAt(0);

        _GameManager.DecideNextTiles(1);

    }



    bool CompareTileTypeIsSameOrNot(TileType _TargetTileType, TileType  _ReferTileType)  //두개의 타일 타입을 비교하여 같으면 true 반환. 유사 타일도 트루임.
    {
        bool _return = false;
        if ( _TargetTileType == _ReferTileType)
        {
            _return = true;
        }
        else if(_TargetTileType==TileType.Forest && _ReferTileType == TileType.StoneForest)
        {
            _return = true;

        }
        else if (_TargetTileType == TileType.StoneForest && _ReferTileType == TileType.Forest)
        {
            _return = true;

        }
        else if (_TargetTileType == TileType.Mountain && _ReferTileType == TileType.StoneMountain)
        {
            _return = true;

        }
        else if (_TargetTileType == TileType.StoneMountain && _ReferTileType == TileType.Mountain)
        {
            _return = true;

        }
       else if (_TargetTileType == TileType.FarmLand && _ReferTileType == TileType.StoneFarm)
        {
            _return = true;

        }
        else if (_TargetTileType == TileType.StoneFarm && _ReferTileType == TileType.FarmLand)
        {
            _return = true;

        }
        

        return _return;

    }


    bool CheckCurrSelectedCombineList()  //현재 손가락으로 그리고 있는 패턴이 해당되는 컴바인 리스트들을 뽑아준다.
    {
        bool _return = false;
        _GameManager._SelectedUnitCombineList.Clear();
         
        
        if (_GameManager._SelectedTileList.Count == 1) //두번째 긋는 타일일 경우. 
        {
            for ( int i=0; i<_GameManager._DataManager._TileCombineList.Count; i++) 
            {
                if(_GameManager._SelectedTileList[0]._TileGrade == _GameManager._DataManager._TileCombineList[i]._CombineGrade    //첫번째 저장한 타일을 체크. 
                    &&CompareTileTypeIsSameOrNot(_GameManager._SelectedTileList[0]._TileType, _GameManager._DataManager._TileCombineList[i]._TileType1) )
                {
                    if(_CurrUnitTile._TileGrade == _GameManager._DataManager._TileCombineList[i]._CombineGrade  //현재 저장하려고 하는 두번째 타일을 체크
                        && CompareTileTypeIsSameOrNot(_CurrUnitTile._TileType, _GameManager._DataManager._TileCombineList[i]._TileType2))                     
                    {
                        _return = true;

                    }
                }                
            }

            if (_GameManager._SelectedTileList[0]._TileType == TileType.Water)
            {
                if (_GameManager._SelectedTileList[0]._TileGrade > _CurrUnitTile._TileGrade)
                {
                    return true;
                }
            }
        }
        else if (_GameManager._SelectedTileList.Count == 2) //세번째 긋는 타일일 경우. 여기까지만 의미가 있다. 
        {           
            for (int i = 0; i < _GameManager._DataManager._TileCombineList.Count; i++)
            {
                if (_GameManager._SelectedTileList[0]._TileGrade == _GameManager._DataManager._TileCombineList[i]._CombineGrade    //첫 번째 저장한 타일을 체크. 
                  && CompareTileTypeIsSameOrNot(_GameManager._SelectedTileList[0]._TileType, _GameManager._DataManager._TileCombineList[i]._TileType1))
                {
                    if (_GameManager._SelectedTileList[1]._TileGrade == _GameManager._DataManager._TileCombineList[i]._CombineGrade  //두 번째 저장한 타일을 체크. 
                          && CompareTileTypeIsSameOrNot(_GameManager._SelectedTileList[1]._TileType, _GameManager._DataManager._TileCombineList[i]._TileType2))
                    {

                        if (_CurrUnitTile._TileGrade == _GameManager._DataManager._TileCombineList[i]._CombineGrade  //현재 저장하려고 하는 세 번째 타일을 체크
                            && CompareTileTypeIsSameOrNot(_CurrUnitTile._TileType, _GameManager._DataManager._TileCombineList[i]._TileType3))
                        {
                            _GameManager._SelectedUnitCombineList.Add(_GameManager._DataManager._TileCombineList[i]);  
                            _return = true;
                            break;  //여러개가 입력될 수도 있다. 일단, 그냥 브레이크 해주면 첫번째 위에 있는게 유일하게 입력될 것이다. 나중에는 확률 넣기도 용이한 구조.
                        }
                    }
                }
            }
            if (_GameManager._SelectedTileList[0]._TileType == TileType.Water)
            {
                if (_GameManager._SelectedTileList[0]._TileGrade > _CurrUnitTile._TileGrade && _GameManager._SelectedTileList[0]._TileGrade > _GameManager._SelectedTileList[1]._TileGrade)
                {

                    return true;


                }
            }
        }
       
        return _return;
    }

    public void MainTouchBTN()  
    {
        if (!_CurrUnitTile._IsBoughtLand)//아직 구매하지 않은 땅.
        {
        }
        else  //구매한 땅
        {
            if (!_CurrUnitTile._IsDiscovered)  //아직 발견되지 않은 땅.
            {        
            }
            else   //발견한 땅
            {
                if (_GameManager._SelectedTileList.Count < 3)
                {
                    if (_GameManager._SelectedTileList.Count == 0)
                    {
                        _IsSelected = true;
                        _GameManager._SelectedTileList.Add(_CurrUnitTile);
                        SetColorByIsSelected();

                        _GameManager._DataManager._BGMManager.SoundPlay(36);

                     

                    }
                    else if (_GameManager._SelectedTileList.Count == 1)
                    {
                        if ( !_GameManager._SelectedTileList.Contains(_CurrUnitTile)
                        && DistBetweenPointR(this.transform.position, _GameManager._SelectedTileList[_GameManager._SelectedTileList.Count - 1]._UnitHexLand.transform.position) < 30
                        )
                        {
                            if (CheckCurrSelectedCombineList())
                            {
                                _IsSelected = true;
                                _GameManager._SelectedTileList.Add(_CurrUnitTile);
                                SetColorByIsSelected();
                                _GameManager._DataManager._BGMManager.SoundPlay(36);

                            }
                            else if (                                
                                ( CompareTileTypeIsSameOrNot (_GameManager._SelectedTileList[0]._TileType, _CurrUnitTile._TileType)
                                && (_CurrUnitTile._TileType!= TileType.TownForest && _CurrUnitTile._TileType != TileType.TownFarm && _CurrUnitTile._TileType != TileType.TownMountain && _CurrUnitTile._TileType != TileType.Water)
                                && _GameManager._SelectedTileList[0]._TileGrade> _CurrUnitTile._TileGrade )
                                ||
                                ( CompareTileTypeIsSameOrNot(_GameManager._SelectedTileList[0]._TileType, TileType.Town)
                                && (_CurrUnitTile._TileType != TileType.TownForest && _CurrUnitTile._TileType != TileType.TownFarm && _CurrUnitTile._TileType != TileType.TownMountain && _CurrUnitTile._TileType != TileType.Water)
                                && _GameManager._SelectedTileList[0]._TileGrade > _CurrUnitTile._TileGrade )  
                                )  //타일 이동해주는거.
                            {
                                
                                float tempPosX = _GameManager._SelectedTileList[0]._PosX;
                                float tempPosZ = _GameManager._SelectedTileList[0]._PosZ ;
                                Vector3 tempVec3 = new Vector3(_GameManager._SelectedTileList[0]._UnitHexLand.transform.localPosition.x
                                    , _CurrUnitTile._TileGrade * 2
                                    , _GameManager._SelectedTileList[0]._UnitHexLand.transform.localPosition.z) ;

                                _GameManager._SelectedTileList[0]._PosX = _CurrUnitTile._PosX;
                                _GameManager._SelectedTileList[0]._PosZ = _CurrUnitTile._PosZ;
                                _GameManager._SelectedTileList[0]._UnitHexLand.transform.localPosition = new Vector3(_CurrUnitTile._UnitHexLand.transform.localPosition.x
                                    , _GameManager._SelectedTileList[0]._TileGrade*2 //이렇게 해서 강제로 높이 값에 대한 싱크를 맞춘다.
                                    , _CurrUnitTile._UnitHexLand.transform.localPosition.z);

                                _CurrUnitTile._PosX = tempPosX;
                                _CurrUnitTile._PosZ = tempPosZ;
                                _CurrUnitTile._UnitHexLand.transform.localPosition = tempVec3;

                                _GameManager._SelectedTileList[0]._UnitHexLand._IsSelected = false;  //선택은 다 취소
                                _IsSelected = false;
                                _GameManager._SelectedTileList[0]._UnitHexLand.SetColorByIsSelected();
                                SetColorByIsSelected();
                                

                                _GameManager.RedrawCurrentWorldMap(); //외모를 다시 표현해준다.
                                _GameManager._DataManager._BGMManager.SoundPlay((int)Random.Range(29, 32));

                                _GameManager.PlayEffectAffectedByThisTile(_GameManager._SelectedTileList[0]);
                                _GameManager._TileInfoWindow._DisplayTileInfoCount = -9999;
                                _GameManager._TileInfoWindow.DisplayOffTileInfo();
                                _GameManager.CheckTotalPoducting();

                            }
                        }
                    }
                    else if (_GameManager._SelectedTileList.Count == 2)
                    {
                        if (_GameManager._SelectedTileList[0] == _CurrUnitTile)//해당 타일이 첫번째 셀렉팅된 타일일 경우 취소 로직
                        {                    
                            _GameManager._SelectedTileList[1]._UnitHexLand._IsSelected = false;
                            _GameManager._SelectedTileList[1]._UnitHexLand.SetColorByIsSelected();
                            _GameManager._SelectedTileList.Remove(_GameManager._SelectedTileList[1]);

                            _GameManager._DataManager._BGMManager.SoundPlay(38);

                        }
                        else//그 외의 경우는 신규로 선택해준다.
                        {
                            if ( !_GameManager._SelectedTileList.Contains(_CurrUnitTile)
                         && DistBetweenPointR(this.transform.position, _GameManager._SelectedTileList[_GameManager._SelectedTileList.Count - 1]._UnitHexLand.transform.position) < 30
                         )
                            {
                                if (CheckCurrSelectedCombineList())
                                {
                                    _IsSelected = true;
                                    _GameManager._SelectedTileList.Add(_CurrUnitTile);
                                    SetColorByIsSelected();
                                    _GameManager._DataManager._BGMManager.SoundPlay(37);

                                }
                            }
                        }
                    }
                }
                else   //3개일 때 취소하는 기능
                {
                    if (_GameManager._SelectedTileList[1] == _CurrUnitTile) //해당 타일이 두번째로 셀렉팅된 타일일 경우, 
                    {
                        _GameManager._SelectedTileList[2]._UnitHexLand._IsSelected = false;
                        _GameManager._SelectedTileList[2]._UnitHexLand.SetColorByIsSelected();
                        _GameManager._SelectedTileList.Remove(_GameManager._SelectedTileList[2]);
                        _GameManager._DataManager._BGMManager.SoundPlay(38);


                    }
                    else if (_GameManager._SelectedTileList[0] == _CurrUnitTile)//해당 타일이 첫번째 셀렉팅된 타일일 경우
                    {
                        _GameManager._SelectedTileList[2]._UnitHexLand._IsSelected = false;
                        _GameManager._SelectedTileList[2]._UnitHexLand.SetColorByIsSelected();
                        _GameManager._SelectedTileList.Remove(_GameManager._SelectedTileList[2]);

                        _GameManager._SelectedTileList[1]._UnitHexLand._IsSelected = false;
                        _GameManager._SelectedTileList[1]._UnitHexLand.SetColorByIsSelected();
                        _GameManager._SelectedTileList.Remove(_GameManager._SelectedTileList[1]);
                        _GameManager._DataManager._BGMManager.SoundPlay(38);

                    }

                }
                _GameManager._TileInfoWindow.DisplayTileInfo(_CurrUnitTile);


            }
        }      
    }


    public void ResetMeshRenderers()  //메쉬렌더러들을 사전적으로 먼저 잡아서 정보에 저장해놓는다.
    {        
        _SavedMeshRenderers = GetComponentsInChildren<MeshRenderer>();
        _SavedMaterialList = new List<Material>();
        Material[] tempMaterials= new Material[] { };

        for (int i = 0; i < _SavedMeshRenderers.Length; i++)
        {
            tempMaterials= _SavedMeshRenderers[i].sharedMaterials;
            _SavedMaterialList.Add(tempMaterials[0]);
        }
        _SelectingMaterials = new Material[1] {null };  //이건 대입해줄 매터리얼스다.
        _SelectingMaterials[0] = _SelectingMaterial;
    }

    public void ShowHexAsDark()
    {
        _DarkMaterials = new Material[1] { null };
        _DarkMaterials[0] = _DarkMaterial;

        for (int i = 0; i < _SavedMeshRenderers.Length; i++)
        {
            _SavedMeshRenderers[i].materials = _DarkMaterials;  //보여줄 매터리얼을 각 메쉬렌더러에 넣어준다.
        }

    }


    public void DisplayBeSelected()  //선택되었다는 것을 시각적으로 보여준다.
    {
        for (int i = 0; i < _SavedMeshRenderers.Length; i++)
        {
            _SavedMeshRenderers[i].materials = _SelectingMaterials;  //보여줄 매터리얼을 각 메쉬렌더러에 넣어준다.
        }
    }

    public void UndisplayBeSelected()  //선택이 풀렸따는 것을 보여준다.
    {
        _TempMaterials = new Material[1] {null };

        for (int i = 0; i < _SavedMeshRenderers.Length; i++)
        {
            _TempMaterials[0] = _SavedMaterialList[i];
            _SavedMeshRenderers[i].materials = _TempMaterials;  //보여줄 매터리얼을 각 메쉬렌더러에 넣어준다.

        }
    }

    public void SetColorByIsSelected()  // isSelect에 맞게 색상을 조절해준다.
    {
        if (_IsSelected)
        {
            DisplayBeSelected();
           

        }
        else
        {
            UndisplayBeSelected();
        }
    }

    //기능
    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.z - _refer.z) * (_target.z - _refer.z);

    }


    //메시징
    public List<UnitMsgPlusResources> _MsgsResources;
    public GameObject _MsgPoolingRoot;

    void InitMsgBoxes() {
        _MsgsResources = new List<UnitMsgPlusResources>();

        for (int i = 0; i < 10; i++)
        {
            UnitMsgPlusResources tempMsg = Instantiate(_GameManager._UnitMsgPlusResourcesPrefab, _MsgPoolingRoot.transform);
            tempMsg.gameObject.SetActive(false);
            _MsgsResources.Add(tempMsg);
        }
    }

    public void ShowMSG(ResourceType _ResourceType, int _Number, float _delaySec, int _size, Vector3 _force, int _playFrame, Vector3 _downForce)
    {
        UnitMsgPlusResources _currText;
        for (int i = 0; i < _MsgsResources.Count; i++)
        {
            if (_MsgsResources[i] != null)
            {
                if (!_MsgsResources[i].gameObject.activeSelf)
                {

                    _currText = _MsgsResources[i];
                   // _MsgsResources[i]._ReferedNumber3D.DisplayNumber(_Number);
                    _MsgsResources[i].SetResourceType(_ResourceType);

                    StartCoroutine(ShowMSGIEnumWithForce(_currText, _delaySec, _force, _playFrame, _downForce));
                    break;

                }
            }
        }
    }

    IEnumerator ShowMSGIEnumWithForce(UnitMsgPlusResources _msg, float _delaySec, Vector3 _velInit, int _playFrame, Vector3 _downForce)
    {
        _msg.gameObject.SetActive(true);
        yield return new WaitForSeconds(_delaySec);

        _msg.gameObject.transform.localPosition = new Vector3(0, 0, 0);

        Vector3 _velocity = _velInit;

        for (int i = 0; i < _playFrame; i++)
        {
            if (i < 0)
            {
                _msg.gameObject.transform.Translate(_velocity);
            }
            else
            {
                _msg.gameObject.transform.Translate(_velocity);
               
                _velocity = new Vector3(_velocity.x, _velocity.y, _velocity.z) + _downForce;
                
            }
            yield return new WaitForEndOfFrame();
        }

        _msg.gameObject.SetActive(false);
    }




    //버닝 이펙트
    public GameObject _LandLava;
    public void BurnThisTile()
    {
        for(int i=0; i<_BurnEffectList.Count; i++)
        {
            if (Random.Range(0, 100) < 40)
                _BurnEffectList[i].Play();

        }
        StartCoroutine(AfterBurn());
    }

    IEnumerator AfterBurn()
    {
        yield return new WaitForSeconds(Random.Range(6, 20));

        //일단은 모든 표현체를 꺼준다.
        _NotBought.SetActive(false);
        _NotDiscoveried.SetActive(false);

        for (int i = 0; i < _TileTypeTown.Count; i++)
        {
            if (_TileTypeTown[i].activeSelf)
                _TileTypeTown[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeForest.Count; i++)
        {
            if (_TileTypeForest[i].activeSelf)
                _TileTypeForest[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeMountain.Count; i++)
        {
            if (_TileTypeMountain[i].activeSelf)
                _TileTypeMountain[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeFarmLand.Count; i++)
        {
            if (_TileTypeFarmLand[i].activeSelf)
                _TileTypeFarmLand[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeWater.Count; i++)
        {
            if (_TileTypeWater[i].activeSelf)
                _TileTypeWater[i].SetActive(false);
        }
        for (int i = 0; i < _TileTypeFlyingStone.Count; i++)
        {
            if (_TileTypeFlyingStone[i].activeSelf)
                _TileTypeFlyingStone[i].SetActive(false);

        }

        for (int i = 0; i < _TileTypeTownFarm.Count; i++)
        {
            if (_TileTypeTownFarm[i].activeSelf)
                _TileTypeTownFarm[i].SetActive(false);

        }

        for (int i = 0; i < _TileTypeTownForest.Count; i++)
        {
            if (_TileTypeTownForest[i].activeSelf)
                _TileTypeTownForest[i].SetActive(false);

        }

        for (int i = 0; i < _TileTypeTownMountain.Count; i++)
        {
            if (_TileTypeTownMountain[i].activeSelf)
                _TileTypeTownMountain[i].SetActive(false);

        }
        for (int i = 0; i < _TileTypeStoneForest.Count; i++)
        {
            if (_TileTypeStoneForest[i].activeSelf)
                _TileTypeStoneForest[i].SetActive(false);

        }
        for (int i = 0; i < _TileTypeStoneFarmLand.Count; i++)
        {
            if (_TileTypeStoneFarmLand[i].activeSelf)
                _TileTypeStoneFarmLand[i].SetActive(false);

        }
        for (int i = 0; i < _TileTypeStoneMountain.Count; i++)
        {
            if (_TileTypeStoneMountain[i].activeSelf)
                _TileTypeStoneMountain[i].SetActive(false);

        }

        _LandLava.SetActive(true);

    }


}
