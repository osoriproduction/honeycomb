﻿using UnityEngine;
using UnityEngine.UI;

public class TileInfoWindow : MonoBehaviour {

    public GameManager _GameManager;

    public Text _TileNameText;
    public Text _TileDesText;
    public Text _TileProductDesText;

    public UnitTileType _UnitTileType;

    private void Start()
    {
        this.gameObject.SetActive(false);

    }

    public int _DisplayTileInfoCount = 0;
    public void DisplayTileInfo(UnitTile _UnitTile )
    {
        _DisplayTileInfoCount++;
               
        if (_DisplayTileInfoCount > 40)
        {    

            if (_GameManager._SelectedTileList.Count == 1)
            {

                if (_DisplayTileInfoCount == 41) //41프레임마다 한번씩 플레이 시켜준다.
                {

                  _GameManager.PlayEffectAffectedByThisTile(_UnitTile); 

                }


                if (this.gameObject.activeSelf)
                    _DisplayTileInfoCount = -9999; //계속 아래 로직을 반복하는것을 방지해준다.
                    
           
                for (int i = 0; i < _GameManager._DataManager._TileTypeList.Count; i++)
                {
                    if (_UnitTile._TileGrade == _GameManager._DataManager._TileTypeList[i]._Grade && _UnitTile._TileType == _GameManager._DataManager._TileTypeList[i]._TileType)
                    {
                        this.gameObject.SetActive(true);
    
                        if (_GameManager._TouchInputManager._MainCamera.transform.position.x < _UnitTile._UnitHexLand.transform.position.x)
                        {
                            this.transform.localPosition = new Vector3(-491.12f, 112.8f, 0);
                        }
                        else
                        {
                            this.transform.localPosition = new Vector3(491.12f, 112.8f, 0);

                        }
                        _TileNameText.text = _GameManager._DataManager._TileTypeList[i]._ResultTileName;
                        _TileDesText.text = _GameManager._DataManager._TileTypeList[i]._ResultTileDes;
                        if (_GameManager._DataManager._TileTypeList[i]._TileProductDes != null) {
                            _TileProductDesText.text = _GameManager._DataManager._TileTypeList[i]._TileProductDes + " +"+ _UnitTile._CurrentProductCount;

                        }
                        else
                        {
                            _TileProductDesText.text = "";
                        }

                        break;
                    }
                }
            }
        }
    }

    public void DisplayOffTileInfo()
    {
        _DisplayTileInfoCount = 0;
        this.gameObject.SetActive(false);

    }
}
