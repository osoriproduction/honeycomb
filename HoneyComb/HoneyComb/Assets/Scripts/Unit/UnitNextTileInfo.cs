﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitNextTileInfo : MonoBehaviour {

    public GameManager _GameManager;
    public UnitHexLand _UnitHextLand;
    public int _NextTileOrder;

	void Start () {
        _GameManager = GetComponentInParent<GameManager>();
        _UnitHextLand = GetComponent<UnitHexLand>();
    }
	
    public void RedrawNextTileHexLand()
    {

        _UnitHextLand._CurrUnitTile = new UnitTile(0,0,_GameManager._NextTileTypes[_NextTileOrder]._UnitTileType._TileType , _GameManager._NextTileTypes[_NextTileOrder]._UnitTileType._Grade);
        _UnitHextLand._CurrUnitTile._IsBoughtLand = true;
        _UnitHextLand._CurrUnitTile._IsDiscovered = true;
        _UnitHextLand.DisplayHexAppearance();
    }

}
