﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class QUTransformAnimation : MonoBehaviour {

    public string _AnimMode;

    public float _ScalingCosW;
    public float _ScalingCosA;

    public float _RotateYSPD;
    public float _RotateZSPD;

    public float _MoveSPD;

    Vector3 _InitLocalScale;
    Vector3 _RotateYRefVec;
    Vector3 _RotateZRefVec;

    void Awake () {
        _InitLocalScale = this.transform.localScale;

        if (Random.Range(0, 100) < 50)
        {
            _RotateYRefVec = new Vector3(0, Random.Range( _RotateYSPD*0.9f, _RotateYSPD*1.1f), 0);

        }
        else
        {
            _RotateYRefVec = new Vector3(0, Random.Range(-_RotateYSPD * 1.1f,- _RotateYSPD * 0.9f), 0);

        }
        _RotateZRefVec = new Vector3(0,0 , _RotateZSPD);

        _vec3 = new Vector3(0, 0, 0);
    }

    int _tempFrame;

    Vector3 _vec3;
	void Update () {
        _tempFrame++;

        if (_AnimMode == "ROTATEY")
        {
            this.transform.Rotate(_RotateYRefVec);

        }
        else if (_AnimMode == "ROTATEZ")
        {
            this.transform.Rotate(_RotateZRefVec);

        }
        else if (_AnimMode == "SELECTION")
        {
            this.transform.localScale = _InitLocalScale *(1+ _ScalingCosA * Mathf.Sin(_tempFrame * _ScalingCosW));
        }
        else if (_AnimMode == "MARKER")
        {
            this.transform.localScale = _InitLocalScale * (1 + _ScalingCosA * Mathf.Sin(_tempFrame * _ScalingCosW));
            this.transform.Rotate(_RotateYRefVec);

        }
        else if (_AnimMode == "ICON3D")
        {
            this.transform.localScale = _InitLocalScale * (1.5f + _ScalingCosA * Mathf.Sin(_tempFrame * _ScalingCosW));
            //this.transform.Rotate(_RotateYRefVec);

        }
        else if (_AnimMode == "FLOATING")
        {
            _vec3.y = _MoveSPD * Mathf.Sin(_tempFrame * _ScalingCosW);

            this.transform.Translate(_vec3) ;

        }
    }

    void OnDisable()  // 
    {
        this.transform.localScale = _InitLocalScale;
        
    }

}

