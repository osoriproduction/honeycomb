﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitWorldLand : MonoBehaviour {

    public TitleManager _TitleManager;
    public UnitIdealWorldMap _UnitIdealWorldMap;  //생성시 미리 지정해준다.

    //섬 정보 표시
    public Text _TextEntFeeGold;

    public GameObject _IslandInfo;

    //둥둥 플로팅 관련 연출 사항
    float _FloatingCoefAngle;
    float _FloatingCoefAngleWelocity;
    float _FloatingCoefAmplitude;

    float _FloatingAngleWelocity;

    private void Start()
    {
        _TitleManager = GetComponentInParent<TitleManager>();
        StartCoroutine(SlowUpdate());

        _FloatingCoefAmplitude =Random.Range(0.001f, 0.005f);
        _FloatingCoefAngleWelocity =  Random.Range(0.001f, 0.005f);
        _FloatingAngleWelocity = Random.Range(-0.01f, 0.01f);

        if (!_UnitIdealWorldMap._IsOwned)
        {
            _IslandInfo.SetActive(true);
            if (_UnitIdealWorldMap._EntranceGoldFee == 0)
            {
                _TextEntFeeGold.text = "Free";

            }
            else
            {
                _TextEntFeeGold.text = _UnitIdealWorldMap._EntranceGoldFee + "";

            }

        }
        else
        {
            _IslandInfo.SetActive(false);

        }

        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.1f);

        _TitleManager._UnitWorldLandList.Add(this);

    }

    private void Update()
    {
        if (_TitleManager._IsPause)
        {
            return;
        }
        _FloatingCoefAngle++;

        //if (!_UnitIdealWorldMap._IsOwned) //소유하지 않은 경우.
        //{
            _UnitIdealWorldMap._WorldCurrAngle++;

        //}
       
        this.transform.Rotate(new Vector3(0, _FloatingAngleWelocity, 0));

        this.transform.localPosition = new Vector3(Mathf.Cos(_UnitIdealWorldMap._WorldAngleWelocity * _UnitIdealWorldMap._WorldCurrAngle) * _UnitIdealWorldMap._WorldRadius
             , _FloatingCoefAmplitude * Mathf.Cos(_FloatingCoefAngleWelocity * _FloatingCoefAngle) + _UnitIdealWorldMap._WorldRefY
             , Mathf.Sin(_UnitIdealWorldMap._WorldAngleWelocity * _UnitIdealWorldMap._WorldCurrAngle) * _UnitIdealWorldMap._WorldRadius);

      
        
    }



    IEnumerator SlowUpdate()
    {
        for(; ; )
        {
            yield return new WaitForSeconds(0.2f);

        }
    }

    public void TagThisWorldToCurrOpeningWorldMap()
    {
        _TitleManager._CurrUnitWorldLand = this;
        _TitleManager._CurrOpeningWorldMap = _UnitIdealWorldMap;
        _TitleManager.CallUpThisIslandUIBTN();

        _TitleManager._SleepingShip.SetTarget(this);
        _TitleManager._SleepingShip3.SetTarget(this);

    }



    public void MoveController()
    {


    }

}
