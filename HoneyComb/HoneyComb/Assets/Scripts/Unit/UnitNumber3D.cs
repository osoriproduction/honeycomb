﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitNumber3D : MonoBehaviour {

    public UnitHexLand _UnitHexLand;
    public List<GameObject> _NumberFirst;
    public List<GameObject> _NumberSecond;
    public List<GameObject> _NumberThird;
    public List<GameObject> _NumberFourth;


	
	
    public void TurnOffDisplayNumber() // 숫자를 모두 꺼주는 기능
    {
        for (int i = 0; i < _NumberFirst.Count; i++)
        { 
            if(_NumberFirst[i].activeSelf)
            _NumberFirst[i].SetActive(false);
        }
        for (int i = 0; i < _NumberSecond.Count; i++)
        {
            if (_NumberSecond[i].activeSelf)
                _NumberSecond[i].SetActive(false);

        }
        for (int i = 0; i < _NumberThird.Count; i++)
        {
            if (_NumberThird[i].activeSelf)
                _NumberThird[i].SetActive(false);

        }
        for (int i = 0; i < _NumberFourth.Count; i++)
        {
            if (_NumberFourth[i].activeSelf)
                _NumberFourth[i].SetActive(false);

        }
        if (_UnitHexLand != null)
        {
            _UnitHexLand._WoodIcon3D.SetActive(false);
            _UnitHexLand._FoodIcon3D.SetActive(false);
            _UnitHexLand._IronIcon3D.SetActive(false);
            _UnitHexLand._GoldIcon3D.SetActive(false);

        }
    }


    public List<GameObject> DisplayNumber(int _Num)   //숫자를 보여주는 기능
    {
        List<GameObject> _returnList = new List<GameObject>();

        TurnOffDisplayNumber();

        if (_Num < 10)
        {
            _NumberFirst[_Num].SetActive(true);

            _returnList.Add(_NumberFirst[_Num]);
        }
        else if (_Num < 100)
        {
            _NumberSecond[_Num / 10].SetActive(true);
            _NumberFirst[_Num%10].SetActive(true);

            _returnList.Add(_NumberSecond[_Num / 10]);
            _returnList.Add(_NumberFirst[_Num % 10]);

        }
        else if (_Num < 1000)
        {
            _NumberThird[_Num / 100].SetActive(true);
            _NumberSecond[_Num %100 /10 ].SetActive(true);
            _NumberFirst[_Num % 10].SetActive(true);

            _returnList.Add(_NumberThird[_Num / 100]);
            _returnList.Add(_NumberSecond[_Num % 100 / 10]);
            _returnList.Add(_NumberFirst[_Num % 10]);

        }
        else if (_Num < 10000)
        {
            _NumberFourth[_Num / 1000].SetActive(true);
            _NumberThird[_Num % 1000/100].SetActive(true);
            _NumberSecond[_Num % 100 / 10].SetActive(true);
            _NumberFirst[_Num % 10].SetActive(true);

            _returnList.Add(_NumberFourth[_Num / 1000]);
            _returnList.Add(_NumberThird[_Num % 1000 / 100]);
            _returnList.Add(_NumberSecond[_Num % 100 / 10]);
            _returnList.Add(_NumberFirst[_Num % 10]);

        }
        else
        {
            Debug.Log("Out of Number Range");
        }

        return _returnList;
    }
}
